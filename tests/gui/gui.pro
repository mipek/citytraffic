TEMPLATE = app
TARGET = tst_gui

CONFIG += qmltestcase console warn_on

QT += qml qml-private quick quick-private core-private gui-private frame frame-private multimedia sql widgets printsupport widgets framesecurity charts

DEV_MODE = 0
TEST_MODE = 1
LOCAL_DEV = 0
CONSOLE_DEV = 0

CONFIG(debug, release|debug): DEV_MODE = 1

INCLUDEPATH = $$PWD/../../

SOURCES += \
    $$PWD/tst_gui.cpp

include($$PWD/../../appvantage/src/src.pri)
include($$PWD/../../appvantage/res/dbpatch/dbpatch.pri)
include($$PWD/../../appvantage/res/key/key.pri)
include($$PWD/../../appvantage/deployment.pri)
include($$PWD/../../appvantage/version.pri)

TESTDATA = $$PWD/qml/*

OTHER_FILES += \
    $$PWD/qml/tst_queryBar.qml \
    $$PWD/qml/tst_pfmController.qml \
    $$PWD/qml/tst_Range.qml \
    $$PWD/qml/TestContainer.qml

DEFINES += \
    PFM_QML_TEST_SOURCE_DIR=\\\"$${_PRO_FILE_PWD_}/qml\\\" \
    DEV_MODE=$$DEV_MODE \
    CONSOLE_DEV=$$CONSOLE_DEV

