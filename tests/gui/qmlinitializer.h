#ifndef QMLINITIALIZER_H
#define QMLINITIALIZER_H

#include <QObject>

class QMLInitializer : public QObject
{
    Q_OBJECT
public:
    explicit QMLInitializer(QObject *parent = 0);

signals:

public slots:
};

#endif // QMLINITIALIZER_H
