import QtQuick 2.4
import QtTest 1.1
import Frame.Mobile.Core 1.0
import "../../../appvantage/res/gui"
import "../../../appvantage/res/gui/PfmQueryBarPrivate.js" as PfmQueryBarPrivate

TestContainer {
    id: container

    PfmQueryBar {
        id: queryBar

        SignalSpy {
            id: queryBarSpy
            target: queryBar
            signalName: 'queryChanged'
        }

        TestCase {
            name: 'QueryBarTest'

            function test_queryBar() {
                var triggerCount = 1; // no trigger when loading

                // compare(queryBar.fromDay, 365, 'Query should started back from a year ago');

                // =======================================================================
                // Calculate for Reference Monday 23-11-2015
                //
                //      Expected:
                //          Current Start/Finish : 16-11-2015/22-11-2015
                //          Previous Start/Finish: 17-11-2015/23-11-2015
                // =======================================================================

                queryBar.setQuery(new Date(Date.UTC(2015, 10, 23)));
                var refDate = DateTime.date('Y/m/d', queryBar.dd.startDateRef);
                var query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/11/16', 'Current Start Date should be 2015/11/16 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/11/22', 'Current Finish Date should be 2015/11/22 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/11/17', 'Previous Start Date should be 2014/11/17 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/11/23', 'Previous Finish Date should be 2014/11/23 for Ref ' + refDate);

                // =======================================================================
                // Calculate for Reference Monday 24-11-2015
                //
                //      Expected:
                //          Current Start/Finish : 23-11-2015/23-11-2015
                //          Previous Start/Finish: 24-11-2015/24-11-2015
                // =======================================================================

                queryBar.setQuery(new Date(Date.UTC(2015, 10, 24)));
                refDate = DateTime.date('Y/m/d', queryBar.dd.startDateRef);
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/11/23', 'Current Start Date should be 2015/11/23 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/11/23', 'Current Finish Date should be 2015/11/23 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/11/24', 'Previous Start Date should be 2014/11/24 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/11/24', 'Previous Finish Date should be 2014/11/24 for Ref ' + refDate);

                // =======================================================================
                // Calculate for Reference Monday 30-11-2015
                //
                //      Expected:
                //          Current Start/Finish : 23-11-2015/29-11-2015
                //          Previous Start/Finish: 24-11-2015/24-11-2015
                // =======================================================================

                queryBar.setQuery(new Date(Date.UTC(2015, 10, 30)));
                refDate = DateTime.date('Y/m/d', queryBar.dd.startDateRef);
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/11/23', 'Current Start Date should be 2015/11/23 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/11/29', 'Current Finish Date should be 2015/11/29 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/11/24', 'Previous Start Date should be 2014/11/24 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/11/30', 'Previous Finish Date should be 2014/11/30 for Ref ' + refDate);

                verify(!queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference Monday 30-11-2015
                //
                //      Action:
                //          previous()
                //
                //      Expected:
                //          Current Start/Finish : 16-11-2015/22-11-2015
                //          Previous Start/Finish: 17-11-2015/23-11-2015
                // =======================================================================

                queryBar.dd.previous();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/11/16', 'Current Start Date should be 2015/11/16 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/11/22', 'Current Finish Date should be 2015/11/22 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/11/17', 'Previous Start Date should be 2014/11/17 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/11/23', 'Previous Finish Date should be 2014/11/23 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should be query for Previous x 1 next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for Previous x 1 previous week.');

                // =======================================================================
                // Calculate for Reference Monday 30-11-2015
                //
                //      Action:
                //          previous() -> next()
                //
                //      Expected:
                //          Current Start/Finish : 23-11-2015/29-11-2015
                //          Previous Start/Finish: 24-11-2015/24-11-2015
                // =======================================================================

                queryBar.dd.next();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/11/23', 'Current Start Date should be 2015/11/23 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/11/29', 'Current Finish Date should be 2015/11/29 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/11/24', 'Previous Start Date should be 2014/11/24 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/11/30', 'Previous Finish Date should be 2014/11/30 for Ref ' + refDate);

                verify(!queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference Monday 30-11-2015
                //
                //      Action:
                //          previous() -> next() -> previous() -> previous()
                //
                //      Expected:
                //          Current Start/Finish : 09-11-2015/15-11-2015
                //          Previous Start/Finish: 10-11-2015/16-11-2015
                // =======================================================================

                queryBar.dd.previous();
                queryBar.dd.previous();
                query = queryBar.query();

                triggerCount += 2; // double action so double signal
                compare(queryBarSpy.count, triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/11/09', 'Current Start Date should be 2015/11/09 for Previous x 2 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/11/15', 'Current Finish Date should be 2015/11/15 for Previous x 2 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/11/10', 'Previous Start Date should be 2014/11/10 for Previous x 2 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/11/16', 'Previous Finish Date should be 2014/11/16 for Previous x 2 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference Monday 30-11-2015
                //
                //      Action:
                //          previous() -> next() -> previous() -> previous() -> next()
                //
                //      Expected:
                //          Current Start/Finish : 16-11-2015/22-11-2015
                //          Previous Start/Finish: 17-11-2015/23-11-2015
                // =======================================================================

                queryBar.dd.next();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/11/16', 'Current Start Date should be 2015/11/16 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/11/22', 'Current Finish Date should be 2015/11/22 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/11/17', 'Previous Start Date should be 2014/11/17 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/11/23', 'Previous Finish Date should be 2014/11/23 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should be query for Previous x 1 next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for Previous x 1 previous week.');

                // =======================================================================
                // Calculate for Reference Monday 30-12-2015
                //
                //      Expected:
                //          Current Start/Finish : 28-12-2015/29-12-2015
                //          Previous Start/Finish: NULL (Becuase 53th Week)
                // =======================================================================

                queryBar.dd.reset();
                ++triggerCount;
                queryBar.setQuery(new Date(Date.UTC(2015, 11, 30)));

                query = queryBar.query();
                refDate = DateTime.date('Y/m/d', queryBar.dd.startDateRef);

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/12/28', 'Current Start Date should be 2015/12/28 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/12/29', 'Current Finish Date should be 2015/11/22 for Ref ' + refDate);

                verify(query.previous === null, 'Previous Date should have been null for last year 53th week.');

                // =======================================================================
                // Calculate for Reference  13-3-2016 Type: Monthly
                //
                //      Expected:
                //          Current Start/Finish : 01-03-2016/13-03-2016
                //          Previous Start/Finish: 01-03-2015/13-03-2015
                // =======================================================================

                queryBar.dd.reset();
                queryBar.setQuery(new Date(Date.UTC(2016, 2, 13)), null, PfmQueryBarPrivate.TYPE_MONTHLY);

                refDate = DateTime.date('Y/m/d', queryBar.dd.startDateRef);
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_MONTHLY, queryBar.type, "Query Bar Type sould be Monthly")
                compare(0, queryBar.offset, "Query Bar Offset sould be 0")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2016/03/01', 'Current Start Date should be 2016/03/01 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2016/03/13', 'Current Finish Date should be 2016/03/13 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2015/03/01', 'Previous Start Date should be 2015/03/01 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2015/03/13', 'Previous Finish Date should be 2015/03/13 for Ref ' + refDate);

                // =======================================================================
                // Calculate for Reference  13-3-2016 Type: Monthly
                //
                //      Action:
                //          previous()
                //
                //      Expected:
                //          Current Start-Finish : 2016/02/01 - 2016/02/29
                //          Previous Start-Finish: 2015/02/01 - 2015/02/28
                // =======================================================================

                queryBar.dd.previous();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_MONTHLY, queryBar.type, "Query Bar Type sould be Monthly")
                compare(1, queryBar.offset, "Query Bar Offset sould be 1")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2016/02/01', 'Current Start Date should be 2016/02/01 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2016/02/29', 'Current Finish Date should be 2016/02/29 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2015/02/01', 'Previous Start Date should be 2015/02/01 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2015/02/28', 'Previous Finish Date should be 2015/02/28 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should be query for Previous x 1 next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for Previous x 1 previous week.');

                // =======================================================================
                // Calculate for Reference  13-3-2016 Type: Monthly
                //
                //      Action:
                //          previous() -> previous()
                //
                //      Expected:
                //          Current Start-Finish : 2016/01/01 - 2016/01/31
                //          Previous Start-Finish: 2015/01/01 - 2015/01/31
                // =======================================================================

                queryBar.dd.previous();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_MONTHLY, queryBar.type, "Query Bar Type sould be Monthly")
                compare(2, queryBar.offset, "Query Bar Offset sould be 2")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2016/01/01', 'Current Start Date should be 2016/01/01 for Previous x 2 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2016/01/31', 'Current Finish Date should be 2016/01/31 for Previous x 2 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2015/01/01', 'Previous Start Date should be 2015/01/01 for Previous x 2 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2015/01/31', 'Previous Finish Date should be 2015/01/31 for Previous x 2 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference  13-3-2016 Type: Monthly
                //
                //      Action:
                //          previous() -> previous() -> previous()
                //
                //      Expected:
                //          Current Start-Finish : 2015/12/01 - 2015/12/31
                //          Previous Start-Finish: 2014/12/01 - 2014/12/31
                // =======================================================================

                queryBar.dd.previous();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_MONTHLY, queryBar.type, "Query Bar Type sould be Monthly")
                compare(3, queryBar.offset, "Query Bar Offset sould be 3")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/12/01', 'Current Start Date should be 2015/12/01 for Previous x 3 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/12/31', 'Current Finish Date should be 2015/12/31 for Previous x 3 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/12/01', 'Previous Start Date should be 2014/12/01 for Previous x 3 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/12/31', 'Previous Finish Date should be 2014/12/31 for Previous x 3 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference  13-3-2016 Type: Monthly
                //
                //      Action:
                //          previous() -> previous() -> previous() -> next()
                //
                //      Expected:
                //          Current Start-Finish : 2016/01/01 - 2016/01/31
                //          Previous Start-Finish: 2015/01/01 - 2015/01/31
                // =======================================================================

                queryBar.dd.next();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_MONTHLY, queryBar.type, "Query Bar Type sould be Monthly")
                compare(2, queryBar.offset, "Query Bar Offset sould be 2")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2016/01/01', 'Current Start Date should be 2016/01/01 for Previous x 2 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2016/01/31', 'Current Finish Date should be 2016/01/31 for Previous x 2 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2015/01/01', 'Previous Start Date should be 2015/01/01 for Previous x 2 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2015/01/31', 'Previous Finish Date should be 2015/01/31 for Previous x 2 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference  20-4-2016 Type: Quarterly
                //
                //      Expected:
                //          Current Start/Finish : 2016/04/01 - 2016/06/30
                //          Previous Start/Finish: 2015/04/01 - 2015/06/30
                // =======================================================================

                queryBar.dd.reset();
                ++triggerCount;
                queryBar.setQuery(new Date(Date.UTC(2016, 3, 20)), null, PfmQueryBarPrivate.TYPE_QUARTERLY);

                refDate = DateTime.date('Y/m/d', queryBar.dd.startDateRef);
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_QUARTERLY, queryBar.type, "Query Bar Type sould be Quarterly")
                compare(0, queryBar.offset, "Query Bar Offset sould be 0")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2016/04/01', 'Current Start Date should be 2016/04/01 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2016/06/30', 'Current Finish Date should be 2016/06/30 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2015/04/01', 'Previous Start Date should be 2015/04/01 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2015/06/30', 'Previous Finish Date should be 2015/06/30 for Ref ' + refDate);

                // =======================================================================
                // Calculate for Reference  20-4-2016 Type: Quarterly
                //
                //      Action:
                //          previous()
                //
                //      Expected:
                //          Current Start-Finish : 2016/01/01 - 2016/03/31
                //          Previous Start-Finish: 2015/01/01 - 2015/03/31
                // =======================================================================

                queryBar.dd.previous();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_QUARTERLY, queryBar.type, "Query Bar Type sould be Quarterly")
                compare(1, queryBar.offset, "Query Bar Offset sould be 1")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2016/01/01', 'Current Start Date should be 2016/01/01 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2016/03/31', 'Current Finish Date should be 2016/03/31 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2015/01/01', 'Previous Start Date should be 2015/01/01 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2015/03/31', 'Previous Finish Date should be 2015/03/31 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should be query for Previous x 1 next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for Previous x 1 previous week.');

                // =======================================================================
                // Calculate for Reference  20-4-2016 Type: Quarterly
                //
                //      Action:
                //          previous() -> previous()
                //
                //      Expected:
                //          Current Start-Finish : 2015/10/01 - 2015/12/31
                //          Previous Start-Finish: 2014/10/01 - 2014/12/31
                // =======================================================================

                queryBar.dd.previous();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_QUARTERLY, queryBar.type, "Query Bar Type sould be Quarterly")
                compare(2, queryBar.offset, "Query Bar Offset sould be 2")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/10/01', 'Current Start Date should be 2015/10/01 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/12/31', 'Current Finish Date should be 2015/12/31 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/10/01', 'Previous Start Date should be 2014/10/01 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/12/31', 'Previous Finish Date should be 2014/12/31 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference  20-4-2016 Type: Quarterly
                //
                //      Action:
                //          previous() -> previous() -> previous()
                //
                //      Expected:
                //          Current Start-Finish : 2015/07/01 - 2015/09/30
                //          Previous Start-Finish: 2014/07/01 - 2014/09/30
                // =======================================================================

                queryBar.dd.previous();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_QUARTERLY, queryBar.type, "Query Bar Type sould be Quarterly")
                compare(3, queryBar.offset, "Query Bar Offset sould be 3")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/07/01', 'Current Start Date should be 2015/07/01 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/09/30', 'Current Finish Date should be 2015/09/30 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/07/01', 'Previous Start Date should be 2014/07/01 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/09/30', 'Previous Finish Date should be 2014/09/30 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference  20-4-2016 Type: Quarterly
                //
                //      Action:
                //          previous() -> previous() -> previous() -> next()
                //
                //      Expected:
                //          Current Start-Finish : 2015/10/01 - 2015/12/31
                //          Previous Start-Finish: 2014/10/01 - 2014/12/31
                // =======================================================================

                queryBar.dd.next();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_QUARTERLY, queryBar.type, "Query Bar Type sould be Quarterly")
                compare(2, queryBar.offset, "Query Bar Offset sould be 2")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/10/01', 'Current Start Date should be 2015/10/01 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/12/31', 'Current Finish Date should be 2015/12/31 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/10/01', 'Previous Start Date should be 2014/10/01 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/12/31', 'Previous Finish Date should be 2014/12/31 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference  10-3-2016 20-4-2016 Type: Custom
                //
                //      Expected:
                //          Current Start/Finish : 2016/03/10 - 2016/04/20
                //          Previous Start/Finish: 2015/03/10 - 2015/04/20
                // =======================================================================

                queryBar.dd.reset();
                ++triggerCount;
                queryBar.setQuery(new Date(Date.UTC(2016, 2, 10)), new Date(Date.UTC(2016, 3, 20)), PfmQueryBarPrivate.TYPE_CUSTOM);

                refDate = DateTime.date('Y/m/d', queryBar.dd.startDateRef) + " " +
                          DateTime.date('Y/m/d', queryBar.dd.finishDateRef)

                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_CUSTOM, queryBar.type, "Query Bar Type sould be Custom")
                compare(0, queryBar.offset, "Query Bar Offset sould be 0")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2016/03/10', 'Current Start Date should be 2016/03/10 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2016/04/20', 'Current Finish Date should be 2016/04/20 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2015/03/10', 'Previous Start Date should be 2015/03/10 for Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2015/04/20', 'Previous Finish Date should be 2015/04/20 for Ref ' + refDate);

                // =======================================================================
                // Calculate for Reference  10-3-2016 20-4-2016 Type: Custom
                //
                //      Action:
                //          previous()
                //
                //      Expected:
                //          Current Start-Finish : 2016/01/28 - 2016/03/09
                //          Previous Start-Finish: 2015/01/29 - 2015/03/10
                // =======================================================================

                queryBar.dd.previous();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_CUSTOM, queryBar.type, "Query Bar Type sould be Custom")
                compare(1, queryBar.offset, "Query Bar Offset sould be 1")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2016/01/28', 'Current Start Date should be 2016/01/28 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2016/03/09', 'Current Finish Date should be 2016/03/09 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2015/01/28', 'Previous Start Date should be 2015/01/28 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2015/03/10', 'Previous Finish Date should be 2015/03/10 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should be query for Previous x 1 next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for Previous x 1 previous week.');

                // =======================================================================
                // Calculate for Reference   10-3-2016 20-4-2016 Type: Custom
                //
                //      Action:
                //          previous() -> previous()
                //
                //      Expected:
                //          Current Start-Finish : 2015/12/17 - 2016/01/27
                //          Previous Start-Finish: 2014/12/17 - 2015/01/27
                // =======================================================================

                queryBar.dd.previous();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_CUSTOM, queryBar.type, "Query Bar Type sould be Custom")
                compare(2, queryBar.offset, "Query Bar Offset sould be 2")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/12/17', 'Current Start Date should be 2015/12/19 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2016/01/27', 'Current Finish Date should be 2015/01/27 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/12/17', 'Previous Start Date should be 2014/12/17 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2015/01/27', 'Previous Finish Date should be 2014/01/27 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference  20-4-2016 Type: Custom
                //
                //      Action:
                //          previous() -> previous() -> previous()
                //
                //      Expected:
                //          Current Start-Finish : 2015/11/05 - 2015/12/16
                //          Previous Start-Finish: 2014/11/05 - 2014/12/16
                // =======================================================================

                queryBar.dd.previous();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_CUSTOM, queryBar.type, "Query Bar Type sould be Custom")
                compare(3, queryBar.offset, "Query Bar Offset sould be 3")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/11/05', 'Current Start Date should be 2015/11/05 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2015/12/16', 'Current Finish Date should be 2015/12/16 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/11/05', 'Previous Start Date should be 2014/11/05 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2014/12/16', 'Previous Finish Date should be 2014/12/16 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

                // =======================================================================
                // Calculate for Reference  20-4-2016 Type: Custom
                //
                //      Action:
                //          previous() -> previous() -> previous() -> next()
                //
                //      Expected:
                //          Current Start-Finish : 2015/12/17 - 2016/01/27
                //          Previous Start-Finish: 2014/12/17 - 2015/01/27
                // =======================================================================

                queryBar.dd.next();
                query = queryBar.query();

                compare(queryBarSpy.count, ++triggerCount, 'queryChanged() signal should have triggered by now.');

                compare(PfmQueryBarPrivate.TYPE_CUSTOM, queryBar.type, "Query Bar Type sould be Custom")
                compare(2, queryBar.offset, "Query Bar Offset sould be 2")

                compare(DateTime.date('Y/m/d', query.current.start),
                        '2015/12/17', 'Current Start Date should be 2015/12/17 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.current.finish),
                        '2016/01/27', 'Current Finish Date should be 2015/01/27 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.start),
                        '2014/12/17', 'Previous Start Date should be 2014/12/17 for Previous x 1 Ref ' + refDate);

                compare(DateTime.date('Y/m/d', query.previous.finish),
                        '2015/01/27', 'Previous Finish Date should be 2014/01/27 for Previous x 1 Ref ' + refDate);

                verify(queryBar.dd.canQueryNextInternal(), 'Date should not be query for next week.');
                verify(queryBar.dd.canQueryPreviousInternal(), 'Date should be query for previous week.');

            }
        }
    }
}

