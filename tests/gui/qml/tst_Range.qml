import QtQuick 2.4
import QtTest 1.1
import Frame.Mobile.Core 1.0
import "../../../appvantage/res/gui"
import "../../../appvantage/res/gui/PfmQueryBarPrivate.js" as PfmQueryBarPrivate

TestContainer {
    property alias queryBar: queryBar
    property alias navigation: navigation

    id: container

    function changeQueryType(queryType, startDateCustom, finishDateCustom, yearBack) {
        d.queryType = queryType;
        d.startDateCustom = startDateCustom;
        d.finishDateCustom = finishDateCustom;
        d.yearBack = yearBack;
    }

    QtObject {
        id: d

        property int navigationCloseTrigger: 0
        property var queryType
        property var startDateCustom
        property var finishDateCustom
        property bool yearBack
    }

    Item {
        id: navigation

        signal close()

        SignalSpy {
            id: navigationSpy
            target: navigation
            signalName: 'close'
        }
    }

    PfmQueryBar {
        id: queryBar
    }

    Range {
        id: range
        opener: container

        TestCase {
            name: 'RangeTest'

            function test_order0_Week() {
                range.dd.unitTestMode = true;

                scene.rangeButtonText = 'Week';
                range.beforeOpen();
                compare(range.dd.tabView.currentIndex, 0, 'For week current index should be zero.');

                range.dd.localNav.okButtonClicked();
                range.dd.saveResult();
                compare(d.queryType, PfmQueryBarPrivate.TYPE_WEEKLY, 'Query type should be ' + PfmQueryBarPrivate.TYPE_WEEKLY);
                compare(navigationSpy.count, ++d.navigationCloseTrigger, 'Navigation should have triggered close by now.');
            }

            function test_order0_Month() {
                range.dd.unitTestMode = true;

                scene.rangeButtonText = 'Month';
                range.beforeOpen();
                compare(range.dd.tabView.currentIndex, 1, 'For momth current index should be 1.');

                range.dd.localNav.okButtonClicked();
                range.dd.saveResult();
                compare(d.queryType, PfmQueryBarPrivate.TYPE_MONTHLY, 'Query type should be ' + PfmQueryBarPrivate.TYPE_MONTHLY);
                compare(navigationSpy.count, ++d.navigationCloseTrigger, 'Navigation should have triggered close by now.');
            }

            function test_order0_Quarter() {
                range.dd.unitTestMode = true;

                scene.rangeButtonText = 'Quarter';
                range.beforeOpen();
                compare(range.dd.tabView.currentIndex, 2, 'For momth current index should be 2.');

                range.dd.localNav.okButtonClicked();
                range.dd.saveResult();
                compare(d.queryType, PfmQueryBarPrivate.TYPE_QUARTERLY, 'Query type should be ' + PfmQueryBarPrivate.TYPE_QUARTERLY);
                compare(navigationSpy.count, ++d.navigationCloseTrigger, 'Navigation should have triggered close by now.');
            }

            function test_order0_Custom() {
                range.dd.unitTestMode = true;

                scene.rangeButtonText = 'Custom';
                range.beforeOpen();
                compare(range.dd.tabView.currentIndex, 3, 'For momth current index should be 3.');

                range.dd.localNav.okButtonClicked();
                compare(navigationSpy.count, d.navigationCloseTrigger, 'Navigation should not have been triggered. It should have been give an error.');
            }
        }
    }
}

