import QtQuick 2.4
import Frame.Mobile 1.0
import "../../../appvantage/res/gui"

Item {
    property alias theme: theme
    property alias serverModel: serverModel
    property alias scene: scene

    id: applicationWindow

    width: 400
    height: 400

    function imagePath(value) {
        var scaleFactor = Math.min(4, ScreenInfo.imageScaleFactor);
        var imgPath = '../../../appvantage/res/img/' + scaleFactor + 'x/' + value;

        return imgPath;
    }

    function qmlPath(value) {
        return '../../../appvantage/res/gui/' + value;
    }

    function fontPath(value) {
        return '../../../appvantage/res/font/' + value;
    }

    function dp(px) {
        return ScreenInfo.dp(px);
    }

    function sp(px) {
        return ScreenInfo.sp(px);
    }

    PfmTheme {
        id: theme
    }

    Item {
        id: serverModel

        property var serverDate: new Date()
    }

    Item {
        id: scene

        property string rangeButtonText: 'Week'
    }
}

