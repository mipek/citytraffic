import QtQuick 2.4
import QtTest 1.0
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0
import com.triodor.pfm 1.0
import "../../../appvantage/res/gui/PfmController.js" as PfmController
import "../../../appvantage/res/gui"

TestContainer {

    PfmQueryBar {
        id: queryBar
    }

    Chart {
        id: chart
    }

    TestCase {
        name: 'PfmControllerTest'

        function initTestCase() {
            PfmController.setVersion(ApplicationInfo.version);
            PfmController.m_testMode = true;
            PfmController.m_mockMode = true;
            PfmController.m_mockResponseDelay = false;
            PfmController.bootstrap();
        }

        function cleanupTestCase() {
            DbHelper.deleteDatabase('TestDb');
        }

        function test_order0_login() {
            var userInfo = {
                username: 'test',
                password: '123',
                remember: true
            };

            verify(PfmController.getSavedUser() === null, 'There should not be saved user initially.');

            PfmController.login({
                responseLoginInfoFailed: function(errorId, code, message) {
                    fail(qsTr('Login request failed with error: %1 (%2) %3').arg(errorId).arg(code).arg(message));
                },

                responseLoginInfoSuccess: function(loginInfo) {
                    verify(Lang.isObjectLiteral(loginInfo), 'Login information should be object literal.');

                    verify(loginInfo.hasOwnProperty('token'), 'Login information should have token property.');
                    verify(Lang.isString(loginInfo.token), 'token property should be string');
                    verify(Lang.trim(loginInfo.token).length > 0, 'token should not be empty.');

                    verify(loginInfo.hasOwnProperty('user_id'), 'Login information should have user_id property.');
                    verify(Lang.isNumber(loginInfo.user_id), 'user_id property should be number');
                    verify(loginInfo.user_id > 0, 'user_id should not be greater then 0.');

                    var savedUser = PfmController.getSavedUser();
                    verify(savedUser !== null, 'User data should be saved into database.');
                    compare(savedUser.username, userInfo.username, 'User names are not matched.');
                    compare(savedUser.password, userInfo.password, 'Passwords are not matched.');
                }
            }, userInfo.username, userInfo.password, userInfo.remember);
        }

        function test_order1_locations() {
            // set initially to a known date
            queryBar.dd.startDateRef = new Date(Date.UTC(2015, 10, 23));

            var lockedRequest = false;
            var unlockedRequest = false;
            var calculationsCompleted = null;
            var responseSuccess = false;
            var sensorVisitDataSuccess = false;
            var query = queryBar.query();

            PfmController.init({
                responseLocationDataSuccess: function() {
                    responseSuccess = true;
                },
                responseLocationDataFailed: function(errorId, code, message) {
                    fail(qsTr('Location request failed with error: %1 (%2) %3').arg(errorId).arg(code).arg(message));
                },
                responseSensorVisitDataSuccess: function() {
                    sensorVisitDataSuccess = true;
                },
                responseSensorVisitDataFailed: function(locationId, errorId, code, message) {
                    fail(qsTr('Sensor visit data request failed for location id %1 with error: %2 (%3) %4').arg(locationId).arg(errorId).arg(code).arg(message));
                },
                lockRequest: function() {
                    lockedRequest = true;
                },
                unlockRequest: function() {
                    unlockedRequest = true;
                },
                calculationCompleted: function(dataType, locationId) {
                    compare(dataType, PfmController.DATA_TYPE_SENSOR, 'Data Type should have been sensor.');
                    if (calculationsCompleted === null)
                        calculationsCompleted = {};

                    calculationsCompleted[locationId] = true;
                }
            }, query);

            verify(responseSuccess, 'Response is not resulted with success.');
            verify(sensorVisitDataSuccess, 'Sensor visit data success should have been called.');
            verify(lockedRequest, 'Locked request should have called.');
            verify(unlockedRequest, 'Unlock request should have called.');
            verify(PfmController.m_model.count > 0, 'Pfm Model should have been filled with locations.');

            var locations = PfmController.m_pfmDataManager.m_sqlManager.requestLocationData();
            var cache = PfmController.m_pfmDataManager.m_memoryManager.requestLocationData();
            var locationCount = 0;
            var current;
            var locationIndex;
            var currentVisitCount;
            var previousVisitCount;
            var dateList;
            var sensorVisitData = null;
            var total = 0;
            for (var locationId in locations) {
                if (locations.hasOwnProperty(locationId)) {
                    locationCount++;

                    locationIndex = PfmController.m_modelMap[locationId];
                    current = PfmController.m_model.get(locationIndex);

                    verify(current !== undefined && current !== null, 'Location is not in model.');
                    compare('' + current.locationId, locationId, 'Locations are not matched with model.');
                    compare(current.name, locations[locationId].name, 'Location names are not matched with model.');
                    verify(calculationsCompleted.hasOwnProperty(locationId), 'Calculation should have been completed for this location');
                    verify(cache.hasOwnProperty(locationId), 'Location should have been in the memory.');

                    currentVisitCount = current.currentVisitCount;
                    previousVisitCount = current.previousVisitCount;

                    dateList = PfmController.getDateList(query.current.start, query.current.finish);
                    sensorVisitData = PfmController.m_pfmDataManager.m_memoryManager.requestSensorVisitData(locationId, null, dateList);

                    verify(Lang.isObjectLiteral(sensorVisitData), 'Sensor Visit data should have been an Javascript Object.');
                    verify(Lang.isObjectLiteral(sensorVisitData.data), 'Sensor Visit data should contain data property as in Javascript Object.')

                    total = 0;
                    for (var visitDate in sensorVisitData.data) {
                        if (!sensorVisitData.data.hasOwnProperty(visitDate))
                            continue;

                        total += sensorVisitData.data[visitDate].total_count;
                    }

                    compare(current.currentVisitCount, total, 'Current visit total is not matched with Total visit total.');

                    dateList = PfmController.getDateList(query.previous.start, query.previous.finish);
                    sensorVisitData = PfmController.m_pfmDataManager.m_memoryManager.requestSensorVisitData(locationId, null, dateList);

                    verify(Lang.isObjectLiteral(sensorVisitData), 'Sensor Visit data should have been an Javascript Object.');
                    verify(Lang.isObjectLiteral(sensorVisitData.data), 'Sensor Visit data should contain data property as in Javascript Object.')

                    total = 0;
                    for (visitDate in sensorVisitData.data) {
                        if (!sensorVisitData.data.hasOwnProperty(visitDate))
                            continue;

                        total += sensorVisitData.data[visitDate].total_count;
                    }

                    compare(current.previousVisitCount, total, 'Previous visit total is not matched with Total visit total.');
                }
            }

            compare(PfmController.m_model.count, locationCount, 'Location counts are not matched with model.');
            verify(calculationsCompleted !== null, 'Calculations should have been completed for all locations.');
        }

        function test_order2_wifiVisitData() {
            var lockedRequest = false;
            var unlockedRequest = false;
            var calculationsCompleted = null;
            var wifiVisitDataSuccess = false;
            var wifiGranularityDataSuccess = false;
            var query = queryBar.query();

            PfmController.updateVisitCounts(PfmController.DATA_TYPE_WIFI, PfmController.GRANULARITY_WEEKLY, {
                lockRequest: function() {
                    lockedRequest = true;
                },
                unlockRequest: function() {
                    unlockedRequest = true;
                },
                calculationCompleted: function(dataType, locationId) {
                    compare(dataType, PfmController.DATA_TYPE_WIFI, 'Data Type should have been wifi.');
                    if (calculationsCompleted === null)
                        calculationsCompleted = {};

                    calculationsCompleted[locationId] = true;
                },
                responseWifiVisitDataSuccess: function() {
                    wifiVisitDataSuccess = true;
                },
                responseWifiVisitDataFailed: function(locationId, errorId, code, message) {
                    fail(qsTr('Wifi visit data request failed for location id %1 with error: %2 (%3) %4').arg(locationId).arg(errorId).arg(code).arg(message));
                },
                responseWifiGranularityDataSuccess: function() {
                    wifiGranularityDataSuccess = true;
                },
                responseWifiGranularityDataFailed: function(locationId, errorId, code, message) {
                    fail(qsTr('Wifi Granularity visit data request failed for location id %1 with error: %2 (%3) %4').arg(locationId).arg(errorId).arg(code).arg(message));
                }
            }, query, true);

            verify(wifiVisitDataSuccess, 'Wifi visit data success should have been called.');
            verify(wifiGranularityDataSuccess, 'Wifi granularitydata success should have been called.');
            verify(lockedRequest, 'Locked request should have called.');
            verify(unlockedRequest, 'Unlock request should have called.');

            var locations = PfmController.m_pfmDataManager.m_memoryManager.requestLocationData();
            var locationCount = 0;
            var current;
            var locationIndex;
            var currentVisitCount;
            var previousVisitCount;
            var dateList;
            var wifiVisitData = null;
            var wifiGranularityData = null;
            var totalCurrent = 0;
            var queryCurrentStart = PfmController.getDateQueryString(query.current.start);
            var queryCurrentWeekFinish = PfmController.getDateQueryString(PfmController.getFinishDateOfGranularity(query.current.start, PfmController.GRANULARITY_WEEKLY));

            var queryPreviousStart = PfmController.getDateQueryString(query.previous.start);
            var queryPreviousWeekFinish = PfmController.getDateQueryString(PfmController.getFinishDateOfGranularity(query.previous.start, PfmController.GRANULARITY_WEEKLY));

            for (var locationId in locations) {
                if (locations.hasOwnProperty(locationId)) {
                    locationCount++;

                    locationIndex = PfmController.m_modelMap[locationId];
                    current = PfmController.m_model.get(locationIndex);

                    verify(current !== undefined && current !== null, 'Location is not in model.');
                    compare('' + current.locationId, locationId, 'Locations are not matched with model.');
                    compare(current.name, locations[locationId].name, 'Location names are not matched with model.');
                    verify(calculationsCompleted.hasOwnProperty(locationId), 'Calculation should have been completed for this location');

                    currentVisitCount = current.currentVisitCount;
                    previousVisitCount = current.previousVisitCount;

                    dateList = PfmController.getDateList(query.current.start, query.current.finish);
                    wifiVisitData = PfmController.m_pfmDataManager.m_memoryManager.requestWifiVisitData(locationId, null, dateList);

                    verify(Lang.isObjectLiteral(wifiVisitData), 'Wifi Visit data should have been an Javascript Object.');
                    verify(Lang.isObjectLiteral(wifiVisitData.data), 'Wifi Visit data should contain data property as in Javascript Object.');

                    wifiGranularityData = PfmController.m_pfmDataManager.m_memoryManager.getWifiGranularityData();

                    verify(Lang.isObjectLiteral(wifiGranularityData), 'Wifi Granularity data should have been an Javascript Object.');
                    verify(wifiGranularityData.hasOwnProperty(PfmController.GRANULARITY_WEEKLY), 'Wifi Granularity at this point should have been Weekly.');
                    verify(Lang.isObjectLiteral(wifiGranularityData[PfmController.GRANULARITY_WEEKLY]), 'Wifi Granularity Weekly data should have been an Javascript Object.');
                    verify(wifiGranularityData[PfmController.GRANULARITY_WEEKLY].hasOwnProperty(locationId), 'Wifi Granularity Weekly data does not cached with the location id.');
                    verify(Lang.isObjectLiteral(wifiGranularityData[PfmController.GRANULARITY_WEEKLY][locationId]), 'Wifi Granularity Weekly location data should have been an Javascript Object.');
                    verify(wifiGranularityData[PfmController.GRANULARITY_WEEKLY][locationId].hasOwnProperty(queryCurrentStart + '|' + queryCurrentWeekFinish), 'Wifi Granularity Weekly location data does not cached with the current date: ' + queryCurrentStart);
                    verify(wifiGranularityData[PfmController.GRANULARITY_WEEKLY][locationId].hasOwnProperty(queryPreviousStart + '|' + queryPreviousWeekFinish), 'Wifi Granularity Weekly location data does not cached with the previous date: ' + queryPreviousStart);
                }
            }

            compare(PfmController.m_model.count, locationCount, 'Location counts are not matched with model.');
            verify(calculationsCompleted !== null, 'Calculations should have been completed for all locations.');
        }
    }

    function test_order3_chart() {
        var locations = PfmController.m_pfmDataManager.m_memoryManager.requestLocationData();
        var sensorVisitData;
        var query = queryBar.query();
        var currentDateList = PfmController.getDateList(query.current.start, query.current.finish);
        var previousDateList = PfmController.getDateList(query.previous.start, query.previous.finish);
        var totalCurrent;
        var totalPrevious;
        var visitDate;
        var currentWifiGranularityData;
        var previousWifiGranularityData;

        var currentStartDate = PfmController.getDateQueryString(query.current.start);
        var currentFinishDate = PfmController.getDateQueryString(query.current.finish);

        var previousStartDate = PfmController.getDateQueryString(query.previous.finish);
        var previousFinishDate = PfmController.getDateQueryString(query.previous.finish);

        for (var locationId in locations) {
            if (locations.hasOwnProperty(locationId)) {
                chart.setLocationId(locationId);
                chart.dd.queryBar.fromDay = queryBar.fromDay;

                sensorVisitData = PfmController.m_pfmDataManager.m_memoryManager.requestSensorVisitData(locationId, null, currentDateList);
                totalCurrent = 0;
                for (visitDate in sensorVisitData.data) {
                    if (!sensorVisitData.data.hasOwnProperty(visitDate))
                        continue;

                    totalCurrent += sensorVisitData.data[visitDate].total_count;
                }

                compare(chart.dd.totalVisit.current, Numeral.numeral(totalCurrent).format('0,0'), 'Chart Sensor Visit Current Total Data is not matched.');

                sensorVisitData = PfmController.m_pfmDataManager.m_memoryManager.requestSensorVisitData(locationId, null, previousDateList);
                totalPrevious = 0;
                for (visitDate in sensorVisitData.data) {
                    if (!sensorVisitData.data.hasOwnProperty(visitDate))
                        continue;

                    totalPrevious += sensorVisitData.data[visitDate].total_count;
                }

                compare(chart.dd.totalVisit.previous, Numeral.numeral(totalPrevious).format('0,0'), 'Chart Sensor Visit Previous Data is not matched.');
                compare(chart.dd.totalVisit.avg, PfmController.getPercentageOfOverview(totalCurrent, totalPrevious), 'Chart Sensor Visit AVG is not matched.');

                currentWifiGranularityData = PfmController.m_pfmDataManager.m_memoryManager.requestWifiGranularityData(PfmController.GRANULARITY_WEEKLY, locationId, currentStartDate, currentFinishDate);
                previousWifiGranularityData = PfmController.m_pfmDataManager.m_memoryManager.requestWifiGranularityData(PfmController.GRANULARITY_WEEKLY, locationId, previousStartDate, previousFinishDate);

                compare(chart.dd.newVisit.current, currentWifiGranularityData.new_count, '[NewVisit] Wifi Granularity Current Data is not matched.');
                compare(chart.dd.newVisit.previous, previousWifiGranularityData.new_count, '[NewVisit] Wifi Granularity Previous Data is not matched.');
                compare(chart.dd.newVisit.avg, Number(currentWifiGranularityData.total_count === 0 ? 0 : currentWifiGranularityData.new_count / currentWifiGranularityData.total_count).toFixed(1), '[NewVisit] Wifi Granularity Current AVG is not matched.');

                var currentDwellTime = { formatted: FrameCore.DateTime.timeFormatter('?d ?h ?m !s', currentWifiGranularityData.dwell_time), value: currentWifiGranularityData.dwell_time };
                var previousDwellTime = { formatted: FrameCore.DateTime.timeFormatter('?d ?h ?m !s', previousWifiGranularityData.dwell_time), value: previousWifiGranularityData.dwell_time };

                compare(chart.dd.dwellTime.current, currentDwellTime.formatted, '[DwellTime] Wifi Granularity Current Data is not matched.');
                compare(chart.dd.dwellTime.previous, previousDwellTime.formatted, '[DwellTime] Wifi Granularity Previous Data is not matched.');
                compare(chart.dd.dwellTime.avg, Number(PfmController.getPercentageOfOverview(currentDwellTime.value, previousDwellTime.value)).toFixed(1), '[DwellTime] Wifi Granularity Current AVG is not matched.');

                compare(chart.dd.uniqueVisit.current, currentWifiGranularityData.unique_count, '[UniqueVisit] Wifi Granularity Current Data is not matched.');
                compare(chart.dd.uniqueVisit.previous, previousWifiGranularityData.unique_count, '[UniqueVisit] Wifi Granularity Previous Data is not matched.');
                compare(chart.dd.uniqueVisit.avg, Number(currentWifiGranularityData.total_count === 0 ? 0 : currentWifiGranularityData.unique_count / currentWifiGranularityData.total_count).toFixed(1), '[UniqueVisit] Wifi Granularity Current AVG is not matched.');
            }
        }
    }
}

