#include <QApplication>
#include <QtQuickTest/quicktest.h>
#include <QQuickView>
#include <QQmlEngine>
#include <QLoggingCategory>
#include <QQmlApplicationEngine>
#include "appvantage/src/applicationplugin.h"
#include <Frame/applicationinfo.h>


int main(int argc, char *argv[])
{
#if !defined(DEV_MODE) || DEV_MODE == 0
    QLoggingCategory::setFilterRules("*=false");
#endif

    QApplication *app = 0;
    if (!QCoreApplication::instance())
        app = new QApplication(argc, argv);

    ApplicationPlugin::registerTypes();

    // set application info
    ApplicationInfo *pfmApplication = ApplicationInfo::instance();
    pfmApplication->setVersion(QLatin1String(APP_VERSION));

    int r = quick_test_main(argc, argv, "FluconGui", PFM_QML_TEST_SOURCE_DIR);
    delete app;

    return r;
}
