TEMPLATE = app
TARGET = citytraffic

QT += qml quick core-private sql widgets charts frame framesecurity frameanalytics


# Pre Configure
PROJECT_HOME = $$clean_path($${PWD})
PROJECT_FOLDER = $$basename(PROJECT_HOME)

ANDROID_HOME = $$(ANDROID_HOME)
QT_PATH = $$[QT_INSTALL_PREFIX]

# needs to be filled by own configuration
QML_PATH =
FONT_PATH =
IMAGE_PATH =
ANALYTICS_TRACKING_NUMBER =

# set developer mode
DEV_MODE = 0
LOCAL_DEV = 0
CONSOLE_DEV = 0

CONFIG(debug, debug|release)|desktop: LOCAL_DEV = 1
CONFIG(debug, debug|release): DEV_MODE = 1

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

CONFIG += qtquickcompiler

# Project Structure
include(src/src.pri)
include(res/res.pri)
include(native/native.pri)
include(version.pri)

# Default rules for deployment.
include(deployment.pri)

# Main App
include(app/app.pri)

# Compiler Flags
DEFINES += \
    FRAME_QML_PATH="\\\"$$QML_PATH\\\"" \
    FRAME_FONT_PATH="\\\"$$FONT_PATH\\\"" \
    FRAME_IMAGE_PATH="\\\"$$IMAGE_PATH\\\"" \
    IS_MOBILE=$$IS_MOBILE \
    DEV_MODE=$$DEV_MODE \
    CONSOLE_DEV=$$CONSOLE_DEV \
    ANALYTICS_TRACKING_NUMBER="\\\"$$ANALYTICS_TRACKING_NUMBER\\\""

