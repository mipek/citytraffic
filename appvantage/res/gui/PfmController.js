.pragma library
.import Frame.Mobile 1.0 as Frame
.import Frame.Mobile.Core 1.0 as FrameCore
.import "PfmDataManager.js" as PfmDataManager

var TAG = '[PfmController]';

var PARTIAL = 'partial';
var COMPLETED = 'completed';
var NOCHANGE = 'nochange';

var ERROR_INVALID_SERVER_RESPONSE = 'invalidServerResponse';
var ERROR_SERVER_ERROR = 'serverError';
var ERROR_NETWORK = 'networkError';

var DATA_TYPE_SENSOR = 'sensor';
var DATA_TYPE_WIFI = 'wifi';

var GRANULARITY_NONE = -1;
var GRANULARITY_DAILY = 0;
var GRANULARITY_WEEKLY = 1;
var GRANULARITY_MONTHLY = 2;
var GRANULARITY_QUARTERLY = 3;
var GRANULARITY_YEARLY = 4;
var GRANULARITY_CUSTOM = 5;

var TIME_AXIS_DAILY = 0;
var TIME_AXIS_MONTHLY = 1;
var TIME_AXIS_WEEKLY = 2;

var STATE_ERROR = -2;
var STATE_UNLOADED = -1;
var STATE_LOADING = 0;
var STATE_LOADED_PREVIOUS = 1;
var STATE_LOADED_ALL = 2;

var OPTION_HAS_SENSOR = 1;
var OPTION_HAS_WIFI = 2;

var APPLICATION_NO_OPTIONS = 0;
var APPLICATION_CLEAR_CACHES = 1;
var APPLICATION_DONT_USE_DATABASE = 2;

var m_mockMode = false;
var m_mockResponseDelay = true;
var m_testMode = false;

var m_boostrapped = false;

var m_model = Qt.createQmlObject("import QtQuick 2.4; ListModel {}", Qt.application);
var m_cryptoUtil = Qt.createQmlObject("import com.triodor.pfm 1.0; CryptoUtil {}", Qt.application);

var m_pfmDataManager;
var m_modelMap = {};
var m_shared = {};

var m_version = '1.2.6';

var m_syncLock = false;

var landPageAlias = undefined
var versionData

function showUpdateMessage(){
    if(landPageAlias){
        landPageAlias.updateDialog.simpleConfirm()
    }
}


function controlVersion(){
    if(versionData){
        var versiondata2 = JSON.parse(versionData)
        console.log("version data : ",JSON.stringify(versiondata2))
        console.log("versionData[m_version] : ",versiondata2[m_version])
        console.log("m_version : ",m_version)
        if(versiondata2[m_version]){
            return true;
        }
        else{
            showUpdateMessage()
            return false
        }
    }
    return true
}


var m_apiIndexes = [
            //{ name: 'Master', path: 'http://retaildata.pfm-intelligence.com:8090/' },
            { name: 'Master', path: 'https://api.citytraffic.nl/' },
            { name: 'Demo', path: 'http://retaildemo.pfm-intelligence.com:9000/' },
            { name: 'Test WAN', path: 'http://46.234.9.184/PFMAPI/' },
            { name: 'Test Network', path: 'http://192.168.12.173/PFMAPI/' },
        ];

var m_selectedApiIndex = 0;
var m_useDatabase = m_selectedApiIndex === 0;
var m_dbDataCache = m_useDatabase;

var m_apiPath = m_apiIndexes[m_selectedApiIndex].path;
var m_apiName = m_apiIndexes[m_selectedApiIndex].name;

var m_totalHandler = {
    wifi: {
        dwell_time: function(dataCache) {
            var total = 0;
            var dwellTime = 0;
            var current;
            var value = 0;
            for (var date in dataCache) {
                if (!dataCache.hasOwnProperty(date))
                    continue;

                current = dataCache[date];
                dwellTime += current.dwell_time;
                total++;
            }

            if (total === 0 || dwellTime === 0)
                value = 0;
            else
                value = Math.floor(dwellTime / total);

            return { formatted: FrameCore.DateTime.timeFormatter('?d ?h ?m !s', value), value: value };
        }
    }
};

var m_atomicHandler = {
    /*wifi: {
        unique_count: function(data) {
            return data.total_count === 0 ? 0 : (data.unique_count / data.total_count);
        },

        new_count: function(data) {
            return data.total_count === 0 ? 0 : (data.new_count / data.total_count) * 100;
        }
    }*/
};

function setVersion(version) {
    m_version = version;
}

function bootstrap() {
    if (m_boostrapped)
        return;

    m_boostrapped = true;
    var db = FrameCore.Db.open({
                                   dbName: m_testMode ? 'TestDb' : 'DefaultDb',
                                                        version: m_version,
                                                        dbVersionScriptPaths: ['qrc:/res/dbpatch']
                               });
    m_pfmDataManager = new PfmDataManager.Adapter(this);
    m_pfmDataManager.useDatabase(m_useDatabase, m_dbDataCache);
}

function setApiIndex(apiIndex) {
    m_selectedApiIndex = apiIndex;
    m_apiPath = m_apiIndexes[apiIndex].path;
    m_apiName = m_apiIndexes[apiIndex].name;
    m_useDatabase = m_selectedApiIndex === 0;

    if (m_boostrapped)
        m_pfmDataManager.useDatabase(m_useDatabase, m_dbDataCache);
}

function getModel() {
    return m_model;
}

function getMockMode() {
    return m_mockMode;
}

function setMockMode(value, noDelay) {
    m_mockMode = value ? true : false;
    if (m_mockMode)
        m_useDatabase = false;
    else
        m_useDatabase = m_selectedApiIndex === 0;

    if (noDelay !== undefined)
        m_mockResponseDelay = noDelay ? false : true;

    if (m_boostrapped)
        m_pfmDataManager.useDatabase(m_useDatabase);
}

function setSharedData(key, value) {
    m_shared[key] = value;
}

function getSharedData(key, defaultValue) {
    return m_shared.hasOwnProperty(key) ? m_shared[key] : defaultValue;
}

function getLocationById(locationId) {
    return (!m_pfmDataManager.hasLocationData() || !m_pfmDataManager.getLocationData().hasOwnProperty(locationId))
            ? null : m_pfmDataManager.getLocationData()[locationId];
}

function resetModel() {
    m_model.clear();
    m_modelMap = {};
}

function _postMock(url, callback, data) {
    var mock = { Data: data, Meta: { Code: 200, Message: 'OK' }, ExecutionTime: 20 };
    if (m_mockResponseDelay) {
        FrameCore.Util.setTimeout(function() {
            callback(JSON.stringify(mock), { statusCode: 200 }, url);
        }, FrameCore.Util.rand(500, 3000));
    } else {
        callback(JSON.stringify(mock), { statusCode: 200 }, url);
    }
}

function getDateList(startDate, finishDate, type) {
    var dateList = [];
    fromStartToFinish(startDate, finishDate, function(currentDate, currentDay) {
        dateList.push(FrameCore.DateTime.date('Y-m-d', currentDate));
    }, type);

    return dateList;
}

function getDateListFromObject(data) {
    var dateList = [];
    for (var date in data) {
        if (data.hasOwnProperty(date))
            dateList.push(date);
    }

    return dateList;
}

function getTotalCount(dataType, data, field) {
    var handler = m_totalHandler.hasOwnProperty(dataType) ? m_totalHandler[dataType][field] : 'linear';
    var total = 0;
    if (handler === 'linear' || !handler) {
        var current;
        for (var date in data) {
            if (!data.hasOwnProperty(date))
                continue;

            current = data[date];
            total += current[field];
        }
    } else {
        total = handler(data);
    }

    return total;
}

function getFinishDateOfGranularity(start, type) {
    var finish = new Date();
    finish.setTime(start.getTime());

    switch (type) {
    case GRANULARITY_WEEKLY:
        finish.setTime(finish.getTime() + 86400000 * 6);
        break;
    case GRANULARITY_MONTHLY:
        finish.setDate((new Date(Date.UTC(finish.getFullYear(), finish.getMonth() + 1, 0)).getDate()));
        break;
    case GRANULARITY_QUARTERLY:
        finish.setMonth(finish.getMonth() + 2);
        finish.setDate((new Date(Date.UTC(finish.getFullYear(), finish.getMonth() + 1, 0)).getDate()));
        break;
    case GRANULARITY_YEARLY:
        finish.setDate(finish.getTime() + (finish.getFullYear() % 4 ? 86400000 * 365 : 86400000 * 366));
        break;
    }

    return finish;
}

function api(method, path, callback, data, token) {
    var url = m_apiPath + path;
    var network = new XMLHttpRequest();
    var timer = null;

    network.open(method, url, true);
    if (method.toLowerCase() === 'post')
        network.setRequestHeader('Content-Type', 'application/json');

    if (token !== undefined)
        network.setRequestHeader('Authorization-Token', token);

    network.setRequestHeader('Application-Version', m_version);
    network.onreadystatechange = function() {
        if (XMLHttpRequest.DONE !== network.readyState)
            return;

        console.log(TAG, 'STATUS:', network.status, network.responseText, url, JSON.stringify(data));
        if (typeof callback === 'function')
            callback(network.responseText, network, url, network.status);

        if (timer !== null) {
            timer.stop();
            timer.destroy();
        }
    };

    var post = null;
    if (data) {
        post = [];
        for (var i in data)
            post.push(i + '=' + encodeURIComponent(data[i]));

        post = post.join('&');
        post = JSON.stringify(data);
    }

    if (!m_mockMode) {
        console.log(TAG, 'Sending Post through network:', post, url)
        network.send(post);

        timer = FrameCore.Util.setTimeout(function() {
            if (network.readyState !== XMLHttpRequest.DONE) {
                console.log(TAG, 'Network did not respond. Destroying for url:', path);
                network.abort();
            }
        }, 15000); // after 15 seconds destroy the network

    } else if (FrameCore.Lang.isFunction(callback)) {
        console.log(TAG, 'Sending Post with mock:', post);

        var startDate;
        var finishDate;
        var mockData = [];
        var k;
        var tmp;
        var currentDay;
        var locSeed = parseInt(data.Data.loc, 10) % 1000;
        var mockIndex = 0;
        var mockEven = 0;
        var mockWifiTemplate = [
                    {
                        "dwelltime": 3383,
                        "date": "2015-10-12T00:00:00",
                        "frequency": 1.0099999904632568,
                        "capturerate": 0,
                        "uniqueper": 98.9,
                        "newper": 19.3
                    },
                    {
                        "dwelltime": 3589,
                        "date": "2015-10-17T00:00:00",
                        "frequency": 1.0099999904632568,
                        "capturerate": 0,
                        "uniqueper": 99.2,
                        "newper": 28.9
                    },
                    {
                        "dwelltime": 3356,
                        "date": "2015-10-13T00:00:00",
                        "frequency": 1.0099999904632568,
                        "capturerate": 0,
                        "uniqueper": 99.2,
                        "newper": 23.1
                    },
                    {
                        "dwelltime": 4038,
                        "date": "2015-10-14T00:00:00",
                        "frequency": 1,
                        "capturerate": 0,
                        "uniqueper": 99.8,
                        "newper": 22.2
                    },
                    {
                        "dwelltime": 3881,
                        "date": "2015-10-15T00:00:00",
                        "frequency": 1,
                        "capturerate": 0,
                        "uniqueper": 99.8,
                        "newper": 26.6
                    },
                    {
                        "dwelltime": 3653,
                        "date": "2015-10-16T00:00:00",
                        "frequency": 1.0099999904632568,
                        "capturerate": 0,
                        "uniqueper": 98.9,
                        "newper": 26.3
                    },
                    {
                        "dwelltime": 82289,
                        "date": "2015-10-18T00:00:00",
                        "frequency": 1,
                        "capturerate": 0,
                        "uniqueper": 100,
                        "newper": 0
                    }
                ];
        var mockWifiTemplateEven = [
                    {
                        "dwelltime": 0,
                        "date": "2015-10-11T00:00:00",
                        "frequency": 0,
                        "capturerate": 0,
                        "uniqueper": 0,
                        "newper": 0
                    },
                    {
                        "dwelltime": 3154,
                        "date": "2015-10-08T00:00:00",
                        "frequency": 1,
                        "capturerate": 0,
                        "uniqueper": 99.8,
                        "newper": 22.4
                    },
                    {
                        "dwelltime": 3689,
                        "date": "2015-10-07T00:00:00",
                        "frequency": 1.0199999809265137,
                        "capturerate": 0,
                        "uniqueper": 98.5,
                        "newper": 25.9
                    },
                    {
                        "dwelltime": 3589,
                        "date": "2015-10-09T00:00:00",
                        "frequency": 1.0099999904632568,
                        "capturerate": 0,
                        "uniqueper": 98.7,
                        "newper": 24.4
                    },
                    {
                        "dwelltime": 3637,
                        "date": "2015-10-10T00:00:00",
                        "frequency": 1,
                        "capturerate": 0,
                        "uniqueper": 99.6,
                        "newper": 27.2
                    },
                    {
                        "dwelltime": 4142,
                        "date": "2015-10-05T00:00:00",
                        "frequency": 1.0099999904632568,
                        "capturerate": 0,
                        "uniqueper": 98.9,
                        "newper": 22.5
                    },
                    {
                        "dwelltime": 3438,
                        "date": "2015-10-06T00:00:00",
                        "frequency": 1.0099999904632568,
                        "capturerate": 0,
                        "uniqueper": 99.2,
                        "newper": 25.5
                    }
                ];

        if (path.indexOf('GetLocations') > -1) {
            mockData = [
                        { id: 400132, name: 'Maltepe Park', openingtime: 900, closingtime: 2100, options: 1 },
                        { id: 400148, name: 'Optimum', openingtime: 900, closingtime: 2100, options: 3 },
                        { id: 400149, name: 'Tepe Nautilus', openingtime: 900, closingtime: 2100, options: 1 },
                        { id: 400161, name: 'Akasya AVM', openingtime: 900, closingtime: 2100, options: 3 },
                        { id: 400194, name: 'Brandium', openingtime: 900, closingtime: 2100, options: 1 },
                        { id: 400179, name: 'Mall of Istanbul', openingtime: 900, closingtime: 2100, options: 3 }
                    ];

        } else if (path.indexOf('GetVisitCountByGranularity') > -1) {
            mockData.push({
                              TotalVisits: 429,
                              StartDate: data.Data.start
                          });

        } else if (path.indexOf('GetWifiDataByGranularity') > -1) {
            mockData.push({
                              capturerate: 0.0,
                              date:        data.Data.start,
                              uniqueper:   85.6,
                              newper:      23.3,
                              frequency:   1.1200000047683716,
                              dwelltime:   9512
                          });
        } else if (path.indexOf('GetVisitCount') > -1) {

            startDate = new Date(data.Data.start.split('T')[0]);
            finishDate = new Date(data.Data.end.split('T')[0]);

            fromStartToFinish(startDate, finishDate, function(current, currentDay) {
                mockData.push({
                                  TotalVisits: locSeed + currentDay * 100,
                                  StartDate: FrameCore.DateTime.date('Y-m-d', current) + 'T00:00:00'
                              });
            });

        } else if (path.indexOf('GetWifiData') > -1) {

            startDate = new Date(data.Data.start.split('T')[0]);
            finishDate = new Date(data.Data.end.split('T')[0]);

            mockIndex = 0;
            fromStartToFinish(startDate, finishDate, function(current, currentDay) {
                var data = JSON.parse(JSON.stringify((++mockEven % 2 ? mockWifiTemplate[mockIndex++] : mockWifiTemplateEven[mockIndex++])));
                data.date = FrameCore.DateTime.date('Y-m-d', current) + 'T00:00:00';

                mockData.push(data);
            });
        } else if (path.indexOf('Authenticate') > -1) {
            mockData = {
                Succeed:    true,
                UserId:     3,
                Token:      { Token: 'd619190f-d79c-4122-b66a-3a186872b10e' },
                ServerTime: ((new Date()).getTime() * 10000) + 621355968000000000
            };
        }

        console.log('Response Mock:', JSON.stringify(mockData));
        _postMock(url, callback, mockData);
    }
}

function handleResponse(params, data, validateCallback) {
    console.log("Data: " + data);
    data = parseJSON(data);
    if (isValidResponse(data)) {
        if(landPageAlias){
            console.log("message : ",data.Message)
            landPageAlias.popMessage("Error",data.Message)
        }

        var meta = (data && data.Meta) ? data.Meta : { Code: 0, Message: '' };
        console.log(params.onFailed)
        params.onFailed(ERROR_INVALID_SERVER_RESPONSE, meta.Code, meta.Message);


        return;
    }


    if (FrameCore.Lang.isFunction(validateCallback) && validateCallback(params, data) === true)
        return; // accepted.

    params.onSuccess(data.Data, params);
}

function isValidResponse(data) {
    return (!FrameCore.Lang.isObjectLiteral(data)
            || !data.hasOwnProperty('Meta')
            || !FrameCore.Lang.isObjectLiteral(data.Meta)
            || data.Meta.Code !== 200);
}

function parseJSON(data) {
    try {
        data = JSON.parse(data);
    } catch (e) {
        console.log(TAG, 'JSON Parse Error:', e);
        data = null;
    }

    return data;
}

function applyMaintenance(options) {
    if (options & APPLICATION_CLEAR_CACHES) {
        var version = Frame.Utility.getSetting('maintenance/clearCachedVersion');
        if (version === undefined || version !== m_version) {
            console.log(TAG, 'Clearing data cache because of a regression:', m_version);

            clearData();
            Frame.Utility.saveSetting('maintenance/clearCachedVersion', m_version);
        } else {
            console.log(TAG, 'Clearing data cache applied once:', m_version);
        }
    }

    if (options & APPLICATION_DONT_USE_DATABASE) {
        console.log(TAG, 'Database cannot use for data cache for this version:', m_version);

        m_useDatabase = true;
        m_dbDataCache = false;
        if (m_boostrapped)
            m_pfmDataManager.useDatabase(m_useDatabase, m_dbDataCache);
    }
}

function maintenanceCheck() {
    var options = Frame.Utility.getSetting('maintenance/options');
    var maintenanceVersion = Frame.Utility.getSetting('maintenance/version');
    if (maintenanceVersion !== m_version)
        options = undefined;

    //TODO
    api('GET', 'options.txt', function(data, xhr, url, statusCode) {
        console.log("data : ",data)
        versionData = data
        if (options === undefined)
            options = APPLICATION_NO_OPTIONS;

        if (statusCode < 200 && statusCode >= 300) {
            applyMaintenance(options);
            return;
        }

        data = parseJSON(data);
        if (FrameCore.Lang.isObjectLiteral(data) && data.hasOwnProperty(m_version) && FrameCore.Lang.isNumber(data[m_version])) {
            options = data[m_version];
            Frame.Utility.saveSetting('maintenance/version', m_version);
            Frame.Utility.saveSetting('maintenance/options', options);
        }

        applyMaintenance(options);

        if(!controlVersion()){

        }
    });

    if (options !== undefined)
        applyMaintenance(options);


}

function login(viewNode, username, password, remember) {
    m_pfmDataManager.requestToken(viewNode, username, password, remember ? true : false);
}

function logout(viewNode) {
    m_pfmDataManager.requestLogout(viewNode);
}

function getSavedUserInfo() {
    return m_pfmDataManager.getSavedUser();
}

function clearData() {
    m_pfmDataManager.clearData();
}

function init(viewNode, query) {

    bootstrap();

    revokeWifiGranularity();
    revokeSensorGranularity();

    m_pfmDataManager.requestLocationData({
                                             responseLocationDataSuccess: function(data) {
                                                 handleResponseLocationDataSuccess(viewNode, data, query);
                                             },
                                             responseLocationDataFailed: function(errorId, code, message) {
                                                 handleResponseLocationDataFailed(viewNode, errorId, code, message)
                                             }
                                         });
}

function handleResponseLocationDataSuccess(viewNode, data, query) {
    refillModel(data);
    updateVisitCounts(DATA_TYPE_SENSOR, GRANULARITY_NONE, viewNode, query, true);
    viewNode.responseLocationDataSuccess(data);
}

function handleResponseLocationDataFailed(viewNode, errorId, code, message) {
    viewNode.responseLocationDataFailed(errorId, code, message);
}

function refillModel(locationData) {
    m_model.clear();
    m_modelMap = {};

    var modelIndex = 0;
    for (var i in locationData) {
        if (!locationData.hasOwnProperty(i))
            continue;

        m_model.append({
                           locationId:         locationData[i].location_id,
                           name:               locationData[i].name,
                           currentVisitCount:  0,
                           previousVisitCount: 0,
                           sensorState:        STATE_UNLOADED,
                           wifiState:          STATE_UNLOADED
                       });

        m_modelMap[i] = modelIndex++;
    }
}

function setInProgressMode(dataType, locationId) {
    if (locationId !== undefined) {
        if (!m_modelMap.hasOwnProperty(locationId)) {
            console.log(TAG, 'Location is not exists:', locationId);
            return;
        }

        var dataRow = m_model.get(m_modelMap[locationId]);
        if (dataType === DATA_TYPE_WIFI)
            dataRow.wifiState = STATE_LOADING;
        else
            dataRow.sensorState = STATE_LOADING;

        return;
    }

    var i = 0;
    var len = m_model.count;
    var row;
    for (; i < len; i++) {
        row = m_model.get(i);
        if (dataType === DATA_TYPE_WIFI)
            row.wifiState = STATE_LOADING;
        else
            row.sensorState = STATE_LOADING;
    }
}

function getDateQueryString(date, endOfDay) {
    return FrameCore.DateTime.date('Y-m-d', date);
}

function getNormalizedDay(day, second) {
    if (day instanceof Date)
        day = day.getDay();

    return day === 0 ? 7 : day;
}

function getTopDayOfMonth(date) {
    var tmpDate = new Date(Date.UTC(date.getFullYear(), date.getMonth() + 1, 0));
    return tmpDate.getDate();
}

function getFormattedPercentage(value) {
    var prefix = '';

    value = Number(value);
    if (value > 0)
        prefix = '+';

    if (value !== 0 && value !== 100)
        value = value.toFixed(1);
    else
        value = '' + value;

    return (prefix + '' + value + '%');
}

function fromStartToFinish(start, finish, callback, type) {
    var current = new Date(Date.UTC(start.getFullYear(), start.getMonth(), start.getDate()));
    var currentDay;
    var callbackResult;

    type = type || 'weekly';

    if (type === 'weekly') {
        while ((currentDay = getNormalizedDay(current.getDay())) <= getNormalizedDay(finish.getDay())) {
            callbackResult = callback(current, currentDay);
            if (currentDay === 7 || (callbackResult !== undefined && callbackResult > 0))
                break;

            current.setTime(current.getTime() + 86400000);
        }
    } else {
        while (current.getTime() <= finish.getTime()) {
            currentDay = current.getDate();
            callbackResult = callback(current, currentDay);
            current.setTime(current.getTime() + 86400000);
        }
    }
}

function getQueries(dataType, locationId, start, finish, type) {
    var dataCache = dataType === DATA_TYPE_WIFI ? m_pfmDataManager.getWifiVisitData() : m_pfmDataManager.getSensorVisitData();
    var queries = [];
    var lostData = dataCache === null || !dataCache.hasOwnProperty(locationId);

    if (!lostData) {
        var visit = dataCache[locationId];
        fromStartToFinish(start, finish, function(currentTime, currentDay) {
            if (!visit.hasOwnProperty(FrameCore.DateTime.date('Y-m-d', currentTime))) {
                lostData = true;
            }
        }, type);
    }

    if (lostData) {
        queries.push({
                         loc: locationId,
                         start: getDateQueryString(start),
                         end: getDateQueryString(finish, true)
                     });
    }

    return queries;
}

function getAvg(current, previous) {
    var value = 0;
    if (current === 0 && previous === 0)
        return value;

    var diff = (current - previous);
    if (current === 0 || previous === 0)
        return diff;

    value = Number(diff * 100 / Math.max(current, previous));
    if (value !== 0 && value !== 100)
        value = value.toFixed(1);
    else
        value = '' + value;

    return value;
}

function getPercentageOfOverview(current, previous) {
    var value = 0;
    if (previous !== 0)
        value = ((current / previous) - 1) * 100;

    return value;
}

function getSensorModelData(locationId) {
    return m_model.get(m_modelMap[locationId]);
}

function getData(dataType, locationId, start, finish, viewData, type) {
    console.log(TAG, '[' + dataType + ']', 'Using for getting data:', FrameCore.DateTime.date('Y-m-d', start), 'to', FrameCore.DateTime.date('Y-m-d', finish));

    var dataCache = dataType === DATA_TYPE_WIFI ? m_pfmDataManager.getWifiVisitData() : m_pfmDataManager.getSensorVisitData();
    if (!m_pfmDataManager.hasLocationData() || dataCache === null) {
        console.log(TAG, '[' + dataType + ']', 'Location or visit counts are empty. Cannot get data.')
        return null;
    }

    var visit = dataCache[locationId];
    if (!visit) {
        console.log(TAG, '[' + dataType + ']', 'Location not found:', locationId);
        return null;
    }

    var data = {};
    var date;
    var currentVisit;

    fromStartToFinish(start, finish, function(currentTime, currentDay) {
        date = FrameCore.DateTime.date('Y-m-d', currentTime);
        if (!visit.hasOwnProperty(date)) {
            console.warn(TAG, '[' + dataType + ']', 'Date not found in cache:', date);
            return; // continue
        }

        currentVisit = visit[date];
        if (viewData) {
            if (m_atomicHandler.hasOwnProperty(dataType) && m_atomicHandler[dataType]) {
                for (var field in currentVisit) {
                    if (!currentVisit.hasOwnProperty(field) || !m_atomicHandler[dataType].hasOwnProperty(field))
                        continue;

                    currentVisit[field] = m_atomicHandler[dataType][field](currentVisit);
                }
            }
        }

        data[date] = currentVisit;
    }, type);

    return data;
}

function updateVisitCounts(dataType, granularityType, listener, query, updateModel, locationId) {

    if (!m_pfmDataManager.hasLocationData()) {
        console.log(TAG, '[' + dataType + ']', 'Location is empty. Cannot update visit.')
        return;
    }

    if (listener)
        listener.lockRequest(dataType);
    else
        console.log(TAG, '[' + dataType + ']', 'Unhandled lock request.');

    console.log(TAG, '[' + dataType + ']', 'Current query is: ' + JSON.stringify(query));

    setInProgressMode(dataType, locationId);
    FrameCore.Util.setTimeout(function() {
        var locationData = m_pfmDataManager.getLocationData();

        var location;
        var visit;
        var found = false;
        for (var currentId in locationData) {
            if (!locationData.hasOwnProperty(currentId))
                continue;

            location = locationData[currentId];
            if (locationId !== undefined) {
                if (location.location_id !== locationId)
                    continue;
                else
                found = true;
            }

            queryVisitCount(dataType, granularityType, listener, location.location_id, query, updateModel);
            if (found)
                break;
        }

        if (locationId !== undefined && !found)
            console.warn(TAG, '[' + dataType + ']', 'Location id is not exists:', locationId);

        unlockIfAvailable(dataType, listener);
    }, m_testMode ? -1 : 100);
}

function queryVisitCount(dataType, granularityType, listener, locationId, query, updateModel) {
    var previousQueries = query.previous === null ? null : getQueries(dataType, locationId, query.previous.start, query.previous.finish, query.type);
    var currentQueries = getQueries(dataType, locationId, query.current.start, query.current.finish, query.type);

    if (previousQueries !== null) {
        if (previousQueries.length > 0) {
            console.log(TAG, '[' + dataType + ']', 'Executing new queries for previous.');
            executeQueries(dataType, granularityType, listener, previousQueries, query.previous.start, query.previous.finish, false, updateModel, query.type);
        } else {
            console.log(TAG, '[' + dataType + ']', 'Calculating data for previous from cache.');
            var previousData = getData(dataType, locationId, query.previous.start, query.previous.finish, query.type);
            if (updateModel && !setCalculatedValue(dataType, previousData, locationId, false))
                console.log(TAG, '[' + dataType + ']', 'Calculation failed for previous date from cache.');
            else if (listener)
                listener.calculationCompleted(dataType, locationId, false);
            else
                console.log(TAG, '[' + dataType + ']', 'Calculation completed with previous data for location id:', locationId);

            updateModelState(dataType, locationId);
        }

        executeGranularity(listener, granularityType, dataType, locationId, query.previous.start, query.previous.finish, false);

    } else {
        if (updateModel && !setCalculatedValue(dataType, {}, locationId, false))
            console.log(TAG, '[' + dataType + ']', 'Calculation failed for previous date from cache.');
        else if (listener)
            listener.calculationCompleted(dataType, locationId, false);
        else
            console.log(TAG, '[' + dataType + ']', 'Calculation completed with previous data for location id:', locationId);

        updateModelState(dataType, locationId);
        listener.responseWifiGranularityDataSuccess(granularityType, locationId, null, false);
    }

    if (currentQueries.length > 0) {
        console.log(TAG, '[' + dataType + ']', 'Executing new queries for current.', dataType);
        executeQueries(dataType, granularityType, listener, currentQueries, query.current.start, query.current.finish, true, updateModel, query.type);
    } else {
        console.log(TAG, '[' + dataType + ']', 'Calculating data for current from cache.');
        var currentData = getData(dataType, locationId, query.current.start, query.current.finish, query.type);
        if (updateModel && !setCalculatedValue(dataType, currentData, locationId, true))
            console.log(TAG, '[' + dataType + ']', 'Calculation failed for current date from cache.');
        else if (listener)
            listener.calculationCompleted(dataType, locationId, true);
        else
            console.log(TAG, '[' + dataType + ']', 'Calculation completed with current data for location id:', locationId);

        updateModelState(dataType, locationId);
    }

    executeGranularity(listener, granularityType, dataType, locationId, query.current.start, query.current.finish, true);
}

function executeGranularity(listener, granularityType, dataType, locationId, startDate, finishDate, currentVisitCount) {
    if (granularityType === GRANULARITY_NONE)
        return;

    console.log(TAG, '[' + dataType + '] Executing Granularity.');
    if (dataType === DATA_TYPE_WIFI) {
        m_pfmDataManager.requestWifiGranularityData({
                                                        responseWifiGranularityDataSuccess: function(granularityType, locationId, data) {
                                                            listener.responseWifiGranularityDataSuccess(granularityType, locationId, data, currentVisitCount);
                                                        },
                                                        responseWifiGranularityDataFailed: function(granularityType, locationId, errorId, code, message) {
                                                            listener.responseWifiGranularityDataFailed(granularityType, locationId, errorId, code, message, currentVisitCount);
                                                        }
                                                    }, granularityType, locationId, startDate, finishDate);
    }
}

function executeQueries(dataType, granularityType, listener, queries, start, finish, currentVisitCount, updateModel, type) {
    var i = 0;
    var len = queries.length;
    var query;
    for (; i < len; i++) {
        query = queries[i];
        executeAPIQuery(dataType, granularityType, listener, query, start, finish, currentVisitCount, updateModel, type);
    }
}

function executeAPIQuery(dataType, granularityType, listener, query, start, finish, currentVisitCount, updateModel, type) {
    if (dataType === DATA_TYPE_SENSOR) {
        m_pfmDataManager.requestSensorVisitData({
                                                    responseSensorVisitDataSuccess: function(locationId, data) {
                                                        console.log(TAG, '[' + dataType + ']', 'Calculating value through SENSOR api for', query.loc);
                                                        if (updateModel && !setCalculatedValue(dataType, data, query.loc, currentVisitCount))
                                                            console.log(TAG, '[' + dataType + ']', 'Calculation failed for location during api call.');
                                                        else if (listener)
                                                            listener.calculationCompleted(dataType, query.loc, currentVisitCount ? true : false);
                                                        else
                                                            console.log(TAG, '[' + dataType + ']', 'Calculation completed with',  currentVisitCount ? 'current' : 'previous', 'data for location id:', query.loc);

                                                        updateModelState(dataType, locationId);
                                                        unlockIfAvailable(dataType, listener);
                                                        listener.responseSensorVisitDataSuccess(locationId, data);
                                                    },
                                                    responseSensorVisitDataFailed: function(locationId, errorId, code, message) {
                                                        listener.responseSensorVisitDataFailed(locationId, errorId, code, message);
                                                    }
                                                }, query.loc, start, finish, type);
    } else {
        m_pfmDataManager.requestWifiVisitData({
                                                  responseWifiVisitDataSuccess: function(locationId, data) {
                                                      console.log(TAG, '[' + dataType + ']', 'Calculating value through WIFI api for', query.loc);
                                                      if (listener)
                                                          listener.calculationCompleted(dataType, query.loc, currentVisitCount ? true : false);
                                                      else
                                                          console.log(TAG, '[' + dataType + ']', 'Calculation completed with',  currentVisitCount ? 'current' : 'previous', 'data for location id:', query.loc);

                                                      updateModelState(dataType, locationId);
                                                      unlockIfAvailable(dataType, listener);

                                                      listener.responseWifiVisitDataSuccess(locationId, data);
                                                  },
                                                  responseWifiVisitDataFailed: function(locationId, errorId, code, message) {
                                                      listener.responseWifiVisitDataFailed(locationId, errorId, code, message);
                                                  }
                                              }, query.loc, start, finish, type);
    }
}

function startSyncProgress(listenerItemCallback) {

    if (m_syncLock) {
        console.log(TAG, 'Cannot send sync request Because a sync process still in progress.');
        return;
    }

    var lastRequest = Frame.Utility.getSetting('sync/lastRequest');
    if (lastRequest === undefined) {
        lastRequest = (new Date()).getTime();
        Frame.Utility.saveSetting('sync/lastRequest', lastRequest);
    }

    var timeout = 10800000; // 3 hours
    var elapsedTime = (new Date()).getTime() - lastRequest;
    if (elapsedTime < timeout) {
        console.log(TAG, 'Sync skipping. Time left to next sync:', FrameCore.DateTime.timeFormatter('?d ?h ?h ?m !s', timeout - elapsedTime));
        return;
    }

    console.log(TAG, 'Requesting sync request.');
    m_syncLock = true;
    m_pfmDataManager.requestSyncList({
                                         responseSyncListDataSuccess: function(data) {
                                             var listenerItem = FrameCore.Lang.isFunction(listenerItemCallback)
                                                     ? listenerItemCallback()
                                                     : (listenerItemCallback === undefined ? null : listenerItemCallback);
                                             var i = 0;
                                             var len = data.length;
                                             if (len > 0) {
                                                 if (listenerItem)
                                                     listenerItem.syncStarted();

                                                 for (; i < len; i++) {
                                                     if (data[i].isWifi) {
                                                         m_pfmDataManager.m_apiManager.requestWifiVisitData({
                                                                                                                startDate: new Date(data[i].date),
                                                                                                                finishDate: new Date(data[i].date),
                                                                                                                locationId: data[i].locationId,
                                                                                                                index: i,
                                                                                                                onSuccess: function(response, params) {
                                                                                                                    recacheWifi(params.locationId, response);
                                                                                                                    if ((len - 1) === params.index) {
                                                                                                                        m_syncLock = false;
                                                                                                                        if (listenerItem)
                                                                                                                            listenerItem.syncCompleted();

                                                                                                                        Frame.Utility.saveSetting('sync/lastRequest', lastRequest);
                                                                                                                        console.log(TAG, 'Sync completed.');
                                                                                                                    }
                                                                                                                },
                                                                                                                onFailed: function(errorId, code, message) {
                                                                                                                    console.log(TAG, 'Sync failed with:', errorId, code, message);

                                                                                                                    m_syncLock = false;
                                                                                                                    if (listenerItem)
                                                                                                                        listenerItem.syncError(errorId, code, message);
                                                                                                                }
                                                                                                            });
                                                     } else if (data[i].isSensor) {
                                                         m_pfmDataManager.m_apiManager.requestSensorVisitData({
                                                                                                                  startDate: new Date(data[i].date),
                                                                                                                  finishDate: new Date(data[i].date),
                                                                                                                  locationId: data[i].locationId,
                                                                                                                  index: i,
                                                                                                                  onSuccess: function(response, params) {
                                                                                                                      recacheSensor(params.locationId, response);
                                                                                                                      if ((len - 1) === params.index) {

                                                                                                                          m_syncLock = false;
                                                                                                                          if (listenerItem)
                                                                                                                              listenerItem.syncCompleted();

                                                                                                                          Frame.Utility.saveSetting('sync/lastRequest', lastRequest);
                                                                                                                          console.log(TAG, 'Sync completed.');
                                                                                                                      }
                                                                                                                  },
                                                                                                                  onFailed: function(errorId, code, message) {
                                                                                                                      console.log(TAG, 'Sync failed with:', errorId, code, message);
                                                                                                                      m_syncLock = false;
                                                                                                                      if (listenerItem)
                                                                                                                          listenerItem.syncError(errorId, code, message);
                                                                                                                  }
                                                                                                              });
                                                     }
                                                 }

                                                 revokeWifiGranularity();
                                             } else {
                                                 console.log(TAG, 'No Sync data.');
                                                 Frame.Utility.saveSetting('sync/lastRequest', lastRequest);
                                                 m_syncLock = false;
                                             }
                                         },
                                         responseSyncListDataFailed: function(errorId, code, message) {
                                             console.log(TAG, 'Retriving sync failed.');
                                             var listenerItem = FrameCore.Lang.isFunction(listenerItemCallback)
                                                     ? listenerItemCallback()
                                                     : (listenerItemCallback === undefined ? null : listenerItemCallback);

                                             m_syncLock = false;
                                             if (listenerItem)
                                                 listenerItem.syncError(errorId, code, message);
                                         }
                                     });
}

function recacheWifi(locationId, data) {
    m_pfmDataManager.recacheWifi(locationId, data);
}

function recacheSensor(locationId, data) {
    m_pfmDataManager.recacheSensor(locationId, data);
}

function revokeWifiGranularity() {
    m_pfmDataManager.revokeWifiGranularity();
}

function revokeSensorGranularity() {
    m_pfmDataManager.revokeSensorGranularity();
}

function calculateValue(dataType, data, field) {
    console.log(TAG, '[' + dataType + ']', 'Using calculating value.');
    return getTotalCount(dataType, data, field);
}

function setCalculatedValue(dataType, data, locationId, currentVisitCount) {
    if (dataType !== DATA_TYPE_SENSOR)
        return true;

    var modelIndex = m_modelMap[locationId];
    if (modelIndex === undefined) {
        console.warn(TAG, '[' + dataType + ']', 'Invalid model index for location id:', locationId);
        return false;
    }

    var value = calculateValue(dataType, data, 'total_count');
    var node = m_model.get(modelIndex);
    if (currentVisitCount)
        node.currentVisitCount = value;
    else
        node.previousVisitCount = value;

    return true;
}

function updateModelState(dataType, locationId) {
    var modelIndex = m_modelMap[locationId];
    if (modelIndex === undefined) {
        console.warn(TAG, '[' + dataType + ']', 'Invalid model index for location id:', locationId);
        return false;
    }

    var node = m_model.get(modelIndex);
    if (dataType === DATA_TYPE_WIFI)
        node.wifiState = node.wifiState <= STATE_LOADING ? STATE_LOADED_PREVIOUS : STATE_LOADED_ALL;
    else
        node.sensorState = node.sensorState <= STATE_LOADING ? STATE_LOADED_PREVIOUS : STATE_LOADED_ALL;

    return true;
}

function unlockIfAvailable(dataType, viewNode) {
    var i = 0;
    var len = m_model.count;
    var row;
    var state;
    for (; i < len; i++) {
        row = m_model.get(i);
        state = dataType === DATA_TYPE_WIFI ? row.wifiState : row.sensorState;
        if (state > STATE_UNLOADED && state < STATE_LOADED_ALL)
            return false;
    }

    if (viewNode)
        viewNode.unlockRequest(dataType);
    else
        console.log(TAG, '[' + dataType + ']', 'Unhandled unlock request.');

    return true;
}

function setErrorMode(dataType, locationId) {
    var node = m_model.get(m_modelMap[locationId]);
    if (dataType === DATA_TYPE_WIFI)
        node.wifiState = STATE_ERROR;
    else
        node.sensorState = STATE_ERROR;
}

function getSavedUser() {
    bootstrap();
    return m_pfmDataManager.getSavedUser();
}

function hasLocationData() {
    return !m_pfmDataManager ? false : m_pfmDataManager.hasLocationData();
}

function objectCount(obj) {
    var count = 0;
    if (FrameCore.Lang.isObjectLiteral(obj)) {
        for (var i in obj) {
            if (obj.hasOwnProperty(i))
                count++;
        }
    }

    return count;
}

function objectToArray(obj) {
    var pair = [];
    if (FrameCore.Lang.isObjectLiteral(obj)) {
        for (var i in obj) {
            if (obj.hasOwnProperty(i))
                pair.push({ key: i, value: obj[i] });
        }
    }

    return pair;
}
