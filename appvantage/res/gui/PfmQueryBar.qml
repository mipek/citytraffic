import QtQuick 2.5
import QtQuick.Layouts 1.1
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0 as FrameCore
import "PfmController.js" as PfmController
import "PfmQueryBarPrivate.js" as PfmQueryBarPrivate

PfmBlock {
    property var serverDate: new Date()

    signal queryChanged()

    signal nextTriggered()
    signal previousTriggered()
    signal resetTriggered()

    property int offset: 0
    property string type
    property bool queryToYearBackForPrevious: true

    property bool locked: false

    property alias dd: d

    function query() {
        return {
            current: { start: d.currentStartDate, finish: d.currentFinishDate },
            previous: d.previousStartDate === null ? null : { start: d.previousStartDate, finish: d.previousFinishDate },
            type: root.type
        };
    }

    function setQuery(startDateRef, finishDateRef, type, queryBack, offset) {
        if (type !== undefined || root.type === '')
            root.type = PfmQueryBarPrivate.getType(type);

        if (startDateRef !== undefined)
            d.startDateRef = startDateRef;

        if (finishDateRef !== undefined)
            d.finishDateRef = finishDateRef;

        if (queryBack !== undefined)
            queryToYearBackForPrevious = queryBack ? true : false;

        if (offset !== undefined && offset !== root.offset)
            root.offset = offset; // this will trigger reinit automatically
        else
            d.reinit();
    }

    id: root
    height: dp(76)

    onServerDateChanged: d.handleServerDateChanged()
    onOffsetChanged: d.reinit()

    QtObject {
        id: d

        property var startDateRef: null
        property var finishDateRef: null

        property var currentStartDate
        property var currentFinishDate

        property var previousStartDate
        property var previousFinishDate

        property string currentWeekValue
        property string currentWeekSubText

        property string previousWeekValue
        property string previousWeekSubText

        property bool queryNextAvailable: canQueryNext(root.locked, root.offset, root.type);
        property bool queryPreviousAvailable: canQueryPrevious(root.locked, root.offset, root.type);

        function reinit() {
            var date = PfmQueryBarPrivate.getDateOffset(startDateRef, finishDateRef, root.offset, root.type);
            var bounds = PfmQueryBarPrivate.getBounds(date, root.offset, root.type, startDateRef, finishDateRef);

            currentStartDate = bounds.start;
            currentFinishDate = bounds.finish;

            currentWeekValue = getQueryText(bounds);
            currentWeekSubText = getOffsetText(root.offset);

            date = PfmQueryBarPrivate.getPreviousDateOffset(startDateRef, finishDateRef, root.offset, root.queryToYearBackForPrevious, root.type);
            if (date !== null) { // like: 53th week for last year
                bounds = PfmQueryBarPrivate.getBounds(date, root.offset, root.type, startDateRef, finishDateRef);

                previousStartDate = bounds.start;
                previousFinishDate = bounds.finish;

                previousWeekValue = getQueryText(bounds);
                previousWeekSubText = getPreviousOffsetText(root.offset);

            } else {
                previousStartDate = null;
                previousFinishDate = null;

                previousWeekValue = getQueryText(null);
                previousWeekSubText = '';
            }

            queryChanged();
        }

        function getOffsetText(offset) {
            return PfmQueryBarPrivate.getOffsetText(offset, root.type);
        }

        function getPreviousOffsetText(offset) {
            return PfmQueryBarPrivate.getPreviousOffsetText(offset, root.type, root.queryToYearBackForPrevious);
        }

        function getQueryText(date) {
            var queryText = date === null ? null : { start: date.start, finish: date.finish };
            if (date !== null && root.offset === 0)
                queryText.finish = PfmQueryBarPrivate.getEndDateOf(date.start, queryBar.type, queryBar.dd.startDateRef, queryBar.dd.finishDateRef);

            return PfmQueryBarPrivate.getQueryText(queryText, root.offset, root.type);
        }

        function canQueryNext(locked, offset, type) {
            if (locked)
                return false;

            return PfmQueryBarPrivate.canQueryNext(offset, type);
        }

        function canQueryNextInternal() {
            return canQueryNext(root.locked, root.offset, root.type);
        }

        function canQueryPrevious(locked, offset, type) {
            if (locked)
                return false;

            return PfmQueryBarPrivate.canQueryPrevious(offset, type);
        }

        function canQueryPreviousInternal() {
            return canQueryPrevious(root.locked, root.offset, root.type);
        }

        function next() {
            if (!canQueryNextInternal())
                return;

            var newOffset = PfmQueryBarPrivate.getNextOffset(root.offset, root.type);
            if (root.offset !== newOffset) {
                root.offset = newOffset;
                nextTriggered();
            }
        }

        function previous() {
            if (!canQueryPreviousInternal())
                return;

            var newOffset = PfmQueryBarPrivate.getPreviousOffset(root.offset, root.type);
            if (root.offset !== newOffset) {
                root.offset = newOffset;
                previousTriggered();
            }
        }

        function reset() {
            root.offset = 0;
            resetTriggered();
        }

        function handleServerDateChanged() {
            if (root.type === '' || root.type !== PfmQueryBarPrivate.TYPE_CUSTOM) {
                d.startDateRef = serverDate;
                setQuery(serverDate);
            }
        }
    }

    body: Item {

        Item {
            anchors.fill: parent

            RowLayout {
                anchors.left: parent.left
                anchors.right: parent.right
                height: dp(66)
                spacing: dp(3)
                anchors.verticalCenter: parent.verticalCenter

                Item {
                    implicitWidth: dp(18)
                    height: parent.height

                    Image {
                        source: imagePath('left-arrow.png')
                        width: dp(13)
                        height: dp(21)
                        visible: d.queryPreviousAvailable
                        anchors.centerIn: parent
                    }

                    Image {
                        source: imagePath('left-arrow-disabled.png')
                        width: dp(13)
                        height: dp(21)
                        visible: !d.queryPreviousAvailable
                        anchors.centerIn: parent
                    }
                }

                Item {
                    height: parent.height
                    Layout.fillWidth: true

                    RowLayout {
                        anchors.fill: parent

                        Item {
                            height: parent.height
                            Layout.fillWidth: true

                            Column {
                                width: parent.width
                                anchors.verticalCenter: parent.verticalCenter

                                PfmText {
                                    color: '#6f777e'
                                    font.pixelSize: theme.queryBarDateFontSize
                                    text: d.previousWeekValue
                                    anchors.horizontalCenter: parent.horizontalCenter
                                }

                                PfmText {
                                    color: '#6f777e'
                                    font.pixelSize: theme.queryBarSubTextFontSize
                                    font.weight: Font.Medium
                                    text: d.previousWeekSubText
                                    anchors.horizontalCenter: parent.horizontalCenter
                                }
                            }
                        }

                        Item {
                            height: parent.height
                            implicitWidth: dp(3)

                            Rectangle {
                                width: dp(1)
                                height: dp(50)
                                color: '#808080'

                                anchors.centerIn: parent
                            }
                        }

                        Item {
                            height: parent.height
                            Layout.fillWidth: true

                            Column {
                                width: parent.width
                                anchors.verticalCenter: parent.verticalCenter

                                PfmText {
                                    color: '#6f777e'
                                    font.pixelSize: theme.queryBarDateFontSize
                                    text: d.currentWeekValue
                                    anchors.horizontalCenter: parent.horizontalCenter
                                }

                                PfmText {
                                    color: '#6f777e'
                                    font.pixelSize: theme.queryBarSubTextFontSize
                                    font.weight: Font.Medium
                                    text: d.currentWeekSubText
                                    anchors.horizontalCenter: parent.horizontalCenter
                                }
                            }
                        }
                    }
                }

                Item {
                    implicitWidth: dp(18)
                    height: parent.height

                    Image {
                        source: imagePath('right-arrow.png')
                        width: dp(13)
                        height: dp(21)
                        visible: d.queryNextAvailable
                        anchors.centerIn: parent
                    }

                    Image {
                        source: imagePath('right-arrow-disabled.png')
                        width: dp(13)
                        height: dp(21)
                        visible: !d.queryNextAvailable
                        anchors.centerIn: parent
                    }
                }
            }

            MouseArea {
                id: leftMouse

                anchors.left: parent.left
                width: parent.width / 2
                height: parent.height

                preventStealing: true
                enabled: !root.locked && d.queryPreviousAvailable
                onClicked: d.previous()
                onPressedChanged: pressed ? touchIneractionLeft.press() : touchIneractionLeft.release()

                TouchInteractionButton {
                    id: touchIneractionLeft
                    circlar: false
                    anchors.fill: parent
                }
            }

            MouseArea {
                id: rightMouse

                anchors.right: parent.right
                width: parent.width / 2
                height: parent.height

                preventStealing: true
                enabled: !root.locked && d.queryNextAvailable
                onClicked: d.next()
                onPressedChanged: pressed ? touchIneractionRight.press() : touchIneractionRight.release()

                TouchInteractionButton {
                    id: touchIneractionRight
                    circlar: false
                    anchors.fill: parent
                }
            }
        }
    }
}

