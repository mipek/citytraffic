.import Frame.Mobile.Core 1.0 as FrameCore

function Adapter(pfmController, manager) {
    this.m_pfmController = pfmController;
    this.m_manager = manager;

    this.m_location = null;
    this.m_sensor = null;
    this.m_wifi = null;

    this.m_sensorGranularity = null;
    this.m_wifiGranularity = null;

    this.m_loginUser = null;
}

Adapter.prototype = {
    clearData: function() {
        this.m_location = null;
        this.m_sensor = null;
        this.m_wifi = null;
        this.m_sensorGranularity = null;
        this.m_wifiGranularity = null;
    },
    requestLocationData: function() {
        return this.m_location;
    },
    saveLocationData: function(data) {
        this.m_location = data;
    },
    getLoggedInUser: function() {
        return this.m_loginUser;
    },
    saveLoggedInUser: function(data) {
        this.m_loginUser = data;
    },
    getLocationData: function() {
        return this.m_location;
    },

    _requestData: function(source, locationId, data, dateList) {
        if (data === null || data === undefined)
            data = {};

        var result = { status: this.m_pfmController.NOCHANGE, data: data };
        if (source === null || !source.hasOwnProperty(locationId))
            return result;

        var i = 0;
        var len = dateList.length;
        var date;
        var foundOne = false;
        var lostOne = false;
        for (; i < len; i++) {
            date = dateList[i];
            if (source[locationId].hasOwnProperty(date)) {
                data[date] = source[locationId][date];
                foundOne = true;
            } else {
                lostOne = true;
            }
        }

        if (foundOne)
            result.status = lostOne ? this.m_pfmController.PARTIAL : this.m_pfmController.COMPLETED;

        return result;
    },

    _saveData: function(target, locationId, data) {
        for (var date in data) {
            if (!data.hasOwnProperty(date))
                continue;

            if (!target.hasOwnProperty(locationId))
                target[locationId] = {};

            target[locationId][date] = data[date];
        }
    },

    saveSensorVisitData: function(locationId, data) {
        if (this.m_sensor === null)
            this.m_sensor = {};

        this._saveData(this.m_sensor, locationId, data);
    },
    getSensorVisitData: function() {
        return this.m_sensor;
    },

    saveWifiVisitData: function(locationId, data) {
        if (this.m_wifi === null)
            this.m_wifi = {};

        this._saveData(this.m_wifi, locationId, data);
    },
    getWifiVisitData: function() {
        return this.m_wifi;
    },

    saveSensorGranularityData: function(granularityType, locationId, data) {
        if (this.m_sensorGranularity === null)
            this.m_sensorGranularity = {};

        if (!this.m_sensorGranularity.hasOwnProperty(granularityType))
            this.m_sensorGranularity[granularityType] = {};

        if (!this.m_sensorGranularity[granularityType].hasOwnProperty(locationId))
            this.m_sensorGranularity[granularityType][locationId] = {};

        var key = data.start_date + '|' + data.finish_date;
        this.m_sensorGranularity[granularityType][locationId][key] = data;
    },

    getSensorGranularityData: function() {
        return this.m_sensorGranularity;
    },

    saveWifiGranularityData: function(granularityType, locationId, data) {
        if (this.m_wifiGranularity === null)
            this.m_wifiGranularity = {};

        if (!this.m_wifiGranularity.hasOwnProperty(granularityType))
            this.m_wifiGranularity[granularityType] = {};

        if (!this.m_wifiGranularity[granularityType].hasOwnProperty(locationId))
            this.m_wifiGranularity[granularityType][locationId] = {};

        var key = data.start_date + '|' + data.finish_date;
        this.m_wifiGranularity[granularityType][locationId][key] = data;
    },
    getWifiGranularityData: function() {
        return this.m_wifiGranularity;
    },

    requestSensorVisitData: function(locationId, data, dateList) {
        return this._requestData(this.m_sensor, locationId, data, dateList);
    },

    requestWifiVisitData: function(locationId, data, dateList) {
        return this._requestData(this.m_wifi, locationId, data, dateList);
    },

    requestSensorGranularityData: function(granularityType, locationId, start, finish) {
        var result = { status: this.m_pfmController.NOCHANGE, data: null };
        if (this.m_sensorGranularity === null
                || !this.m_sensorGranularity.hasOwnProperty(granularityType)
                || !this.m_sensorGranularity[granularityType].hasOwnProperty(locationId)) {
            return result;
        }

        var key = start + '|' + finish;
        if (this.m_sensorGranularity[granularityType][locationId].hasOwnProperty(key)) {
            result.data = this.m_sensorGranularity[granularityType][locationId][key];
            result.status = this.m_pfmController.COMPLETED;
        }

        return result;
    },

    requestWifiGranularityData: function(granularityType, locationId, start, finish) {
        var result = { status: this.m_pfmController.NOCHANGE, data: null };
        if (this.m_wifiGranularity === null
                || !this.m_wifiGranularity.hasOwnProperty(granularityType)
                || !this.m_wifiGranularity[granularityType].hasOwnProperty(locationId)) {
            return result;
        }

        var key = start + '|' + finish;
        if (this.m_wifiGranularity[granularityType][locationId].hasOwnProperty(key)) {
            result.data = this.m_wifiGranularity[granularityType][locationId][key];
            result.status = this.m_pfmController.COMPLETED;
        }

        return result;
    }
};

