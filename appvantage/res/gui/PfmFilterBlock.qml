import QtQuick 2.5

Column {
    property alias label: label
    property alias dateLabel: dateLabel

    width: parent.width

    PfmText {
        id: label

        font.weight: Font.Medium
        font.pixelSize: sp(16)
        text: 'N/A'
        color: '#808080'
    }

    PfmText {
        id: dateLabel

        font.pixelSize: sp(13)
        text: 'N/A'
        color: '#B0B0B0'
    }
}
