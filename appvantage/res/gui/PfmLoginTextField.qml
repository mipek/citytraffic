import QtQuick 2.5
import QtQuick.Controls.Styles 1.3
import Frame.Mobile 1.0

TouchTextField {
    id: root

    clearButton.source: imagePath('clear.png')
    clearButton.width: dp(20)
    clearButton.height: dp(20)

    mouse.width: dp(28)

    implicitWidth: dp(165)
    implicitHeight: dp(51)

    style: TextFieldStyle {
        background: Rectangle {
            color: "#fff"
            implicitHeight: root.height
        }
        padding.left: dp(10)
        padding.right: dp(32)
        font.pixelSize: sp(14)
        font.family: "Avenir"
        font.letterSpacing: sp(-0.24)
        textColor: "#cdcdcd"
    }
}

