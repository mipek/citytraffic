import QtQuick 2.5
import QtQuick.Layouts 1.1

PfmBlockDynamic {
    property ListModel model
    property int selectedIndex: -1

    id: root

    QtObject {
        id: d

        function handleFilterTap(index) {
            selectedIndex = index;
        }
    }

    body: Rectangle {
        implicitWidth: parent.width
        implicitHeight: column.height

        Column {
            id: column
            width: parent.width

            Rectangle {
               color: '#F8F8F8'
               width: parent.width
               height: dp(36)

               PfmText {
                   color: '#B0B0B0'
                   font.pixelSize: sp(17)
                   text: 'Compare to'
                   anchors.verticalCenter: parent.verticalCenter
                   anchors.left: parent.left
                   anchors.leftMargin: dp(15)
               }
            }

            Repeater {
                model: root.model
                delegate: Rectangle {
                    width: parent.width
                    height: dp(70)

                    PfmRangeCheckRow {
                        checked: model.index === selectedIndex
                        filterView.label.text: model.title
                        filterView.dateLabel.text: model.date
                        mouse.onClicked: d.handleFilterTap(model.index)
                    }

                    Rectangle {
                        height: dp(1)
                        width: parent.width - dp(20)
                        color: '#E0E0E0'
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: parent.bottom
                        visible: index < root.model.count - 1
                    }
                }
            }
        }
    }
}
