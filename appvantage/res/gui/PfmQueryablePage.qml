import QtQuick 2.5

PfmApiCallablePage {
    id: callablePage
    signal lockRequest(string dataType);
    signal unlockRequest(string dataType);
    signal calculationCompleted(string dataType, int locationId, bool forCurrent);

    signal responseLocationDataFailed(string errorId, int code, string message);
    signal responseLocationDataSuccess(var data);

    signal responseSensorVisitDataSuccess(string locationId, var data);
    signal responseSensorVisitDataFailed(string locationId, string errorId, int code, string message);



    signal responseWifiVisitDataSuccess(string locationId, var data);
    signal responseWifiVisitDataFailed(string locationId, string errorId, int code, string message);

    signal responseWifiGranularityDataSuccess(int granularityType, string locationId, var data, bool forCurrent)
    signal responseWifiGranularityDataFailed(int granularityType, string locationId, string errorId, int code, string message, bool forCurrent)
}

