import QtQuick 2.5
import QtQuick.Layouts 1.1

Item {
    property alias filterView: filterView
    property alias mouse: mouse
    property bool checked: false

    height: dp(70)
    anchors.left: parent.left
    anchors.right: parent.right

    RowLayout {
        anchors.fill: parent
        anchors.leftMargin: dp(15)
        anchors.rightMargin: dp(15)

        PfmFilterBlock {
            id: filterView
            Layout.fillWidth: true
        }

        Item {
            implicitWidth: dp(14)
            implicitHeight: parent.height

            Image {
                source: imagePath('check.png')
                width: dp(18)
                height: dp(14)

                anchors.verticalCenter: parent.verticalCenter

                visible: checked
            }
        }
    }

    TouchInteractionButton {
        id: listInteraction
        circlar: false
        width: parent.width
        height: parent.height
        visible: mouse.enabled
    }

    MouseArea {
        id: mouse

        anchors.fill: parent
        preventStealing: true
        onPressedChanged: pressed ? listInteraction.press() : listInteraction.release()
    }
}
