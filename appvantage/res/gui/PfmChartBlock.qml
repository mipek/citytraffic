import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtCharts 2.1
import QtGraphicalEffects 1.0
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0
import "PfmController.js" as PfmController
import "PfmQueryBarPrivate.js" as PfmQueryBarPrivate
import com.triodor.pfm 1.0

PfmBlock {
    signal recalculateGranularity(int granularityType)

    property bool inProgress: false
    property string title: ''
    property string previous: ''
    property string current: ''
    property PfmQueryBar queryBar;
    property real avg: 0
    property bool active: false
    property int locationId
    property string field

    function drawChart(previous, current, timeFormat, minMaxX, minMaxY, granularityType, axisType) {
        d.drawChart(previous, current, timeFormat ? true : false, minMaxX, minMaxY, granularityType, axisType);
    }

    id: root

    width: parent.width
    height: dp(340)

    QtObject {
        property bool timeFormat: false
        property ListModel chartAxisSelectionModel: ListModel {
            ListElement { title: 'Daily'; action: 'daily'; }
            ListElement { title: 'Weekly'; action: 'weekly'; }
            ListElement { title: 'Monthly'; action: 'monthly'; }
        }
        property int axisType: PfmController.TIME_AXIS_DAILY
        property int granularityType
        property var previousDataCache
        property var currentDataCache
        property var minMaxXCache
        property var minMaxYCache
        property bool showCustomXAxis: true

        id: d

        function drawChart(previous, current, timeFormat, minMaxX, minMaxY, granularityType, axisType) {
            var item = bodyLoader.item;
            if (!item) {
                console.log('[PfmChartBlock]', 'Inner chart item is not loaded yet.');
                return;
            }

            d.timeFormat = timeFormat ? true : false;
            d.granularityType = granularityType;

            if (d.axisType === PfmController.TIME_AXIS_WEEKLY && axisType === PfmController.TIME_AXIS_WEEKLY) {
                // there is a bug for Qt Charts...
                showCustomXAxis = false;
                showCustomXAxisChanged();
                showCustomXAxis = true;
                showCustomXAxisChanged();
            }

            d.axisType = axisType || PfmController.TIME_AXIS_DAILY;

            d.currentDataCache = current;
            d.previousDataCache = previous;

            d.minMaxXCache = minMaxX;
            d.minMaxYCache = minMaxY;

            var chart = item.chart;
            var dataBridge = chart.dataBridge;
            var data = {
                current: d.currentDataCache,
                previous: d.previousDataCache,
                boundry: {
                    x: d.minMaxXCache,
                    y: d.minMaxYCache
                }
            };

            switch (d.axisType) {
            case PfmController.TIME_AXIS_WEEKLY:
                data = convertDailyToWeekly(data.current, data.previous);
                break;
            case PfmController.TIME_AXIS_MONTHLY:
                data = convertDailyToMonthly(data.current, data.previous);
                break;
            }

            setAxisFormats(chart, axisType, queryBar.type, Math.max(data.previous.length, data.current.length));

            dataBridge.setPoints(chart.previousSeries.upperSeries, data.previous);
            dataBridge.setPoints(chart.currentSeries.upperSeries, data.current);
            dataBridge.setPoints(chart.scatterSeries, data.current);

            updateAxis(chart, data.boundry.x, data.boundry.y);

            if (d.granularityType !== granularityType)
                recalculateGranularity(recalculateGranularityType);
        }

        function setAxisFormats(chart, axisType, queryType, maxDataCount) {
            switch (queryType) {
            case PfmQueryBarPrivate.TYPE_MONTHLY:
                chart.dateAxis.format = 'dd/MM';
                if (axisType === PfmController.TIME_AXIS_DAILY) {
                    chart.dateAxis.tickCount = Math.min(maxDataCount, 5);
                } else if (axisType === PfmController.TIME_AXIS_WEEKLY) {
                    chart.customXAxis.tickCount = 4;
                }

                break;

                // custom & quarterly has the same formatting if defined
            case PfmQueryBarPrivate.TYPE_CUSTOM:
            case PfmQueryBarPrivate.TYPE_QUARTERLY:
                if (axisType === PfmController.TIME_AXIS_MONTHLY) {
                    chart.dateAxis.format = 'MMM';
                    chart.dateAxis.tickCount = 3;
                } else if (axisType === PfmController.TIME_AXIS_WEEKLY) {
                    chart.customXAxis.tickCount = 4;
                } else {
                    chart.dateAxis.format = 'dd/MM';
                    chart.dateAxis.tickCount = Math.min(maxDataCount, 7);
                }
                break;

            case PfmQueryBarPrivate.TYPE_WEEKLY:
            default:
                // default daily
                chart.dateAxis.tickCount = 7;
                chart.dateAxis.format = 'ddd';
                break;
            }
        }

        function updateAxis(chart, minMaxX, minMaxY) {
            var date = new Date();
            var i;

            if (minMaxX !== null && minMaxX.min !== null) {
                if (d.axisType === PfmController.TIME_AXIS_WEEKLY) {
                    chart.customXAxis.clearLabels();

                    chart.customXAxis.min = minMaxX.min;
                    chart.customXAxis.max = minMaxX.max;

                    chart.customXAxis.append('0', 0);

                    var weekNumber;
                    var tickCount = 4;
                    var totalIterator = parseInt((minMaxX.max - minMaxX.min) / tickCount, 10);

                    for (i = 0; i <= tickCount; i++) {
                        date.setTime(minMaxX.min + i * totalIterator);
                        weekNumber = Utility.getWeekNumber(date);
                        chart.customXAxis.append('W ' + weekNumber, date.getTime());
                    }

                } if (queryBar.type === PfmQueryBarPrivate.TYPE_QUARTERLY && d.axisType === PfmController.TIME_AXIS_MONTHLY) {
                    chart.customXAxis.clearLabels();

                    chart.customXAxis.min = minMaxX.min;
                    chart.customXAxis.max = minMaxX.max;

                    date.setTime(minMaxX.min);
                    chart.customXAxis.append(Qt.formatDate(date, 'MMM'), minMaxX.min);

                    var previousMonth = date.getMonth();

                    date.setTime(minMaxX.min + 31 * PfmQueryBarPrivate.A_DAY);
                    if (previousMonth !== date.getMonth()) {
                        date.setTime(date.getTime() + 3 * PfmQueryBarPrivate.A_DAY);
                        date.setDate(1);
                    }

                    chart.customXAxis.append(Qt.formatDate(date, 'MMM'), date.getTime());

                    date.setTime(minMaxX.max);
                    chart.customXAxis.append(Qt.formatDate(date, 'MMM'), minMaxX.max);

                } else {
                    var minDate = new Date();
                    minDate.setTime(minMaxX.min);

                    var maxDate = new Date();
                    maxDate.setTime(minMaxX.max);

                    chart.dateAxis.min = minDate;
                    chart.dateAxis.max = maxDate;
                }
            }

            if (minMaxY !== null && minMaxY.min !== null) {
                var maxRange = parseInt(minMaxY.max + getMaxMargin(minMaxY.max), 10);
                var halfRange = parseInt(maxRange / 2, 10);

                if (d.timeFormat) {
                    chart.customYAxis.clearLabels();

                    chart.customYAxis.min = 0;
                    chart.customYAxis.max = maxRange;

                    chart.customYAxis.append('0s', 0);

                    if (halfRange !== 0)
                        chart.customYAxis.append(DateTime.timeFormatter('?d ?h ?m !s', halfRange), halfRange);

                    if (maxRange !== 0)
                        chart.customYAxis.append(DateTime.timeFormatter('?d ?h ?m !s', maxRange), maxRange);

                } else {
                    chart.valueAxis.min = 0;
                    chart.valueAxis.max = maxRange;
                }
            }
        }

        function updateChartAxis(action, type) {
            if (!isAxisVisible(action, type, queryBar.dd.startDateRef, queryBar.dd.finishDateRef))
                return;

            var item = bodyLoader.item;
            if (!item) {
                console.log('[PfmChartBlock]', 'Inner chart item is not loaded yet.');
                return;
            }

            var chart = item.chart;
            var dataBridge = chart.dataBridge;
            var axisType = PfmController.TIME_AXIS_DAILY;
            var data = {
                current: d.currentDataCache,
                previous: d.previousDataCache,
                boundry: {
                    x: d.minMaxXCache,
                    y: d.minMaxYCache
                }
            };

            switch (action) {
            case 'weekly':
                axisType = PfmController.TIME_AXIS_WEEKLY;
                data = convertDailyToWeekly(data.current, data.previous);
                recalculateGranularity(PfmController.GRANULARITY_WEEKLY);
                break;
            case 'monthly':
                axisType = PfmController.TIME_AXIS_MONTHLY;
                data = convertDailyToMonthly(data.current, data.previous);
                recalculateGranularity(PfmController.GRANULARITY_MONTHLY);
                break;
            case 'daily':
            default:
                axisType = PfmController.TIME_AXIS_DAILY;
                recalculateGranularity(PfmController.GRANULARITY_DAILY);
            }

            d.axisType = axisType;
            setAxisFormats(chart, axisType, type, Math.max(data.previous.length, data.current.length));

            dataBridge.setPoints(chart.previousSeries.upperSeries, data.previous);
            dataBridge.setPoints(chart.currentSeries.upperSeries, data.current);
            dataBridge.setPoints(chart.scatterSeries, data.current);

            updateAxis(chart, data.boundry.x, data.boundry.y);
        }

        function convertDailyToWeekly(current, previous) {
            var minMax = { x: null, y: null };
            var currentData = convertDailyToWeeklyAtomic(current, minMax);
            var previousData = convertDailyToWeeklyAtomic(previous, minMax);

            return { current: currentData, previous: previousData, boundry: minMax };
        }

        function convertDailyToWeeklyAtomic(data, minMax) {
            var weekly = [];
            var lastWeekNumber = -1;
            var currentWeekNumber = -1;
            var i = 0;
            var len = data.length;
            var date = new Date();
            var row = null;
            for (; i < len; i++) {
                date.setTime(data[i].x);
                currentWeekNumber = Utility.getWeekNumber(date);

                if (lastWeekNumber === -1 || lastWeekNumber !== currentWeekNumber) {
                    if (row !== null) {
                        weekly.push(row);
                        setMinMax(row, minMax);
                    }

                    row = Qt.point(data[i].x, 0);
                }

                lastWeekNumber = currentWeekNumber;
                row.x = Math.min(row.x, data[i].x);
                row.y += data[i].y;
            }

            if (row !== null) {
                weekly.push(row);
                setMinMax(row, minMax);
            }

            return weekly;
        }

        function convertDailyToMonthly(current, previous) {
            var minMax = { x: null, y: null };
            var currentData = convertDailyToMonthlyAtomic(current, minMax);
            var previousData = convertDailyToMonthlyAtomic(previous, minMax);

            return { current: currentData, previous: previousData, boundry: minMax };
        }

        function convertDailyToMonthlyAtomic(data, minMax) {
            var monthly = [];
            var lastMonthNumber = -1;
            var currentMonthNumber = -1;
            var i = 0;
            var len = data.length;
            var date = new Date();
            var row = null;
            for (; i < len; i++) {
                date.setTime(data[i].x);
                currentMonthNumber = date.getMonth();

                if (lastMonthNumber === -1 || lastMonthNumber !== currentMonthNumber) {
                    if (row !== null) {
                        monthly.push(row);
                        setMinMax(row, minMax);
                    }

                    row = Qt.point(data[i].x, 0);
                }

                lastMonthNumber = currentMonthNumber;
                row.y += data[i].y;
            }

            if (row !== null) {
                monthly.push(row);
                setMinMax(row, minMax);
            }

            return monthly;
        }

        function setMinMax(row, minMax) {
            if (minMax.x === null)
                minMax.x = { min: Number.MAX_VALUE, max: Number.MIN_VALUE };

            if (minMax.y === null)
                minMax.y = { min: Number.MAX_VALUE, max: Number.MIN_VALUE };

            minMax.x.min = Math.min(minMax.x.min, row.x);
            minMax.x.max = Math.max(minMax.x.max, row.x);

            minMax.y.min = Math.min(minMax.y.min, row.y);
            minMax.y.max = Math.max(minMax.y.max, row.y);
        }

        function getMaxMargin(max) {
            var step = max / 50;
            if (step <= 1) {
                return 10;
            } else {
                return 10 * step;
            }
        }

        function handleMenuTap(action, rightMenu) {
            rightMenu.close();
            updateChartAxis(action, queryBar.type);
        }

        function isMenuVisible(type) {
            return type !== PfmQueryBarPrivate.TYPE_WEEKLY;
        }

        function isAxisVisible(action, type, startDateRef, finishDateRef) {
            var availableAxisList = PfmQueryBarPrivate.getAvailableAxisList(startDateRef, finishDateRef, type);
            var axisType = null;
            switch (action) {
            case 'daily':
                axisType = PfmController.TIME_AXIS_DAILY;
                break;
            case 'weekly':
                axisType = PfmController.TIME_AXIS_WEEKLY;
                break;
            case 'monthly':
                axisType = PfmController.TIME_AXIS_MONTHLY;
                break;
            }

            return axisType !== null ? availableAxisList.indexOf(axisType) > -1 : false
        }

        function isCustomXVisible(axisType, queryType) {
            return axisType === PfmController.TIME_AXIS_WEEKLY
                    || (queryType === PfmQueryBarPrivate.TYPE_QUARTERLY && axisType === PfmController.TIME_AXIS_MONTHLY);
        }

        function isScatterAxisVisible(axisType, type, startDateRef, finishDateRef) {
            var availableAxisList = PfmQueryBarPrivate.getAvailableAxisList(startDateRef, finishDateRef, type);
            if (axisType === PfmController.TIME_AXIS_DAILY
                    && (type === PfmQueryBarPrivate.TYPE_MONTHLY || type === PfmQueryBarPrivate.TYPE_QUARTERLY || type === PfmQueryBarPrivate.TYPE_CUSTOM)
                    && availableAxisList.indexOf(PfmController.TIME_AXIS_DAILY) > -1) {
                return false;
            }

            return true;
        }

        function handleQueryChanged() {
            var item = bodyLoader.item;
            if (!item) {
                console.log('[PfmChartBlock]', 'Inner chart item is not loaded yet.');
                return;
            }

            item.rightMenu.close();
        }
    }

    body: Item {
        property alias chart: chart
        property alias rightMenu: rightMenu

        id: bodyRoot

        Item {
            anchors.fill: parent
            Column {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter

                Item {
                    width: parent.width
                    height: dp(46)
                    z: 99

                    PfmText {
                        font.pixelSize: sp(22)
                        text: root.title
                        color: '#1B2B30'
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: dp(10)
                    }

                    Item {
                        width: parent.height
                        height: parent.height
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                        visible: d.isMenuVisible(queryBar.type)

                        Column {
                            width: dp(20)
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.verticalCenterOffset: dp(-1)
                            anchors.right: parent.right
                            spacing: dp(2)
                            Repeater {
                                model: 3
                                Rectangle{
                                    height: dp(4)
                                    width: height
                                    radius: height / 2
                                    color: "gray"
                                }
                            }
                        }

                        MouseArea {
                            anchors.fill: parent
                            preventStealing: true
                            onClicked: rightMenu.toggle()
                        }

                        Rectangle {
                            id: rightMenu
                            width: dp(130)
                            height: menuContent.height + menuTitle.height
                            radius: dp(2)
                            visible: false
                            clip: true
                            anchors.right: parent.right
                            anchors.top: parent.top
                            anchors.rightMargin: dp(5)
                            anchors.topMargin: dp(5)
                            color: '#F8F8F8'

                            function toggle() {
                                if (visible)
                                    close();
                                else
                                    open();
                            }

                            function open() {
                                visible = true
                            }

                            function close() {
                                visible = false
                            }

                            MouseArea {
                                anchors.fill: parent
                                preventStealing: true
                                onClicked: rightMenu.close()
                            }

                            Item {
                                id: menuTitle

                                width: parent.width
                                height: dp(43)

                                PfmText {
                                    font.pixelSize: sp(16)
                                    text: qsTr("Show by:")
                                    color: '#1B2B30'
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.left: parent.left
                                    anchors.leftMargin: dp(10)
                                }

                                Column {
                                    width: dp(20)
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.right: parent.right
                                    anchors.rightMargin:  -dp(5)
                                    anchors.verticalCenterOffset: -dp(5)
                                    spacing: dp(2)
                                    Repeater {
                                        model: 3
                                        Rectangle {
                                            height: dp(4)
                                            width: height
                                            radius: height / 2
                                            color: "gray"
                                        }
                                    }
                                }
                            }

                            Column {
                                id: menuContent
                                width: parent.width
                                anchors.top: menuTitle.bottom

                                Repeater {
                                    model: d.chartAxisSelectionModel
                                    delegate: Rectangle {
                                        width: parent.width
                                        height: dp(35)
                                        color: menuMouse.pressed ? '#ededed' : 'transparent'
                                        visible: d.isAxisVisible(model.action, queryBar.type, queryBar.dd.startDateRef, queryBar.dd.finishDateRef)
                                        PfmText {
                                            font.pixelSize: sp(14)
                                            text: title
                                            color: '#1B2B30'
                                            anchors.verticalCenter: parent.verticalCenter
                                            anchors.left: parent.left
                                            anchors.leftMargin: dp(10)
                                        }
                                        MouseArea {
                                            id: menuMouse

                                            anchors.fill: parent
                                            preventStealing: true
                                            onClicked: d.handleMenuTap(model.action, rightMenu)
                                        }
                                    }
                                }
                            }
                        }

                        DropShadow {
                            id: rightShadow
                            anchors.fill: rightMenu
                            horizontalOffset: 0
                            verticalOffset: dp(2)
                            radius: 8.0
                            samples: 17
                            color: "#80000000"
                            source: rightMenu
                            visible: rightMenu.visible
                            z:-1
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    height: dp(200)
                    color: '#ebebeb'

                    ChartView {
                        property int space: 0
                        property alias dataBridge: dataBridge
                        property alias dateAxis: dateAxis
                        property alias valueAxis: valueAxis
                        property alias customYAxis: customYAxis
                        property alias customXAxis: customXAxis
                        property alias previousSeries: previousSeries
                        property alias currentSeries: currentSeries
                        property alias scatterSeries: scatterSeries

                        id: chart

                        width: parent.width + space * 2
                        height: parent.height + space * 2
                        x: -space
                        y: -space

                        antialiasing: true
                        dropShadowEnabled: false
                        legend.visible: false
                        backgroundRoundness: 0
                        backgroundColor: '#ebebeb'
                        plotAreaColor: '#ffffff'
                        visible: !root.inProgress

                        margins.left: 0
                        margins.right: 0
                        margins.top: 0
                        margins.bottom: 0

                        DateTimeAxis {
                            id: dateAxis
                            format: 'ddd'
                            tickCount: 7
                            labelsAngle: -45
                            labelsColor: '#8E9297'
                            labelsFont.family: 'Avenir'
                            labelsFont.pixelSize: sp(12)
                            gridLineColor: '#D0D0D0'
                            color: '#D1CECD'
                            visible: !d.isCustomXVisible(d.axisType, queryBar.type)
                        }

                        CategoryAxis {
                            id: customXAxis
                            labelsPosition: CategoryAxis.AxisLabelsPositionOnValue
                            labelsAngle: -45
                            labelsColor: '#8E9297'
                            labelsFont.family: 'Avenir'
                            labelsFont.pixelSize: sp(12)
                            gridLineColor: '#D0D0D0'
                            color: '#D1CECD'
                            visible: d.isCustomXVisible(d.axisType, queryBar.type) && d.showCustomXAxis
                            onVisibleChanged: if (!visible) clearLabels();

                            function clearLabels() {
                                var labelsToRemove = [];
                                for (var i in categoriesLabels) {
                                    if (categoriesLabels.hasOwnProperty(i))
                                        labelsToRemove.unshift(categoriesLabels[i]);
                                }

                                i = 0;
                                var len = labelsToRemove.length;
                                for (; i < len; i++)
                                    remove(labelsToRemove[i]);
                            }
                        }

                        ValueAxis {
                            id: valueAxis
                            tickCount: 3
                            labelFormat: '%d'
                            labelsColor: '#8E9297'
                            labelsFont.family: 'Avenir'
                            labelsFont.pixelSize: sp(12)
                            gridLineColor: '#D0D0D0'
                            color: '#D1CECD'
                            visible: !d.timeFormat
                        }

                        CategoryAxis {
                            id: customYAxis
                            labelsPosition: CategoryAxis.AxisLabelsPositionOnValue
                            labelsColor: '#8E9297'
                            labelsFont.family: 'Avenir'
                            labelsFont.pixelSize: sp(12)
                            gridLineColor: '#D0D0D0'
                            color: '#D1CECD'
                            visible: d.timeFormat
                            onVisibleChanged: if (!visible) clearLabels();

                            function clearLabels() {
                                var labelsToRemove = [];
                                for (var i in categoriesLabels) {
                                    if (categoriesLabels.hasOwnProperty(i))
                                        labelsToRemove.unshift(categoriesLabels[i]);
                                }

                                var i = 0;
                                var len = labelsToRemove.length;
                                for (; i < len; i++)
                                    remove(labelsToRemove[i]);
                            }
                        }

                        AreaSeries {
                            id: previousSeries

                            name: "Previous"
                            color: "#4DFEF0EB"
                            borderColor: "#FCDDD0"
                            borderWidth: dp(2)
                            axisX: d.isCustomXVisible(d.axisType, queryBar.type) ? customXAxis : dateAxis
                            axisY: d.timeFormat ? customYAxis : valueAxis
                            useOpenGL: true
                            upperSeries: LineSeries {}
                        }

                        AreaSeries {
                            id: currentSeries

                            name: "Current"
                            color: "#4DB8D6E6"
                            borderColor: "#5F9EAC"
                            borderWidth: dp(2)
                            axisX: d.isCustomXVisible(d.axisType, queryBar.type) ? customXAxis : dateAxis
                            axisY: d.timeFormat ? customYAxis : valueAxis
                            useOpenGL: true
                            upperSeries: LineSeries {}
                        }

                        ScatterSeries {
                            id: scatterSeries

                            borderColor: '#5F9EAC'
                            color: '#fff'
                            markerSize: dp(8)
                            borderWidth: dp(2)
                            axisX: d.isCustomXVisible(d.axisType, queryBar.type) ? customXAxis : dateAxis
                            axisY: d.timeFormat ? customYAxis : valueAxis
                            visible: d.isScatterAxisVisible(d.axisType, queryBar.type, queryBar.dd.startDateRef, queryBar.dd.finishDateRef)
                            // useOpenGL: true this cannot render circlar scatter
                        }

                        QtChartDataBridge {
                            id: dataBridge
                        }
                    }

                    PfmBusyIndicator {
                        anchors.centerIn: parent
                        visible: root.inProgress
                    }
                }

                Item {
                    width: parent.width
                    height: dp(90)

                    RowLayout {
                        anchors.fill: parent
                        anchors.margins: dp(10)
                        spacing: dp(10)

                        Column {
                            width: dp(90)
                            Layout.preferredWidth: dp(90)

                            PfmText {
                                font.pixelSize: sp(16)
                                text: PfmQueryBarPrivate.getStatPreviousText(queryBar.type, queryBar.queryToYearBackForPrevious)
                                color: '#F9A787'
                                anchors.horizontalCenter: parent.horizontalCenter
                            }

                            PfmText {
                                font.pixelSize: sp(22)
                                color: '#000'
                                text: previous === '' ? 'N/A' : previous
                                anchors.horizontalCenter: parent.horizontalCenter
                            }
                        }

                        Item {
                            Layout.fillWidth: true
                            height: parent.height

                            AvgTextBigStyle {
                                anchors.horizontalCenter: parent.horizontalCenter
                                y: (parent.height - height) / 2 + dp(2)
                                avg: root.avg
                            }
                        }

                        Column {
                            width: dp(90)
                            Layout.preferredWidth: dp(90)

                            PfmText {
                                font.pixelSize: sp(16)
                                text: PfmQueryBarPrivate.getStatText(queryBar.type)
                                color: '#6BBDE4'
                                anchors.horizontalCenter: parent.horizontalCenter
                            }

                            PfmText {
                                font.pixelSize: sp(22)
                                color: '#000'
                                text: current === '' ? 'N/A' : current
                                anchors.horizontalCenter: parent.horizontalCenter
                            }
                        }
                    }
                }
            }
        }
    }

    Connections {
        target: queryBar
        onQueryChanged: d.handleQueryChanged()
    }
}
