import QtQuick 2.5
import QtQuick.Layouts 1.1
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0

Item {
    property alias title: title
    property var selectedDate: null
    property bool checked: false

    height: dp(70)
    anchors.left: parent.left
    anchors.right: parent.right

    onSelectedDateChanged: d.handleSelectedDateChanged()

    QtObject {
        id: d

        property bool pickerVisible: false

        function init() {

        }

        function handleTap() {
            if (d.pickerVisible)
                return;

            d.pickerVisible = true;
            datePicker.show(selectedDate, function(date) {
                d.pickerVisible = false;
                if (date)
                    selectedDate = date;
            });
        }

        function handleSelectedDateChanged() {
            if (!d.pickerVisible)
                return;

            datePicker.date = selectedDate;
        }
    }

    RowLayout {
        anchors.fill: parent
        anchors.leftMargin: dp(15)
        anchors.rightMargin: dp(15)

        Item {
            Layout.fillWidth: true
            height: parent.height

            PfmText {
                id: title

                font.weight: Font.Medium
                font.pixelSize: sp(16)
                text: 'N/A'
                color: '#808080'

                anchors.verticalCenter: parent.verticalCenter
            }
        }

        Item {
            implicitWidth: dp(90)
            implicitHeight: parent.height

            PfmText {
                font.pixelSize: sp(14)
                text: selectedDate ? DateTime.date('M d, Y', selectedDate) : 'N/A'
                color: '#808080'

                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }

    TouchInteractionButton {
        id: listInteraction
        circlar: false
        width: parent.width
        height: parent.height
        visible: mouse.enabled
    }

    MouseArea {
        id: mouse

        anchors.fill: parent
        preventStealing: true
        onClicked: d.handleTap()
        onPressedChanged: pressed ? listInteraction.press() : listInteraction.release()
    }
}
