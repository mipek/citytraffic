.import Frame.Mobile.Core 1.0 as FrameCore

function Adapter(pfmController, manager) {
    this.m_pfmController = pfmController;
    this.m_manager = manager;
}

var inProcess = true
Adapter.prototype = {
    getToken: function() {
        var loggedInUser = this.m_manager.getLoggedInUser();
        if (!loggedInUser) {
            console.log('[DataManager]', 'User is not logged in.');
            return null;
        }

        return loggedInUser.token;
    },
    requestLogin: function(params) {
        console.log("logging in...")
        var m_closure = this;

        this.m_pfmController.api('POST', 'api/Authentication/Authenticate', function(data, xhr) {
            m_closure.m_pfmController.handleResponse(params, data, function(p, d) {
                if (d.Data === null || !d.Data.hasOwnProperty('Succeed') || !d.Data.Succeed) {
                    params.onFailed('InvalidCredentials', d.Meta.Code, d.Meta.Message);
                    return true;
                }
            });
        }, {
            Data: {
                UserName: params.username,
                Password: params.password
            }
        },params.token);

    },
    requestToken: function(params) {
        var m_closure = this;

        //TODO
        var appid = "3a9c30c8-2629-4ace-9a53-a5f36b904fcf"
        var appkey = "c809ecac-dd36-4852-ab46-f3d4261be685"
        var respToken
        var controller = this.m_pfmController

        this.m_pfmController.api('POST', 'api/Authentication/Token', function(respdata, xhr) {
            var resp = JSON.parse(respdata)
            respToken = resp.Data.Token

            console.log("respToken : ",respToken)
            params.onSuccess({token:respToken})
        }, {
            Data: {
                appid: appid,
                appkey: appkey
            }
        });
    },
    requestLocationData: function(params) {
        console.log("user id : ",params.loginInfo.user_id)
        var token = this.getToken();
        if (!token)
            return;

        var m_closure = this;
        this.m_pfmController.api('POST', 'data/Location/GetLocations', function(data, xhr) {
            m_closure.m_pfmController.handleResponse(params, data, function(p, d) {
                var i = 0;
                var len = d.Data === null ? 0 : d.Data.length;
                var row;
                var list = {};
                for (; i < len; i++) {
                    row = d.Data[i];
                    list[row.id] = {
                        location_id:  row.id,
                        name:         row.name,
                        opening_time: row.openingtime,
                        closing_time: row.closingtime,
                        options:      row.options
                    };
                }

                d.Data = list;
            });
        }, {
            "Data": params.loginInfo.user_id
        }, token);
    },
    requestSensorVisitData: function(params) {
        var token = this.getToken();
        if (!token)
            return;

        var m_closure = this;
        var data = params.data || {};
        this.m_pfmController.api('POST', 'data/Location/GetVisitCount', function(data, xhr) {
            m_closure.m_pfmController.handleResponse(params, data, function(p, d) {
                var i = 0;
                var len = d.Data === null ? 0 : d.Data.length;
                var row;
                var list = {};
                for (; i < len; i++) {
                    row = d.Data[i];
                    list[row.StartDate.split('T')[0]] = {
                        total_count: row.TotalVisits
                    };
                }

                d.Data = list;
            });
        }, {
            "Data": {
                loc: params.locationId,
                start: this.m_pfmController.getDateQueryString(params.startDate),
                end: this.m_pfmController.getDateQueryString(params.finishDate, true),
            }
        }, token);
    },
    requestWifiVisitData: function(params) {
        var token = this.getToken();
        if (!token)
            return;

        var data = params.data || {};
        var m_closure = this;
        this.m_pfmController.api('POST', 'data/Location/GetWifiData', function(data, xhr) {
            m_closure.m_pfmController.handleResponse(params, data, function(p, d) {
                var i = 0;
                var len = d.Data === null ? 0 : d.Data.length;
                var row;
                var list = {};
                for (; i < len; i++) {
                    row = d.Data[i];
                    list[row.date.split('T')[0]] = {
                        capture_rate:   row.capturerate,
                        unique_percent: row.uniqueper,
                        new_percent:    row.newper,
                        dwell_time:     row.dwelltime,
                        frequency:      row.frequency
                    };
                }

                d.Data = list;
            });
        }, {
            "Data": {
                loc: params.locationId,
                start: this.m_pfmController.getDateQueryString(params.startDate),
                end: this.m_pfmController.getDateQueryString(params.finishDate, true),
            }
        }, token);
    },

    requestSensorGranularityData: function(params) {
        var token = this.getToken();
        if (!token)
            return;

        var m_closure = this;
        var startDate = this.m_pfmController.getDateQueryString(params.startDate);
        var finishDate = this.m_pfmController.getDateQueryString(params.finishDate, true);

        this.m_pfmController.api('POST', 'data/Location/GetVisitCountByGranularity', function(data, xhr) {
            m_closure.m_pfmController.handleResponse(params, data, function(p, d) {
                var len = d.Data === null ? 0 : d.Data.length;
                var result = null;
                var current;
                if (len > 0) {
                    current = d.Data[0];
                    result = {
                        total_count: current.total,
                        start_date:  startDate,
                        finish_date: finishDate
                    };
                }

                d.Data = result;
            });
        }, {
            "Data": {
                loc: params.locationId,
                start: startDate,
                end: null,
                granularity: params.granularity
            }
        }, token);
    },

    requestWifiGranularityData: function(params) {
        var token = this.getToken();
        if (!token)
            return;

        var data = params.data || {};
        var m_closure = this;
        var startDate = this.m_pfmController.getDateQueryString(params.startDate);
        var finishDate = this.m_pfmController.getDateQueryString(params.finishDate, true);

        this.m_pfmController.api('POST', 'data/Location/GetWifiDataByGranularity', function(data, xhr) {
            m_closure.m_pfmController.handleResponse(params, data, function(p, d) {
                var len = d.Data === null ? 0 : d.Data.length;
                var result = null;
                var current;
                if (len > 0) {
                    current = d.Data[0];
                    result = {
                        capture_rate:    current.capturerate,
                        unique_percent:  current.uniqueper,
                        new_percent:     current.newper,
                        dwell_time:      current.dwelltime,
                        frequency:       current.frequency,
                        start_date:      startDate,
                        finish_date:     finishDate
                    };
                }

                d.Data = result;
            });
        }, {
            "Data": {
                loc: params.locationId,
                start: startDate,
                end: finishDate,
                granularity: params.granularityType
            }
        }, token);
    },

    requestSyncList: function(params) {
        var token = this.getToken();
        if (!token)
            return;

        var data = params.data || {};
        var m_closure = this;

        var loggedInUser = this.m_manager.getLoggedInUser();


        this.m_pfmController.api('POST', 'data/Location/getsynchlist', function(data, xhr) {
            m_closure.m_pfmController.handleResponse(params, data, function(p, d) {
                var len = d.Data === null ? 0 : d.Data.length;
                var result = [];
                var currentData;
                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        currentData = d.Data[i];
                        result.push({
                                        locationId: currentData.id,
                                        date:       currentData.date.split('T')[0],
                                        isWifi:     currentData.option === 2,
                                        isSensor:   currentData.option === 1
                                    });
                    }
                }

                d.Data = result;
            });
        }, {
            "Data": {
                date: this.m_pfmController.getDateQueryString(params.date),
                user: loggedInUser.user_id
            }
        }, token);
    },

    saveLocationData: function(data) {},
    saveLoggedInUser: function(data) {},
    getLoggedInUser: function() { return null; }
};

