import QtQuick 2.5

QtObject {
    property bool canvasWidthLargerThan375: applicationWindow.width >= dp(375)

    property int queryBarDateFontSize: 0
    property int queryBarSubTextFontSize: 0
    property int overviewLocationFontSize: 0
    property int overviewNumberFontSize: 0

    function switchTheme() {
        if (canvasWidthLargerThan375) {
            console.log('[PfmTheme]', 'Switching Canvas 375 >=')

            queryBarDateFontSize = sp(14);
            queryBarSubTextFontSize = sp(15);
            overviewLocationFontSize = sp(18);
            overviewNumberFontSize = sp(20);
        } else {

            console.log('[PfmTheme]', 'Switching Canvas 375 <')

            queryBarDateFontSize = sp(11);
            queryBarSubTextFontSize = sp(13);
            overviewLocationFontSize = sp(18);
            overviewNumberFontSize = sp(15);
        }
    }

    onCanvasWidthLargerThan375Changed: switchTheme()
    Component.onCompleted: switchTheme()
}

