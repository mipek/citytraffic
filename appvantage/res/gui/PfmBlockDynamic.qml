import QtQuick 2.5
import QtGraphicalEffects 1.0

Item {
    property real horizontalPadding: dp(5)
    property real verticalPadding: dp(5)

    property alias bodyLoader: loader
    property Component body
    property alias container: container

    id: root

    width: parent.width
    height: (loader.item ? loader.item.implicitHeight : dp(60)) + verticalPadding * 2

    Rectangle {
        id: container

        width: parent.width - horizontalPadding * 2
        height: loader.item ? loader.item.implicitHeight : dp(60)

        anchors.centerIn: parent
        color: '#fff'
        radius: dp(2)

        layer.enabled: true
        layer.effect: DropShadow {
            radius: 4
            samples: radius * 2
            source: container
            color: Qt.rgba(0, 0, 0, 0.1)
            transparentBorder: true
        }

        Loader {
            id: loader
            width: container.width

            sourceComponent: body
        }
    }
}

