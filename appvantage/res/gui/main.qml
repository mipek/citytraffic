import QtQuick 2.5
import QtQuick.Controls 1.3
import QtGraphicalEffects 1.0
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0
import com.triodor.pfm 1.0
import QtQuick.LocalStorage 2.0

ApplicationWindow {
    property alias datePicker: datePicker

    id: applicationWindow

    width: ScreenInfo.dp(375) // ScreenInfo.dp(375)
    height: ScreenInfo.dp(568)
    visible: true

    onWidthChanged: console.log('[ApplicationWindow]', 'Canvas Size:', Qt.size(width / ScreenInfo.scaleFactor, height / ScreenInfo.scaleFactor))
    onHeightChanged: console.log('[ApplicationWindow]', 'Canvas Size:', Qt.size(width / ScreenInfo.scaleFactor, height / ScreenInfo.scaleFactor))

    function imagePath(value) {
        var scaleFactor = ApplicationInfo.devMode ? 2 : Math.min(4, ScreenInfo.imageScaleFactor);
        var imgPath = 'qrc:/res/img/' + scaleFactor + 'x/' + value;

        return imgPath;
    }

    function qmlPath(value) {
        return 'qrc:/res/gui/' + value;
    }

    function fontPath(value) {
        return 'qrc:/res/font/' + value;
    }

    function dp(px) {
        return ScreenInfo.dp(px);
    }

    function sp(px) {
        return ScreenInfo.sp(px);
    }

    function start() {
        startAnimation.start();
    }

    function handleLoaded() {
        start();
    }

    function completeStartSequence() {
        splashCover.visible = false;
        mainLoader.visible = true;
    }

    SequentialAnimation {
        id: startAnimation

        ScriptAction {
            // break splash overlay binding first
            script: splash.y = parseFloat(splash.y);
        }

        ParallelAnimation {
            PropertyAnimation {
                target: splash
                property: 'y'
                from: target.y
                to: dp(50)
                duration: 300
                easing.type: Easing.InOutQuad
            }

            PropertyAnimation {
                target: splash
                properties: 'width'
                from: target.width
                to: dp(192)
                duration: 300
                easing.type: Easing.InOutQuad
            }

            PropertyAnimation {
                target: splash
                properties: 'height'
                from: target.height
                to: dp(100)
                duration: 300
                easing.type: Easing.InOutQuad
            }
        }

        onRunningChanged: if (!running) completeStartSequence()
    }

    Item {
        anchors.fill: parent
        Loader {
            id: mainLoader

            anchors.fill: parent
            asynchronous: true

            onLoaded: handleLoaded()
        }

        Item {
            id: splashCover

            anchors.fill: parent
            visible: true

            Image {
                width: parent.width
                height: parent.height
                source: imagePath('splash.png')
                fillMode: Image.PreserveAspectCrop
                verticalAlignment: Image.AlignVCenter
            }

            Image {
                id: splash

                source: imagePath('splash-logo.png')
                width: dp(256)
                height: dp(133)

                x: (parent.width - width) / 2
                y: (parent.height - height) / 2
            }
        }

        Timer {
            running: true
            interval: 100
            onTriggered: mainLoader.source = qmlPath('LandPage.qml')
        }
    }

    PfmTheme {
        id: theme
    }

    Item {
        /* to avoid weird signal loop error */
        anchors.fill: parent
        z: 9999

        DatePicker {
            id: datePicker
        }
    }

    /*LogConsole {
        id: logConsole
        active: ApplicationInfo.testMode
        logType: LogBroadcaster.Info
        autoShowWhenHitWarning: true
    }*/
}


