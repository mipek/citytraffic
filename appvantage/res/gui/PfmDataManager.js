.pragma library
.import Frame.Mobile.Core 1.0 as FrameCore
.import "PfmDataManagerMemory.js" as MemoryManager
.import "PfmDataManagerSQL.js" as SQLManager
.import "PfmDataManagerAPI.js" as APIManager

var TAG = '[PfmDataManager]';

function Adapter(pfmController) {
    this.m_pfmController = pfmController;
    this.m_memoryManager = new MemoryManager.Adapter(pfmController, this);
    this.m_sqlManager = new SQLManager.Adapter(pfmController, this);
    this.m_apiManager = new APIManager.Adapter(pfmController, this);
    this.m_useDatabase = true;
    this.m_dbDataCache = true;
}

Adapter.prototype = {
    useDatabase: function(useDb, useDbCache) {
        this.m_useDatabase = useDb ? true : false;
        this.m_dbDataCache = (this.m_useDatabase && useDbCache) ? true : false;
        console.log(TAG, 'Using Database:', this.m_useDatabase, 'Using Db Data Cache:', this.m_dbDataCache);
    },

    clearData: function() {
        if (this.m_useDatabase)
            this.m_sqlManager.clearData();

        this.m_memoryManager.clearData();
        console.log(TAG, 'All data has been deleted.');
    },

    getLoggedInUser: function() {
        return this.m_memoryManager.getLoggedInUser();
    },

    getSavedUser: function() {
        if (!this.m_useDatabase)
            return null;

        return this.m_sqlManager.getSavedUser();
    },

    hasLocationData: function() {
        return this.m_memoryManager.getLocationData() !== null;
    },
    getLocationData: function() {
        return this.m_memoryManager.getLocationData();
    },
    getSensorVisitData: function() {
        return this.m_memoryManager.getSensorVisitData();
    },
    getWifiVisitData: function() {
        return this.m_memoryManager.getWifiVisitData();
    },

    getSensorGranularityData: function() {
        return this.m_memoryManager.getSensorGranularityData();
    },
    getWifiGranularityData: function() {
        return this.m_memoryManager.getWifiGranularityData();
    },

    fillBlanks: function(data, dateList, template) {
        var i = 0;
        var len = dateList.length;
        var date;
        for (; i < len; i++) {
            date = dateList[i];
            if (!data.hasOwnProperty(date))
                data[date] = JSON.parse(JSON.stringify(template));
        }
    },
    requestLogin: function(listenerItem, username, password, remember,token) {
        var m_closure = this;
        this.m_apiManager.requestLogin({
                                           username: username,
                                           password: password,
                                           token : token,
                                           onSuccess: function(data) {
                                               data.Token = token
                                               m_closure._postLoginInfoToListenerItem(listenerItem, data, remember, username, password);
                                           },
                                           onFailed: function(errorId, code, message) {
                                               m_closure._postFailedLoginInfoToListenerItem(listenerItem, errorId, code, message);
                                           }
                                       });
    },
    requestToken: function(listenerItem, username, password, remember) {
        console.log("requesting token...")
        var m_closure = this;
        this.m_apiManager.requestToken({
                                           username: username,
                                           password: password,
                                           onSuccess: function(data) {
                                               console.log("token is : ",data.token)
                                               m_closure.requestLogin(listenerItem, username, password, remember,data.token)
                                               // m_closure._postLoginInfoToListenerItem(listenerItem, data, remember, username, password);
                                           },
                                           onFailed: function(errorId, code, message) {
                                               // m_closure._postFailedLoginInfoToListenerItem(listenerItem, errorId, code, message);
                                           }
                                       });
    },
    requestLogout: function(listenerItem) {
        this.m_memoryManager.saveLoggedInUser(null);
        if (this.m_useDatabase)
            this.m_sqlManager.deleteUser();

        this.clearData();
        this._postLoggedOutToListenerItem(listenerItem);
    },

    requestSyncList: function(listenerItem, lastRequestDate) {
        var m_closure = this;
        this.m_apiManager.requestSyncList({
                                              date: lastRequestDate,
                                              //aca//userId : this.m_memoryManager.m_loginUser
                                              onSuccess: function(data) {
                                                  m_closure._postSyncListToListenerItem(listenerItem, data, lastRequestDate);
                                              },
                                              onFailed: function(errorId, code, message) {
                                                  m_closure._postFailedSyncListToListenerItem(listenerItem, errorId, code, message);
                                              }
                                          });
    },

    requestLocationData: function(listenerItem) {
        var m_closure = this;
        var locations = this.m_memoryManager.requestLocationData();
        if (locations !== null) {
            console.log(TAG, 'Locations found in memory.');
            m_closure._postLocationDataToListenerItem(listenerItem, locations);
            return;
        }

        if (this.m_dbDataCache) {
            locations = this.m_sqlManager.requestLocationData();
            if (locations !== null) {
                console.log(TAG, 'Locations found in db.');
                m_closure._postLocationDataToListenerItem(listenerItem, locations, true);
                return;
            }
        }

        console.log(TAG, 'Locations Requesting via API Manager.');
        this.m_apiManager.requestLocationData({
                                                  loginInfo: this.m_memoryManager.getLoggedInUser(),
                                                  onSuccess: function(data) {
                                                      m_closure._postLocationDataToListenerItem(listenerItem, data, true, true);
                                                  },

                                                  onFailed: function(errorId, code, message) {
                                                      m_closure._postFailedLocationDataToListenerItem(listenerItem, errorId, code, message);
                                                  }
                                              });
    },
    requestSensorVisitData: function (listenerItem, locationId, startDate, finishDate, type) {
        var m_closure = this;
        console.log('Start Finish Date', startDate, finishDate);
        var dateList = this.m_pfmController.getDateList(startDate, finishDate, type);
        var result = this.m_memoryManager.requestSensorVisitData(locationId, null, dateList);
        if (result.status === this.m_pfmController.COMPLETED) {
            console.log(TAG, 'Sensor Visit Data found in memory.');
            m_closure._postSensorVisitDataToListenerItem(listenerItem, locationId, result.data, dateList);
            return;
        }

        if (this.m_dbDataCache) {
            result = this.m_sqlManager.requestSensorVisitData(locationId, result.data, dateList);
            if (result.status === this.m_pfmController.COMPLETED) {
                console.log(TAG, 'Sensor Visit Data found in db.');
                m_closure._postSensorVisitDataToListenerItem(listenerItem, locationId, result.data, dateList, true);
                return;
            } else if (result.status === this.m_pfmController.PARTIAL) {
                this.m_memoryManager.saveSensorVisitData(locationId, result.data);
            }
        }

        console.log(TAG, 'Sensor Visit Data requeting via API.');
        this.m_apiManager.requestSensorVisitData({
                                                     data: result.data,
                                                     locationId: locationId,
                                                     startDate: startDate,
                                                     finishDate: finishDate,
                                                     onSuccess: function(data) {
                                                         m_closure._postSensorVisitDataToListenerItem(listenerItem, locationId, data, dateList, true, true);
                                                     },
                                                     onFailed: function(errorId, code, message) {
                                                         m_closure._postSensorVisitDataFailedToListenerItem(listenerItem, locationId, errorId, code, message, true, true);
                                                     }
                                                 });
    },
    requestWifiVisitData: function(listenerItem, locationId, startDate, finishDate, type) {
        var m_closure = this;
        var dateList = this.m_pfmController.getDateList(startDate, finishDate, type);
        var result = this.m_memoryManager.requestWifiVisitData(locationId, null, dateList);
        if (result.status === this.m_pfmController.COMPLETED) {
            console.log(TAG, 'Wifi Visit Data found in memory.');
            m_closure._postWifiDataToListenerItem(listenerItem, locationId, result.data, dateList);
            return;
        }

        if (this.m_dbDataCache) {
            result = this.m_sqlManager.requestWifiVisitData(locationId, result.data, dateList);
            if (result.status === this.m_pfmController.COMPLETED) {
                console.log(TAG, 'Wifi Visit Data found in db.');
                m_closure._postWifiDataToListenerItem(listenerItem, locationId, result.data, dateList, true);
                return;
            } else if (result.status === this.m_pfmController.PARTIAL) {
                this.m_memoryManager.saveWifiVisitData(locationId, result.data);
            }
        }

        console.log(TAG, 'Wifi Visit Data requesting via API.');
        this.m_apiManager.requestWifiVisitData({
                                                   data: result.data,
                                                   locationId: locationId,
                                                   startDate: startDate,
                                                   finishDate: finishDate,
                                                   onSuccess: function(data) {
                                                       m_closure._postWifiDataToListenerItem(listenerItem, locationId, data, dateList, true, true);
                                                   },
                                                   onFailed: function(errorId, code, message) {
                                                       m_closure._postWifiVisitDataFailedToListenerItem(listenerItem, locationId, errorId, code, message);
                                                   }
                                               });
    },
    requestSensorGranularityData: function (listenerItem, granularityType, locationId, startDate, finishDate) {
        var m_closure = this;

        var startDateLiteral = this.m_pfmController.getDateQueryString(startDate);
        var finishDateLiteral = this.m_pfmController.getDateQueryString(finishDate);

        var result = this.m_memoryManager.requestSensorGranularityData(granularityType, locationId, startDateLiteral, finishDateLiteral);
        if (result.status === this.m_pfmController.COMPLETED) {
            console.log(TAG, 'Sensor Granularity Data found in memory.');
            m_closure._postSensorGranularityDataToListenerItem(listenerItem, granularityType, locationId, result.data, startDate, finishDate, granularityType);
            return;
        }

        if (this.m_dbDataCache) {
            result = this.m_sqlManager.requestSensorGranularityData(granularityType, locationId, startDateLiteral, finishDateLiteral);
            if (result.status === this.m_pfmController.COMPLETED) {
                console.log(TAG, 'Sensor Granularity Data found in db.');
                m_closure._postSensorGranularityDataToListenerItem(listenerItem, granularityType, locationId, result.data, startDate, finishDate, granularityType, true);
                return;
            }
        }

        console.log(TAG, 'Sensor Granularity Data requeting via API.');
        this.m_apiManager.requestSensorGranularityData({
                                                           data: result.data,
                                                           locationId: locationId,
                                                           startDate: startDate,
                                                           finishDate: finishDate,
                                                           granularityType: granularityType,
                                                           onSuccess: function(data) {
                                                               m_closure._postSensorGranularityDataToListenerItem(listenerItem, granularityType, locationId, data, startDate, finishDate, true, true);
                                                           },
                                                           onFailed: function(errorId, code, message) {
                                                               m_closure._postSensorGranularityDataFailedToListenerItem(listenerItem, granularityType, locationId, errorId, code, message);
                                                           }
                                                       });
    },

    requestWifiGranularityData: function (listenerItem, granularityType, locationId, startDate, finishDate) {
        var m_closure = this;

        var startDateLiteral = this.m_pfmController.getDateQueryString(startDate);
        var finishDateLiteral = this.m_pfmController.getDateQueryString(finishDate);

        console.log(TAG, 'Requesting granularity data:', granularityType, startDateLiteral, finishDateLiteral);

        var result = this.m_memoryManager.requestWifiGranularityData(granularityType, locationId, startDateLiteral, finishDateLiteral);
        if (result.status === this.m_pfmController.COMPLETED) {
            console.log(TAG, 'Wifi Granularity Data found in memory.');
            m_closure._postWifiGranularityDataToListenerItem(listenerItem, granularityType, locationId, result.data, startDate, finishDate);
            return;
        }

        if (this.m_dbDataCache) {
            result = this.m_sqlManager.requestWifiGranularityData(granularityType, locationId, startDateLiteral, finishDateLiteral);
            if (result.status === this.m_pfmController.COMPLETED) {
                console.log(TAG, 'Wifi Granularity Data found in db.');
                m_closure._postWifiGranularityDataToListenerItem(listenerItem, granularityType, locationId, result.data, startDate, finishDate, true);
                return;
            }
        }

        console.log(TAG, 'Wifi Granularity Data requeting via API.');
        this.m_apiManager.requestWifiGranularityData({
                                                         data: result.data,
                                                         locationId: locationId,
                                                         startDate: startDate,
                                                         finishDate: finishDate,
                                                         granularityType: granularityType,
                                                         onSuccess: function(data) {
                                                             m_closure._postWifiGranularityDataToListenerItem(listenerItem, granularityType, locationId, data, startDate, finishDate, true, true);
                                                         },
                                                         onFailed: function(errorId, code, message) {
                                                             m_closure._postWifiGranularityDataFailedToListenerItem(listenerItem, granularityType, locationId, errorId, code, message);
                                                         }
                                                     });
    },

    recacheWifi: function(locationId, data) {
        this.m_memoryManager.saveWifiVisitData(locationId, data);
        if (this.m_dbDataCache)
            this.m_sqlManager.saveWifiVisitData(locationId, data);
    },

    recacheSensor: function(locationId, data) {
        this.m_memoryManager.saveSensorVisitData(locationId, data);
        if (this.m_dbDataCache)
            this.m_sqlManager.saveSensorVisitData(locationId, data);
    },

    revokeWifiGranularity: function(locationId) {
        if (this.m_dbDataCache)
            this.m_sqlManager.revokeWifiGranularity();
    },

    revokeSensorGranularity: function(locationId) {
        if (this.m_dbDataCache)
            this.m_sqlManager.revokeSensorGranularity();
    },

    _postFailedSyncListToListenerItem: function(listenerItem, errorId, code, message) {
        if (listenerItem.hasOwnProperty('responseSyncListDataFailed'))
            listenerItem.responseSyncListDataFailed(errorId, code, message);
        else
            console.log(TAG, 'Item listener does not have a signal named responseSyncListDataFailed.');
    },

    _postSyncListToListenerItem: function(listenerItem, data, lastRequestDate) {
        if (listenerItem.hasOwnProperty('responseSyncListDataSuccess'))
            listenerItem.responseSyncListDataSuccess(data);
        else
            console.log(TAG, 'Item listener does not have a signal named responseSyncListDataSuccess.');
    },

    _postSensorVisitDataFailedToListenerItem: function(listenerItem, locationId, errorId, code, message) {
        if (listenerItem.hasOwnProperty('responseSensorVisitDataFailed'))
            listenerItem.responseSensorVisitDataFailed(locationId, errorId, code, message);
        else
            console.log(TAG, 'Item listener does not have a signal named responseSensorVisitDataFailed.');
    },

    _postSensorVisitDataToListenerItem: function(listenerItem, locationId, data, dateList, needCache, needSave) {
        this.fillBlanks(data, dateList, {
                            total_count: 0
                        });

        if (needCache)
            this.m_memoryManager.saveSensorVisitData(locationId, data);

        if (needSave && this.m_dbDataCache)
            this.m_sqlManager.saveSensorVisitData(locationId, data);

        if (listenerItem.hasOwnProperty('responseSensorVisitDataSuccess'))
            listenerItem.responseSensorVisitDataSuccess(locationId, data);
        else
            console.log(TAG, 'Item listener does not have a signal named responseSensorVisitDataSuccess.');
    },

    _postSensorGranularityDataFailedToListenerItem: function(listenerItem, granularityType, locationId, errorId, code, message) {
        if (listenerItem.hasOwnProperty('responseSensorGranularityDataFailed'))
            listenerItem.responseSensorGranularityDataFailed(granularityType, locationId, errorId, code, message);
        else
            console.log(TAG, 'Item listener does not have a signal named responseSensorGranularityDataFailed.');
    },

    _postSensorGranularityDataToListenerItem: function(listenerItem, granularityType, locationId, data, startDate, finishDate, needCache, needSave) {
        if (!data) {
            data = {
                total_count: 0,
                start_date: this.m_pfmController.getDateQueryString(startDate),
                finish_date: this.m_pfmController.getDateQueryString(finishDate, true)
            };
        }

        if (needCache)
            this.m_memoryManager.saveSensorGranularityData(granularityType, locationId, data);

        if (needSave && this.m_dbDataCache)
            this.m_sqlManager.saveSensorGranularityData(granularityType, locationId, data);

        if (listenerItem.hasOwnProperty('responseSensorGranularityDataSuccess'))
            listenerItem.responseSensorGranularityDataSuccess(granularityType, locationId, data);
        else
            console.log(TAG, 'Item listener does not have a signal named responseSensorGranularityDataSuccess.');
    },

    _postWifiGranularityDataFailedToListenerItem: function(listenerItem, granularityType, locationId, errorId, code, message) {
        if (listenerItem.hasOwnProperty('responseWifiGranularityDataFailed'))
            listenerItem.responseWifiGranularityDataFailed(granularityType, locationId, errorId, code, message);
        else
            console.log(TAG, 'Item listener does not have a signal named responseWifiGranularityDataFailed.');
    },

    _postWifiGranularityDataToListenerItem: function(listenerItem, granularityType, locationId, data, startDate, finishDate, needCache, needSave) {
        if (!data) {
            data = {
                capture_rate:   0,
                unique_percent: 0,
                new_percent:    0,
                dwell_time:     0,
                frequency:      0,
                start_date:     this.m_pfmController.getDateQueryString(startDate),
                finish_date:    this.m_pfmController.getDateQueryString(finishDate, true)
            };
        }

        if (needCache)
            this.m_memoryManager.saveWifiGranularityData(granularityType, locationId, data);

        if (needSave && this.m_dbDataCache)
            this.m_sqlManager.saveWifiGranularityData(granularityType, locationId, data);

        if (listenerItem.hasOwnProperty('responseWifiGranularityDataSuccess'))
            listenerItem.responseWifiGranularityDataSuccess(granularityType, locationId, data);
        else
            console.log(TAG, 'Item listener does not have a signal named responseWifiGranularityDataSuccess.');
    },

    _postWifiVisitDataFailedToListenerItem: function(listenerItem, granularityType, locationId, errorId, code, message) {
        if (listenerItem.hasOwnProperty('responseWifiVisitDataFailed'))
            listenerItem.responseWifiVisitDataFailed(granularityType, errorId, locationId, code, message);
        else
            console.log(TAG, 'Item listener does not have a signal named responseWifiVisitDataFailed,');
    },

    _postWifiDataToListenerItem: function(listenerItem, locationId, data, dateList, needCache, needSave) {
        this.fillBlanks(data, dateList, {
                            capture_rate:    0,
                            unique_percent:  0,
                            new_percent:     0,
                            dwell_time:      0,
                            frequency:       0
                        });

        if (needCache)
            this.m_memoryManager.saveWifiVisitData(locationId, data);

        if (needSave && this.m_dbDataCache)
            this.m_sqlManager.saveWifiVisitData(locationId, data);

        if (listenerItem.hasOwnProperty('responseWifiVisitDataSuccess'))
            listenerItem.responseWifiVisitDataSuccess(locationId, data);
        else
            console.log(TAG, 'Item listener does not have a signal named responseWifiVisitDataSuccess.');
    },

    _postFailedLoginInfoToListenerItem: function(listenerItem, errorId, code, message) {
        if (listenerItem.hasOwnProperty('responseLoginInfoFailed'))
            listenerItem.responseLoginInfoFailed(errorId, code, message);
        else
            console.log(TAG, 'Item listener does not have a signal named responseLoginInfoFailed.');
    },

    _postLoginInfoToListenerItem: function(listenerItem, data, remember, username, password) {
        data = {
            user_id: data.UserId,
            //token: this.m_pfmController.m_cryptoUtil.encrypt(data.Token),
            token: data.Token,
            server_time: data.ServerTime
        };

        console.log("saving logged in user user id : ", data.user_id)
        console.log("saving logged in user with token : ", data.token)
        this.m_memoryManager.saveLoggedInUser(data);
        if (this.m_useDatabase) {
            var existingUser = this.m_sqlManager.getSavedUser();
            if (existingUser && existingUser.username !== username)
                this.clearData();

            if (remember)
                this.m_sqlManager.saveUser(username, password);
            else
                this.m_sqlManager.deleteUser();
        }

        if (listenerItem.hasOwnProperty('responseLoginInfoSuccess'))
            listenerItem.responseLoginInfoSuccess(data);
        else
            console.log(TAG, 'Item listener does not have a signal named responseLoginInfoSuccess.');
    },

    _postLoggedOutToListenerItem: function(listenerItem) {
        if (listenerItem.hasOwnProperty('responseLoggedOut'))
            listenerItem.responseLoggedOut();
        else
            console.log(TAG, 'Item listener does not have a signal named responseLoggedOut.');
    },

    _postFailedLocationDataToListenerItem: function(listenerItem, errorId, code, message) {
        console.log(TAG, 'Location retrieval failed:', errorId, code, message);

        if (listenerItem.hasOwnProperty('responseLocationDataFailed'))
            listenerItem.responseLocationDataFailed(errorId, code, message);
        else
            console.log(TAG, 'Item listener does not have a signal named responseLocationDataFailed.');
    },

    _postLocationDataToListenerItem: function(listenerItem, data, needCache, needSave) {
        console.log(TAG, 'Location retrieval successfull.');
        if (needCache)
            this.m_memoryManager.saveLocationData(data);

        if (needSave && this.m_dbDataCache)
            this.m_sqlManager.saveLocationData(data);

        if (listenerItem.hasOwnProperty('responseLocationDataSuccess'))
            listenerItem.responseLocationDataSuccess(data);
        else
            console.log(TAG, 'Item listener does not have a signal named responseLocationDataSuccess.');
    }
};

