import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0
import com.triodor.pfm 1.0
import "PfmController.js" as PfmController

PfmAuthPage {
    property string pageTAG: '[Login]'

    id: root

    shadowTop.visible: shadowVisible
    shadowBottom.visible: shadowVisible

    onResponseLoginInfoFailed: d.handleLoginFailed(errorId, code, message)
    onResponseLoginInfoSuccess: d.handleLoginSucess(loginInfo)

    onBeforeOpen: d.init()

    QtObject {
        id: d

        property bool inProgress: false
        property int selectedApiIndex: 0

        function handleLoginSucess(loginInfo) {
            globalAnalytics.setCustomDimension(globalAnalytics.userIdDimensionIndex, '#' + loginInfo.user_id + ' - ' + loginInfo.username);
            globalAnalytics.sendScreenView("Login");

            var date = new Date();
            // http://ben.lobaugh.net/blog/749/converting-datetime-ticks-to-a-unix-timestamp-and-back-in-php
            date.setTime((loginInfo.server_time - 621355968000000000) / 10000);
            serverModel.serverFullDate = date;

            var queryDate = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
            serverModel.serverDate = queryDate;

            console.log(pageTAG, 'Setted new reference date.', date);

            inProgress = false;
            navigation.replace('home');
        }

        function handleLoginFailed(errorId, code, message) {
            console.log(errorId)
            console.log(code)
            console.log(message)
            if (errorId === PfmController.ERROR_INVALID_SERVER_RESPONSE) {
                console.log(pageTAG, 'Invalid server response with error:', message);
                failedDialog.serverError = true;
            } else {
                failedDialog.serverError = false;
            }

            inProgress = false;
            if (failedDialog.supports()){}
                failedDialog.simpleAlert();
        }

        function login() {
            Qt.inputMethod.hide();

            var username = Lang.trim(emailInput.text);
            var password = Lang.trim(passwordInput.text);

            if (username.length === 0 || password.length === 0) {
                if (invalidInputDialog.supports())
                    invalidInputDialog.simpleAlert();

                return;
            }

            inProgress = true;
            PfmController.setSharedData('rebuildModel', true);
            PfmController.login(root, emailInput.text, passwordInput.text, rememberSwitch.checked ? true : false);
        }

        function init() {
            d.selectedApiIndex = PfmController.getMockMode() ? PfmController.m_apiIndexes.length : PfmController.m_selectedApiIndex;

            var user = PfmController.getSavedUser();
            if (user) {
                emailInput.text = user.username;
                passwordInput.text = user.password;
                rememberSwitch.checked = true;
            } else {
                emailInput.text = '';
                passwordInput.text = '';
                rememberSwitch.checked = false;
            }
        }

        function openDevDialog() {
            devDialog.show();
        }

        function setApiIndex(index, noDelay) {
            if (index === PfmController.m_apiIndexes.length) {
                PfmController.setMockMode(true, noDelay);
            } else {
                PfmController.setMockMode(false, noDelay);
                PfmController.setApiIndex(index);
            }

            d.selectedApiIndex = index;
            devDialog.hide();
        }

        function clearCacheData() {
            PfmController.clearData();
            if (cacheCleared.supports())
                cacheCleared.simpleAlert();

            devDialog.hide();
        }
    }

    Image {
        anchors.fill: parent
        source: imagePath('splash.png')
        fillMode: Image.PreserveAspectCrop
        verticalAlignment: Image.AlignVCenter
    }

    Flickable {
        anchors.fill: parent
        contentWidth: width
        contentHeight: formColumn.height + formColumn.y
        interactive: contentHeight > height

        Image {
            id: loginLogo

            source: imagePath('login-logo.png')
            width: dp(192)
            height: dp(100)

            x: (parent.width - width) / 2
            y: dp(50)

            smooth: parent.flicking ? false : true
        }

        Column {
            id: formColumn

            width: parent.width
            visible: !d.inProgress

            y: Math.max(loginLogo.height + loginLogo.y + dp(50), (root.height - height) / 2)

            Rectangle {
                id: inputContainer

                width: dp(278)
                height: dp(106)
                color: '#fff'
                radius: dp(5)

                anchors.horizontalCenter: parent.horizontalCenter

                Column {
                    width: parent.width
                    RowLayout {
                        width: parent.width
                        height: dp(53)

                        Item {
                            implicitWidth: dp(100)
                            height: parent.height

                            PfmText {
                                text: 'User name'
                                anchors.left: parent.left
                                anchors.leftMargin: dp(25)
                                anchors.verticalCenter: parent.verticalCenter
                            }

                            MouseArea {
                                anchors.fill: parent
                                onClicked: emailInput.forceActiveFocus()
                            }
                        }

                        PfmLoginTextField {
                            id: emailInput
                            text: ''
                            implicitWidth: dp(165)
                        }
                    }

                    Rectangle {
                        width: parent.width
                        height: dp(1)
                        color: '#cdcdcd'
                    }

                    RowLayout {
                        width: parent.width
                        height: dp(53)

                        Item {
                            implicitWidth: dp(100)
                            height: parent.height

                            PfmText {
                                text: 'Password'
                                anchors.left: parent.left
                                anchors.leftMargin: dp(25)
                                anchors.verticalCenter: parent.verticalCenter
                            }

                            MouseArea {
                                anchors.fill: parent
                                onClicked: passwordInput.forceActiveFocus()
                            }
                        }

                        PfmLoginTextField {
                            id: passwordInput
                            text: ''
                            implicitWidth: dp(165)
                            echoMode: TextInput.Password
                        }
                    }
                }
            }

            Item {
                width: inputContainer.width
                height: dp(76)
                anchors.horizontalCenter: parent.horizontalCenter

                Item {
                    width: parent.width
                    height: dp(27)

                    anchors.verticalCenter: parent.verticalCenter

                    Row {
                        height: parent.height
                        spacing: dp(15)

                        TouchSwitch {
                            id: rememberSwitch
                        }

                        PfmText {
                            text: 'Remember me'
                            color: '#fff'
                        }
                    }
                }
            }

            TouchButton {
                width: inputContainer.width
                height: dp(52)

                anchors.horizontalCenter: parent.horizontalCenter

                label.text: 'Login'
                label.color: '#fff'
                label.font.pixelSize: dp(20)

                mouse.onClicked: d.login()

                Rectangle {
                    color: parent.mouse.pressed ? '#283039' : '#323d48'
                    anchors.fill: parent
                    radius: dp(3)
                    z: -1
                }
            }
        }
    }

    Item {
        id: devDialog

        function show() {
            visible = true;
        }

        function hide() {
            visible = false;
        }

        anchors.fill: parent
        visible: false
        Rectangle {
            anchors.fill: parent
            color: '#000'
            opacity: 0.7
            MouseArea {
                anchors.fill: parent
                onClicked: devDialog.hide()
            }
        }

        Column {
            width: dp(260)
            anchors.centerIn: parent
            Rectangle {
                width: parent.width
                height: dp(44)
                color: '#009e3a'
                PfmText {
                    anchors.centerIn: parent
                    font.bold: true
                    font.pixelSize: dp(17)
                    text: 'Choose API Server'
                    color: '#fff'
                }
            }

            Repeater {
                model: PfmController.m_apiIndexes.length
                delegate: Rectangle {
                    width: parent.width
                    height: dp(36)
                    color: '#FFF'

                    Rectangle {
                        width: dp(8)
                        height: dp(8)
                        radius: width / 2
                        color: '#CCC'
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: dp(7)
                        visible: d.selectedApiIndex === index
                    }

                    PfmText {
                        font.pixelSize: dp(15)
                        text: PfmController.m_apiIndexes[index].name
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: dp(22)
                    }

                    TouchInteractionButton {
                        id: apiTouch
                        circlar: false
                        width: parent.width
                        height: parent.height
                    }

                    MouseArea {
                        anchors.fill: parent
                        preventStealing: true
                        onClicked: d.setApiIndex(index)
                        onPressedChanged: pressed ? apiTouch.press() : apiTouch.release()
                    }
                }
            }

            Rectangle {
                width: parent.width
                height: dp(36)
                color: '#FFF'

                Rectangle {
                    width: dp(8)
                    height: dp(8)
                    radius: width / 2
                    color: '#CCC'
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: dp(7)
                    visible: d.selectedApiIndex === PfmController.m_apiIndexes.length
                }

                PfmText {
                    font.pixelSize: dp(15)
                    text: 'Simulated API (Randomized)'
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: dp(22)
                }

                TouchInteractionButton {
                    id: mockTouch
                    circlar: false
                    width: parent.width
                    height: parent.height
                }

                MouseArea {
                    anchors.fill: parent
                    preventStealing: true
                    onClicked: d.setApiIndex(PfmController.m_apiIndexes.length)
                    onPressedChanged: pressed ? mockTouch.press() : mockTouch.release()
                }
            }

            Rectangle {
                width: parent.width
                height: dp(36)
                color: '#FFF'

                Rectangle {
                    width: dp(8)
                    height: dp(8)
                    radius: width / 2
                    color: '#CCC'
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: dp(7)
                    visible: d.selectedApiIndex === PfmController.m_apiIndexes.length
                }

                PfmText {
                    font.pixelSize: dp(15)
                    text: 'Simulated API (No Delay)'
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: dp(22)
                }

                TouchInteractionButton {
                    id: mockTouch2
                    circlar: false
                    width: parent.width
                    height: parent.height
                }

                MouseArea {
                    anchors.fill: parent
                    preventStealing: true
                    onClicked: d.setApiIndex(PfmController.m_apiIndexes.length, true)
                    onPressedChanged: pressed ? mockTouch2.press() : mockTouch2.release()
                }
            }

            Rectangle {
                width: parent.width
                height: dp(44)
                color: '#009e3a'
                PfmText {
                    anchors.centerIn: parent
                    font.bold: true
                    font.pixelSize: dp(17)
                    text: 'Developer Options'
                    color: '#fff'
                }
            }

            Rectangle {
                width: parent.width
                height: dp(36)
                color: '#FFF'

                PfmText {
                    font.pixelSize: dp(15)
                    text: 'Clear Cache Data'
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: dp(22)
                }

                TouchInteractionButton {
                    id: clearCacheTouchInteraction
                    circlar: false
                    width: parent.width
                    height: parent.height
                }

                MouseArea {
                    anchors.fill: parent
                    preventStealing: true
                    onClicked: d.clearCacheData()
                    onPressedChanged: pressed ? clearCacheTouchInteraction.press() : clearCacheTouchInteraction.release()
                }
            }
        }
    }

    MouseArea {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        width: dp(32)
        height: dp(32)
        enabled: ApplicationInfo.devMode ? true : false
        onPressAndHold: d.openDevDialog()
    }

    PfmBusyIndicator {
        anchors.centerIn: parent
        visible: d.inProgress
    }

    NativeDialog {
        property bool serverError: false

        id: failedDialog
        title: qsTr('Login Failed')
        message: serverError ? qsTr('We cannot contact with our server at the moment. Please try again later.') : qsTr('Your credentials is wrong. Please try again.')
    }

    NativeDialog {
        id: invalidInputDialog

        title: qsTr('Invalid Entry')
        message: qsTr('Please enter both username and password.')
    }

    NativeDialog {
        id: cacheCleared

        title: qsTr('Cache Cleared')
        message: qsTr('All caches are flushed from database.')
    }
}

