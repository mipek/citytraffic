import QtQuick 2.5
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0
import Frame.Analytics 1.0
import com.triodor.pfm 1.0
import "PfmController.js" as PfmController

Scene {
    property alias serverModel: serverModel
    property alias syncLoader: syncLoader
    property string rangeButtonText: "Week"
    property string currentTitle
    signal responseLoggedOut()

    signal responseSyncListDataFailed(string errorId, int code, string message)
    signal responseSyncListDataSuccess(var data)

    onResponseLoggedOut: navigation.replace('login')
    property alias confirmDialog:confirmDialog
    property alias updateDialog:updateDialog


    id: scene

    width: parent.width
    height: parent.height

    navigation.initialPageId: 'login'
    navigation.pages: [
        NavigationPageInfo {
            pageId: 'home'
            pageType: NavigationInfo.Standard
            title: 'Overview'
            sourcePage: Home {}
        },

        NavigationPageInfo {
            pageId: 'login'
            pageType: NavigationInfo.Popup
            sourcePage: Login {}
        },

        NavigationPageInfo {
            pageId: 'chart'
            pageType: NavigationInfo.Standard
            title: 'Chart'
            sourcePage: Chart {}
        },

        NavigationPageInfo {
            pageId: 'range'
            pageType: NavigationInfo.Popup
            title: 'Range'
            sourceComponent: Range {}
        }
    ]
    navigation.onCurrentPageChanged: d.handleCurrentPageChanged()

    topLoader.sourceComponent: NavigationBar {
        rangeButtonText : scene.rangeButtonText
        label.text: currentTitle
        mainPageOnline: navigation.depth <= 1
        chartPageOnline: d.chartPageOnline

        onBackButtonClicked: navigation.close()
        onLogoutButtonClicked: confirmDialog.simpleConfirm()
        onRangeButtonClicked: navigation.open('range')
    }
    topLoader.height: dp(44)

    QtObject {
        id: d

        property int appState: 0
        property bool chartPageOnline: false

        onAppStateChanged: applyState(appState)

        function applyState(state) {
            switch (state) {
            case Qt.ApplicationActive:
                console.log('[System]', 'Application is active now. Checking sync status.');
                syncIfNecessary();
                break;
            }
        }

        function init() {
            globalAnalytics.init();
            globalAnalytics.setConfig({
                                          ga_trackingId: 'UA-72311914-1',
                                          ga_appVersion: ApplicationInfo.version,
                                          ga_appName: 'Appvantage',
                                          ga_dispatchPeriod: ApplicationInfo.devMode ? 3 : 300
                                      });

            // globalAnalytics.sendEvent("HelloWorld", "Hey!");
            PfmController.setVersion(ApplicationInfo.version);

            PfmController.maintenanceCheck();
            PfmController.bootstrap();
        }

        function handleApplicationStateChange(state) {
            appState = state;
        }

        function handleCurrentPageChanged() {
            if (navigation.currentPage) {
                currentTitle = Qt.binding(function() { return navigation.currentPage ? navigation.currentPage.pageInfo.title : ''; });
                chartPageOnline = navigation.currentPage.pageInfo.pageId === 'chart';

                console.log('Chart Page Online:', chartPageOnline);
            }
        }

        function syncIfNecessary() {

            //TODO: this is temporarily disabled because API sends all the data. Not just after current date and time that we send.
            PfmController.startSyncProgress(function() {
                return scene.navigation.inProgressPage ? scene.navigation.inProgressPage : scene.navigation.currentPage;
            });
        }
    }

    Analytics {
        property int userIdDimensionIndex: 1

        id: globalAnalytics
    }

    Item {
        id: serverModel

        property var serverDate: new Date()
        property var serverFullDate: new Date()
    }

    Connections {
        target: Qt.application
        onStateChanged: d.handleApplicationStateChange(Qt.application.state)
    }

    SyncLoader {
        id: syncLoader
    }

    NativeDialog {
        id: updateDialog

        title: qsTr("Update Required")
        message: qsTr("New version of the application is available, please update.")

        onConfirmResult:{
            if(accepted){
                if(Qt.platform.os === "android"){
                    Qt.openUrlExternally("https://play.google.com/store/apps/details?id=com.pfm.appvantage.city.traffic");
                }
                else if(Qt.platform.os === "ios"){
                    Qt.openUrlExternally("https://appsto.re/tr/-kM6_.i");
                }
            }
        }

    }

    NativeDialog {
        id: confirmDialog

        title: qsTr('Are you sure to logout?')
        message: qsTr('You are about to logout. Your saved credentials will be deleted.')



        onConfirmResult:{
            if (accepted) PfmController.logout(scene);
        }


    }

    /*
      with async loader this gives a lot of context errors.
    DatePicker {
        id: datePicker
        z: 9999
    }*/

    Timer{
        id: popTimer
        interval: 5000
        onTriggered: {
            popDialog.acceptMessage = true
        }
    }
    NativeDialog {
        id: popDialog
        title: ""
        message: ""
        property bool acceptMessage:true
        Component.onCompleted: {
            PfmController.landPageAlias = scene
        }
        function pop(title,message){
            if(acceptMessage){
                acceptMessage = false
                popDialog.title = title
                popDialog.message = message
                popDialog.simpleAlert();
                popTimer.restart()
            }
        }
    }


    function popMessage(title,message){
        if (popDialog.supports()){
            popDialog.pop(title,message)
        }
    }

    Component.onCompleted: d.init()
}

