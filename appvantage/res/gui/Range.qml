import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0 as FrameCore
import com.triodor.pfm 1.0
import "PfmController.js" as PfmController
import "PfmQueryBarPrivate.js" as PfmQueryBarPrivate

PfmPage {
    property string pageTAG: '[Range]'
    property alias dd: d

    id: root

    shadowTop.visible: shadowVisible
    shadowBottom.visible: shadowVisible

    onBeforeOpen: d.init()
    onAfterClose: d.saveResult();

    onSyncStarted: d.handleSyncStarted()
    onSyncCompleted: d.handleSyncCompleted()
    onSyncError: d.handleSyncError(errorId, code, message)

    QtObject {
        id: d

        property alias tabView: tabView
        property alias localNav: localNav
        property bool unitTestMode: false
        property bool valueSetInProgress: false

        property var startDateRef: new Date(); // serverDate
        property var finishDateRef: null

        property var previousWeek: PfmQueryBarPrivate.getPreviousDateOffset(startDateRef, finishDateRef, 0, false, PfmQueryBarPrivate.TYPE_WEEKLY)
        property var previousMonth: PfmQueryBarPrivate.getPreviousDateOffset(startDateRef, finishDateRef, 0, false, PfmQueryBarPrivate.TYPE_MONTHLY)
        property var previousQuarter: PfmQueryBarPrivate.getPreviousDateOffset(startDateRef, finishDateRef, 0, false, PfmQueryBarPrivate.TYPE_QUARTERLY)

        property var fromDate: PfmQueryBarPrivate.getPreviousDateOffset(startDateRef, finishDateRef, 0, true, PfmQueryBarPrivate.TYPE_WEEKLY)
        property var fromDateMonth: PfmQueryBarPrivate.getPreviousDateOffset(startDateRef, finishDateRef, 0, true, PfmQueryBarPrivate.TYPE_MONTHLY)
        property var fromDateQuarter: PfmQueryBarPrivate.getPreviousDateOffset(startDateRef, finishDateRef, 0, true, PfmQueryBarPrivate.TYPE_QUARTERLY)

        property var startDateCustom : new Date()
        property var finishDateCustom : new Date()

        property ListModel weekListModel: ListModel {
            property bool yearBack: true
        }

        property ListModel monthListModel: ListModel {
            property bool yearBack: true
        }
        property ListModel quarterListModel: ListModel {
            property bool yearBack: true
        }
        property ListModel customListModel: ListModel {
            property bool yearBack: true

            ListElement {
                title: 'Previous Period'
                date: "N/A"
            }

            ListElement{
                title: 'Previous Year Period'
                date: "N/A"
            }
        }

        function init() {
            switch (scene.rangeButtonText) {
                case "Month":
                    tabView.currentIndex = 1;
                    break;
                case "Quarter":
                    tabView.currentIndex = 2;
                    break;
                case "Custom":
                    tabView.currentIndex = 3;
                    break;
                case "Week":
                default:
                    tabView.currentIndex = 0;
                    break;
            }

            weekListModel.append({"title": 'Previous Week', "date": d.getQueryText(d.previousWeek, 0, PfmQueryBarPrivate.TYPE_WEEKLY)});
            weekListModel.append({"title": 'Previous Year Week', "date": d.getQueryText(d.fromDate, 0, PfmQueryBarPrivate.TYPE_WEEKLY)});

            monthListModel.append({"title": 'Previous Month', "date": d.getQueryText(d.previousMonth, 0, PfmQueryBarPrivate.TYPE_MONTHLY)});
            monthListModel.append({"title": 'Previous Year Month', "date": d.getQueryText(d.fromDateMonth, 0, PfmQueryBarPrivate.TYPE_MONTHLY )});

            quarterListModel.append({"title": 'Previous Quarter', "date": d.getQueryText(d.previousQuarter, 0, PfmQueryBarPrivate.TYPE_QUARTERLY)});
            quarterListModel.append({"title": 'Previous Year Quarter', "date": d.getQueryText(d.fromDateQuarter, 0, PfmQueryBarPrivate.TYPE_QUARTERLY)});
        }

        function getQueryText(date, offset, type) {
            var bounds = PfmQueryBarPrivate.getBounds(date, offset, type);
            return PfmQueryBarPrivate.getQueryText(bounds, offset, type);
        }

        function checkResult() {
            var hasError = false;

            // checks only for custom
            if (tabView.currentIndex === 3) {
                if (!d.startDateCustom) {
                    invalidDateDialog.currentMessage = 'missing_start_date';
                    hasError = true;
                } else if (!d.finishDateCustom) {
                    invalidDateDialog.currentMessage = 'missing_finish_date';
                    hasError = true;
                } else if (d.startDateCustom.getTime() > d.finishDateCustom.getTime()) {
                    invalidDateDialog.currentMessage = 'start_date_bigger_than_finish_date';
                    hasError = true;
                } else {
                    var distance = (d.finishDateCustom.getTime() - d.startDateCustom.getTime());
                    if ((distance / (PfmQueryBarPrivate.A_DAY * 366)) > 1) {
                        invalidDateDialog.currentMessage = 'enter_maximum_a_year';
                        hasError = true;
                    } else if (distance < (PfmQueryBarPrivate.A_DAY * 3)) {
                        invalidDateDialog.currentMessage = 'enter_minimum_3_day';
                        hasError = true;
                    }
                }
            }

            if (hasError) {
                if (invalidDateDialog.supports() && !unitTestMode)
                    invalidDateDialog.simpleAlert();
            }

            return !hasError;
        }

        function saveResult() {
            if (!d.valueSetInProgress)
                return;

            d.valueSetInProgress = false;

            var queryType;
            var yearBack = false;

            switch (tabView.currentIndex) {
            case 1:
                queryType = PfmQueryBarPrivate.TYPE_MONTHLY;
                yearBack = d.monthListModel.yearBack;
                break;
            case 2:
                queryType = PfmQueryBarPrivate.TYPE_QUARTERLY;
                yearBack = d.quarterListModel.yearBack;
                break;
            case 3:
                queryType = PfmQueryBarPrivate.TYPE_CUSTOM;
                yearBack = d.customListModel.yearBack;
                break;
            case 0:
            default:
                queryType = PfmQueryBarPrivate.TYPE_WEEKLY;
                yearBack = d.weekListModel.yearBack;
                break;
            }

            if (!d.checkResult())
                return false;

            root.opener.changeQueryType(queryType, d.startDateCustom, d.finishDateCustom, yearBack);
            return true;
        }

        function handleSyncStarted() {
            scene.syncLoader.show();
            Util.setTimeout(function() {
                d.update();
            }, 100);
        }

        function handleSyncCompleted() {
            scene.syncLoader.hide();
        }

        function handleSyncError(errorId, code, message) {
            scene.syncLoader.hide();
        }
    }

    PopupNavigationBar {
        id: localNav

        label.text: root.pageInfo ? root.pageInfo.title : ''

        onOkButtonClicked: {
            if (d.checkResult()) {
                d.valueSetInProgress = true;
                navigation.close();
            }
        }

        onCancelButtonClicked: navigation.close()
    }

    TabView {
        property int tabWidth: parent.width / 4

        id: tabView

        width: parent.width - dp(10)
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: localNav.bottom
        anchors.topMargin: dp(5)
        anchors.bottom: parent.bottom

        style: TabViewStyle {
            frameOverlap: dp(1)
            tabOverlap: dp(1)
            tab: Rectangle {
                implicitWidth: styleData.index === 3 ? (tabView.width - tabView.tabWidth * 3 + dp(3)) : tabView.tabWidth
                implicitHeight: dp(50)

                color: styleData.selected ? '#fff' : '#F8F8F8'
                border.color: '#E4E4E4'

                Text {
                    id: text
                    anchors.centerIn: parent
                    text: styleData.title
                    font.pixelSize: sp(16)
                    color: styleData.selected ? '#808080' : '#B5B5B5'
                }

                Rectangle {
                    width: parent.width - dp(2)
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: dp(1)
                    anchors.bottom: parent.bottom
                    visible: styleData.selected
                }
            }
            frame: Item {}
        }

        Tab {
            title: "Week"

            Item {
                width: parent.width

                Rectangle {
                    id: firstBlock

                    width: parent.width
                    implicitHeight: dp(70)
                    border.color: '#E4E4E4'
                    color: '#fff'

                    PfmRangeCheckRow {
                        filterView.label.text: 'This Week'
                        filterView.dateLabel.text: d.getQueryText(d.startDateRef, 0, PfmQueryBarPrivate.TYPE_WEEKLY)
                        checked: true
                    }
                }

                PfmRangeCompareBlock {
                    y: firstBlock.height
                    horizontalPadding: 0
                    verticalPadding: dp(10)
                    selectedIndex: d.weekListModel.yearBack ? 1 : 0

                    model: d.weekListModel

                    onSelectedIndexChanged: d.weekListModel.yearBack = selectedIndex > 0;
                }
            }
        }

        Tab {
            title: "Month"

            Item {
                width: parent.width

                Rectangle {
                    id: firstBlock

                    width: parent.width
                    implicitHeight: dp(70)
                    border.color: '#E4E4E4'
                    color: '#fff'

                    PfmRangeCheckRow {
                        filterView.label.text: 'This Month'
                        filterView.dateLabel.text: d.getQueryText(d.startDateRef, 0, PfmQueryBarPrivate.TYPE_MONTHLY)
                        checked: true
                    }
                }

                PfmRangeCompareBlock {
                    y: firstBlock.height
                    horizontalPadding: 0
                    verticalPadding: dp(10)
                    selectedIndex: d.monthListModel.yearBack ? 1 : 0

                    model: d.monthListModel

                    onSelectedIndexChanged: d.monthListModel.yearBack = selectedIndex > 0;
                }
            }
        }

        Tab {
            title: "Quarter"

            Item {
                width: parent.width

                Rectangle {
                    id: firstBlock

                    width: parent.width
                    implicitHeight: dp(70)
                    border.color: '#E4E4E4'
                    color: '#fff'

                    PfmRangeCheckRow {
                        filterView.label.text: 'This Quarter'
                        filterView.dateLabel.text: d.getQueryText(d.startDateRef, 0, PfmQueryBarPrivate.TYPE_QUARTERLY)
                        checked: true
                    }
                }

                PfmRangeCompareBlock {
                    y: firstBlock.height
                    horizontalPadding: 0
                    verticalPadding: dp(10)
                    selectedIndex: d.quarterListModel.yearBack ? 1 : 0

                    model: d.quarterListModel

                    onSelectedIndexChanged: d.quarterListModel.yearBack = selectedIndex > 0;
                }
            }
        }

        Tab {
            title: "Custom"

            Item {
                property alias startDate: startDate
                property alias finishDate: finishDate

                width: parent.width

                Rectangle {
                    id: firstBlock

                    width: parent.width
                    implicitHeight: dateColumns.height

                    border.color: '#E4E4E4'
                    color: '#fff'

                    Column {
                        id: dateColumns

                        width: parent.width

                        PfmRangeDatePickRow {
                            id: startDate
                            title.text: 'Start Date'
                            selectedDate: d.startDateCustom

                            onSelectedDateChanged: {
                                if(startDate.selectedDate) d.startDateCustom.setTime(startDate.selectedDate.getTime())

                                if (startDate.selectedDate && finishDate.selectedDate){
                                    var compareTo = []
                                    compareTo = PfmQueryBarPrivate.customDate(startDate.selectedDate, finishDate.selectedDate)

                                    d.customListModel.setProperty(0, "date", compareTo[0])
                                    d.customListModel.setProperty(1, "date", compareTo[1])
                                }
                            }
                        }

                        Rectangle {
                            width: parent.width - dp(30)
                            height: dp(1)
                            color: '#E4E4E4'
                            anchors.horizontalCenter: parent.horizontalCenter
                        }

                        PfmRangeDatePickRow {
                            id: finishDate
                            title.text: 'Finish Date'
                            selectedDate: d.finishDateCustom
                            onSelectedDateChanged: {
                                if(finishDate.selectedDate) d.finishDateCustom.setTime(finishDate.selectedDate.getTime())

                                if (startDate.selectedDate && finishDate.selectedDate){
                                    var compareTo = []
                                    compareTo = PfmQueryBarPrivate.customDate(startDate.selectedDate, finishDate.selectedDate)

                                    d.customListModel.setProperty(0, "date", compareTo[0])
                                    d.customListModel.setProperty(1, "date", compareTo[1])
                                }
                            }
                        }
                    }
                }

                PfmRangeCompareBlock {
                    y: firstBlock.height
                    horizontalPadding: 0
                    verticalPadding: dp(10)
                    selectedIndex: d.customListModel.yearBack ? 1 : 0

                    model: d.customListModel

                    onSelectedIndexChanged: d.customListModel.yearBack = selectedIndex > 0;
                }
            }
        }
    }

    NativeDialog {
        property var messages: {
            'start_date_bigger_than_finish_date': qsTr('Start date cannot be bigger than finish date.'),
            'enter_maximum_a_year': qsTr('Please enter a maximum 1 year range.'),
            'missing_start_date': qsTr('Start date missing.'),
            'missing_finish_date': qsTr('Finish date missing.'),
            'enter_minimum_3_day': qsTr('Please select at least 3 days range.'),
            'default': qsTr('Please correct the date range. It looks incorrect.')
        }

        property string currentMessage: 'default'

        id: invalidDateDialog
        title: qsTr('Invalid date range')
        message: messages[currentMessage]
    }
}

