import QtQuick 2.5
import QtGraphicalEffects 1.0

Item {
    property real horizontalPadding: dp(5)
    property real verticalPadding: dp(5)

    property alias bodyLoader: loader
    property alias bodyContainer: bodyContainer
    property Component body
    property alias container: container

    width: parent.width
    height: parent.height

    Rectangle {
        id: container

        width: parent.width - horizontalPadding * 2
        height: parent.height - verticalPadding * 2

        anchors.centerIn: parent
        color: '#fff'
        radius: dp(2)

        layer.enabled: true
        layer.effect: DropShadow {
            radius: 4
            samples: radius * 2
            source: container
            color: Qt.rgba(0, 0, 0, 0.1)
            transparentBorder: true
        }

        Item {
            id: bodyContainer
            anchors.fill: parent

            Loader {
                id: loader

                anchors.fill: parent

                sourceComponent: body
            }
        }
    }
}

