import QtQuick 2.5

Item {
    property string text: ''
    property bool altColor: false
    property alias textView: innerText

    id: root

    width: innerText.width + dp(10)
    height: innerText.height + dp(4)
    // color: altColor ? '#3a4b51' : '#1c2b30'
    // radius: dp(2)

    PfmText {
        id: innerText

        font.pixelSize: sp(16)
        font.weight: Font.Medium
        text: root.text
        color: '#1c2b30'
        anchors.centerIn: parent
    }
}
