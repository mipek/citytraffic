import QtQuick 2.0

PfmApiCallablePage {
    signal loginSuccess(var loginInfo);
    signal responseLoginInfoSuccess(var loginInfo);
    signal responseLoginInfoFailed(string errorId, int code, string message);
    signal responseLoggedOut();
}

