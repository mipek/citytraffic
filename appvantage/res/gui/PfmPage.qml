import QtQuick 2.5
import QtGraphicalEffects 1.0
import Frame.Mobile 1.0
import "PfmController.js" as PfmController

NavigationPage {
    property alias shadowLeft: shadowLeft
    property alias shadowTop: shadowTop
    property alias shadowBottom: shadowBottom
    property alias shadowRight: shadowRight
    property bool shadowVisible: Qt.platform.os !== 'android'

    signal syncStarted()
    signal syncCompleted()
    signal syncError(string errorId, int code, string message)

    signal responseSyncListDataFailed(string errorId, int code, string message)
    signal responseSyncListDataSuccess(var data)

    onBeforeClose: Qt.inputMethod.hide()

    id: root
    color: '#dde0e6'

    width: parent ? parent.width : dp(320)
    height: parent ? parent.height : dp(480)

    Image {
        id: shadowLeft

        width: dp(8)
        height: parent.height
        fillMode: Image.TileVertically
        source: imagePath('drop-shadow-left.png')
        anchors.right: parent.left
        visible: shadowVisible
    }

    Image {
        id: shadowRight

        width: dp(8)
        height: parent.height
        fillMode: Image.TileVertically
        source: imagePath('drop-shadow-right.png')
        anchors.left: parent.right
        visible: shadowVisible
    }

    Image {
        id: shadowTop

        width: parent.width
        height: dp(8)
        fillMode: Image.TileHorizontally
        source: imagePath('drop-shadow-top.png')
        anchors.bottom: parent.top
        visible: false
    }

    Image {
        id: shadowBottom

        width: parent.width
        height: dp(8)
        fillMode: Image.TileHorizontally
        source: imagePath('drop-shadow-bottom.png')
        anchors.top: parent.bottom
        visible: false
    }
}

