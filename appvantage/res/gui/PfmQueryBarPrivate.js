.pragma library
.import "PfmController.js" as PfmController
.import Frame.Mobile.Core 1.0 as FrameCore
.import Frame.Mobile 1.0 as Frame

var TYPE_WEEKLY = 'weekly';
var TYPE_MONTHLY = 'monthly';
var TYPE_QUARTERLY = 'quarterly';
var TYPE_CUSTOM = 'custom';

var m_validTypes = [TYPE_WEEKLY, TYPE_MONTHLY, TYPE_QUARTERLY, TYPE_CUSTOM];

var A_DAY = 86400000;

var m_rules = {
    weekly: {
        offsetText: function(offset) {
            var text = '';
            switch (offset) {
            case 0:
                text = 'This week';
                break;
            case 1:
                text = 'Last week';
                break;
            default:
                text = offset + ' weeks ago';
            }

            return text;
        },

        previousOffsetText: function(offset, yearBack) {
            var text = yearBack ? 'Previous year' : '';
            if (!yearBack) {
                switch (offset) {
                case 0:
                    text = 'Last week';
                    break;
                default:
                    text = (offset + 1) + ' weeks ago';
                }
            }

            return text;
        },

        bounds: function(date, offset, startDateRef, finishDateRef) {
            if (date === null)
                return null;

            var startDay = PfmController.getNormalizedDay(date);
            var endDayDate = new Date();

            endDayDate.setTime(date.getTime() - A_DAY);
            if (--startDay === 0)
                startDay = 7;

            var startDayDate = new Date();
            startDayDate.setTime(endDayDate.getTime() - ((startDay - 1) * A_DAY));

            if (offset === false || offset > 0)
                endDayDate.setTime(startDayDate.getTime() + 6 * A_DAY);

            return { start: startDayDate, finish: endDayDate };
        },

        endDateOf: function(startDate, startDateRef, finishDateRef) {
            var finishDate = new Date();
            finishDate.setTime(startDate.getTime() + A_DAY * 6);

            return finishDate;
        },

        dateOffset: function(startDate, finishDate, offset) {
            var startDay = startDate.getDay();
            if (startDay === 0)
                startDay = 7;

            if (offset === 0)
                return startDate;

            var newDate = new Date();
            newDate.setTime(startDate.getTime() - offset * 7 * A_DAY);

            var endDay = newDate.getDay();
            if (endDay === 0)
                endDay = 7;

            if (startDay !== endDay) {
                var diff = startDay - endDay;
                newDate.setTime(newDate.getTime() + diff * A_DAY);
            }

            return newDate;
        },

        lastYearOfDate: function(currentDate) {
            var date = new Date();
            date.setTime(currentDate.getTime());
            date.setFullYear(date.getFullYear() - 1);

            var currentWeek = Frame.Utility.getWeekNumber(currentDate);
            if (currentWeek === 53) {
                var tmp = new Date();
                tmp.setTime(date.getTime());
                tmp.setDate(31);
                tmp.setMonth(11);

                if (Frame.Utility.getWeekNumber(tmp) !== 53)
                    return null;
            }

            var lastYearWeek = Frame.Utility.getWeekNumber(date);
            if (lastYearWeek !== currentWeek)
                date.setTime(date.getTime() + A_DAY * 7 * (currentWeek - lastYearWeek));

            var currentDay = PfmController.getNormalizedDay(currentDate);
            var lastYearDay = PfmController.getNormalizedDay(date);
            if (lastYearDay !== currentDay)
                date.setTime(date.getTime() + A_DAY * (currentDay - lastYearDay));

            return date;
        },

        noDateTextNode: function() {
            return 'No 53th Week ';
        },

        statText: function() {
            return 'This Week';
        },

        statPreviousText: function(yearBack) {
            return yearBack ? 'Previous Year' : 'Previous';
        },

        availableTimeAxis: function(startDateRef, finishDateRef) {
            return [PfmController.TIME_AXIS_DAILY];
        },

        granularityType: function(startDateRef, finishDateRef, axisType) {
            return PfmController.GRANULARITY_WEEKLY;
        }
    },

    monthly: {
        offsetText: function(offset) {
            var text = '';
            switch (offset) {
            case 0:
                text = 'This month';
                break;
            case 1:
                text = 'Last month';
                break;
            default:
                text = offset + ' months ago';
            }

            return text;
        },

        previousOffsetText: function(offset, yearBack) {
            var text = yearBack ? 'Previous year' : '';
            if (!yearBack) {
                switch (offset) {
                case 0:
                    text = 'Last month';
                    break;
                default:
                    text = (offset + 1) + ' months ago';
                }
            }

            return text;
        },

        bounds: function(date, offset, startDateRef, finishDateRef) {
            if (date === null)
                return null;

            var startDayDate = new Date();
            startDayDate.setTime(date.getTime());
            startDayDate.setDate(1);

            var endDayDate = new Date();
            endDayDate.setTime(date.getTime());

            if (offset === false || offset > 0) {
                endDayDate = new Date(Date.UTC(date.getFullYear(), date.getMonth() + 1, 0));
            }

            return { start: startDayDate, finish: endDayDate };
        },

        endDateOf: function(startDate, startDateRef, finishDateRef) {
            return new Date(Date.UTC(startDate.getFullYear(), startDate.getMonth() + 1, 0));
        },

        dateOffset: function(startDateRef, finishDateRef, offset) {
            if (offset === 0)
                return startDateRef;

            var year = startDateRef.getFullYear(),
                    month = startDateRef.getMonth() - offset;

            // get top date of previous month
            var newDate = new Date(Date.UTC(year, month + 1, 0));

            // if top date of month is bigger then the date
            if (newDate.getDate() > startDateRef.getDate()) {
                // set the date to it
                newDate.setDate(startDateRef.getDate());
            }

            return newDate;
        },

        lastYearOfDate: function(currentDate) {
            // get top date of last year 's month
            var date = new Date(Date.UTC(currentDate.getFullYear() - 1, currentDate.getMonth() + 1, 0));

            // equalize the date
            if (date.getDate() > currentDate.getDate()) {
                date.setDate(currentDate.getDate());
            }

            return date;
        },

        noDateTextNode: function() {
            return ''; // this won't happen I suppose
        },

        statText: function() {
            return 'This Month';
        },

        statPreviousText: function(yearBack) {
            return yearBack ? 'Previous Year' : 'Previous';
        },

        availableTimeAxis: function(startDateRef, finishDateRef) {
            return [PfmController.TIME_AXIS_WEEKLY, PfmController.TIME_AXIS_DAILY];
        },

        granularityType: function(startDateRef, finishDateRef, axisType) {
            return PfmController.GRANULARITY_MONTHLY;
        }
    },
    quarterly: {
        offsetText: function(offset) {
            var text = '';
            switch (offset) {
            case 0:
                text = 'This quarter';
                break;
            case 1:
                text = 'Last quarter';
                break;
            default:
                text = offset + ' quarter ago';
            }

            return text;
        },

        previousOffsetText: function(offset, yearBack) {
            var text = yearBack ? 'Previous year' : '';
            if (!yearBack) {
                switch (offset) {
                case 0:
                    text = 'Last quarter';
                    break;
                default:
                    text = (offset + 1) + ' quarter ago';
                }
            }

            return text;
        },

        bounds: function(date, offset, startDateRef, finishDateRef) {
            //console.log("quarterly bounds || date>>>>", date, " offset>>>>", offset);
            var startDayDate = new Date();
            var endDayDate = new Date();
            startDayDate.setTime(date.getTime());

            var mounth = startDayDate.getMonth() + 1

           // console.log("quarterly bounds || mounth >>>>>",mounth)

            if(mounth < 4) {
                startDayDate = new Date(Date.UTC(startDayDate.getFullYear(), 0, 1));
                endDayDate = new Date(Date.UTC(startDayDate.getFullYear(), 2, 31));
              //  console.log("quarterly bounds || startDayDate >>>>>",startDayDate, "endDayDate >>>>>", endDayDate)

            } else if (mounth > 3 && mounth < 7){
                startDayDate = new Date(Date.UTC(startDayDate.getFullYear(), 3, 1));
                endDayDate = new Date(Date.UTC(startDayDate.getFullYear(), 5, 30));
                // console.log("quarterly bounds || startDayDate >>>>>",startDayDate, "endDayDate >>>>>", endDayDate)

            } else if (mounth > 6 && mounth < 10){
                startDayDate = new Date(Date.UTC(startDayDate.getFullYear(), 6, 1));
                endDayDate = new Date(Date.UTC(startDayDate.getFullYear(), 8, 30));
                //console.log("quarterly bounds || startDayDate >>>>>",startDayDate, "endDayDate >>>>>", endDayDate)

            } else if (mounth > 9){
                startDayDate = new Date(Date.UTC(startDayDate.getFullYear(), 9, 1));
                endDayDate = new Date(Date.UTC(startDayDate.getFullYear(), 11, 31));
             //   console.log("quarterly bounds || startDayDate >>>>>",startDayDate, "endDayDate >>>>>", endDayDate)

            }

            return { start: startDayDate, finish: endDayDate };
        },

        endDateOf: function(startDate, startDateRef, finishDateRef) {
            return new Date(Date.UTC(startDate.getFullYear(), startDate.getMonth() + 3, 0));
        },

        dateOffset: function(startDate, finishDate, offset) {
            if (offset === 0) return startDate;
           // console.log("previous || date>>>>", date, " offset>>>>", offset);
            var newDate = new Date();
            newDate.setTime(startDate.getTime());
            newDate.setMonth(startDate.getMonth() - (offset*3));
           // console.log("previous || newDate>>>>", newDate);
            return newDate;
        },

        lastYearOfDate: function(currentDate) {
            // get top date of last year 's month
            var date = new Date(Date.UTC(currentDate.getFullYear() - 1, currentDate.getMonth() + 1, 0));

            // equalize the date
            if (date.getDate() > currentDate.getDate()) {
                date.setDate(currentDate.getDate());
            }
        //    console.log("lastYearOfDate || date>>>>", date);
            return date;
        },

        noDateTextNode: function() {
            return ''; // this won't happen I suppose
        },

        statText: function() {
            return 'This Quarter';
        },

        statPreviousText: function(yearBack) {
            return yearBack ? 'Previous Year' : 'Previous';
        },

        availableTimeAxis: function(startDateRef, finishDateRef) {
            return [PfmController.TIME_AXIS_MONTHLY, PfmController.TIME_AXIS_WEEKLY, PfmController.TIME_AXIS_DAILY];
        },

        granularityType: function(startDateRef, finishDateRef, axisType) {
            return PfmController.GRANULARITY_QUARTERLY;
        }
    },
    custom: {
        offsetText: function(offset) {
            return 'Current';
        },

        previousOffsetText: function(offset, yearBack) {
            return yearBack ? 'Previous year' : 'Previous';
        },

        bounds: function(date, offset, startDateRef, finishDateRef) {
            var diff = finishDateRef.getTime() - startDateRef.getTime();

            var startDate = new Date();
            startDate.setTime(date.getTime());

            var finishDate = new Date();
            finishDate.setTime(date.getTime() + diff);

            return { start: startDate, finish: finishDate };
        },

        endDateOf: function(startDate, startDateRef, finishDateRef) {
            var date = new Date();
            date.setTime(startDate.getTime() + (finishDateRef.getTime() - startDateRef.getTime()));

            return date;
        },

        dateOffset: function(startDate, finishDate, offset) {
            if (offset === 0)
                return startDate;

            // calculate diff
            var diff = finishDate.getTime() - startDate.getTime() + A_DAY;

            // get top date of previous month
            var newDate = new Date();
            newDate.setTime(startDate.getTime() - diff * offset); // remove diff

            return newDate;
        },

        lastYearOfDate: function(currentDate) {
            var date = new Date();
            date.setTime(currentDate.getTime());
            date.setFullYear(date.getFullYear() - 1);

            return date;
        },

        noDateTextNode: function() {
            return ''; // this won't happen I suppose
        },

        statText: function() {
            return 'Current';
        },

        statPreviousText: function(yearBack) {
            return yearBack ? 'Previous Year' : 'Previous';
        },

        availableTimeAxis: function(startDateRef, finishDateRef) {
            var timeAxises = [];
            if (startDateRef && finishDateRef) {
                var day = (finishDateRef.getTime() - startDateRef.getTime()) / A_DAY;

                if (day >= 89)
                    timeAxises.push(PfmController.TIME_AXIS_MONTHLY);

                if (day >= 14)
                    timeAxises.push(PfmController.TIME_AXIS_WEEKLY);

                timeAxises.push(PfmController.TIME_AXIS_DAILY);
            }

            return timeAxises;
        },

        granularityType: function(startDateRef, finishDateRef, axisType) {
            var availableTimeAxisList = this.availableTimeAxis(startDateRef, finishDateRef);
            if (availableTimeAxisList.indexOf(PfmController.TIME_AXIS_MONTHLY) > -1)
                return PfmController.GRANULARITY_MONTHLY;

            if (availableTimeAxisList.indexOf(PfmController.TIME_AXIS_WEEKLY) > -1)
                return PfmController.GRANULARITY_WEEKLY;

            return PfmController.GRANULARITY_DAILY;
        }
    }
};

function getType(type) {
    if (!type || m_validTypes.indexOf(type) === -1)
        return TYPE_WEEKLY;

    return type;
}

function getLastYearOfDate(currentDate, type) {
    type = getType(type);
    return m_rules[type].lastYearOfDate(currentDate);
}

function getDateOffset(startDateRef, finishDateRef, offset, type) {
    type = getType(type);
    return m_rules[type].dateOffset(startDateRef, finishDateRef, offset);
}

function getPreviousDateOffset(startDateRef, finishDateRef, offset, yearBack, type) {
    type = getType(type);
    if (yearBack)
        return getLastYearOfDate(getDateOffset(startDateRef, finishDateRef, offset, type), type);

    return getDateOffset(startDateRef, finishDateRef, offset + 1, type);
}

function getBounds(date, offset, type, startDateRef, finishDateRef) {
    type = getType(type);
    return m_rules[type].bounds(date, offset, startDateRef, finishDateRef);
}

function getOffsetText(offset, type) {
    type = getType(type);
    return m_rules[type].offsetText(offset);
}

function getPreviousOffsetText(offset, type, yearBack) {
    type = getType(type);
    return m_rules[type].previousOffsetText(offset, yearBack);
}

function getQueryText(date, offset, type) {
    type = getType(type);
    if (date === null)
        return m_rules[type].noDateTextNode();

    var startDate = FrameCore.DateTime.date('d.m.Y', date.start);
    var finishDate = FrameCore.DateTime.date('d.m.Y', date.finish);

    if (startDate === finishDate) return startDate;

    return [startDate, finishDate].join(' to ');
}

function getStatText(type) {
    type = getType(type);
    return m_rules[type].statText();
}

function getStatPreviousText(type, yearBack) {
    type = getType(type);
    return m_rules[type].statPreviousText(yearBack);
}

function getGranularityType(startDateRef, finishDateRef, axisType, type) {
    type = getType(type);
    return m_rules[type].granularityType(startDateRef, finishDateRef, axisType);
}

function getAvailableAxisList(startDateRef, finishDateRef, type) {
    type = getType(type);
    return m_rules[type].availableTimeAxis(startDateRef, finishDateRef);
}


function canQueryNext(offset, type) {
    return offset > 0;
}

function canQueryPrevious(offset, type) {
    return true;
}

function getNextOffset(offset, type) {
    if (!canQueryNext(offset, type))
        return offset;

    return --offset;
}

function getPreviousOffset(offset, type) {
    if (!canQueryPrevious(offset, type))
        return;

    return ++offset;
}

function getEndDateOf(startDate, type, startDateRef, finishDateRef) {
    type = getType(type);
    return m_rules[type].endDateOf(startDate, startDateRef, finishDateRef);
}

function boundsCustom(startDateCustom, finishDateCustom, offset) {
 //   console.log("boundsCustom || startDateCustom >>>>>",startDateCustom, "finishDateCustom >>>>>", finishDateCustom, " offset >>>",offset)
    if (offset === 0){
        return { start: startDateCustom, finish: finishDateCustom }
    }

    var previousPeriodStart = startDateCustom
    var previousPeriodFinish = finishDateCustom

    var diff = previousPeriodFinish.getTime() - previousPeriodStart.getTime()

    if(offset > 1){
      //  console.log("boundsCustom || diff >>>>>",diff, "diff * offset >>>>>", diff * offset, " offset >>>",offset)

        var previousPeriodStart2  = new Date();
        var previousPeriodFinish2  = new Date();
        previousPeriodStart2.setTime(previousPeriodStart.getTime() - (diff * offset))
        previousPeriodFinish2.setTime(previousPeriodFinish.getTime() - (diff * offset))
        //console.log("boundsCustom || start offset>>>>>",previousPeriodStart2, "finish offset>>>>>", previousPeriodFinish2, " offset >>>",offset)

        return { start: previousPeriodStart2, finish: previousPeriodFinish2 };

    } else {
        var previousPeriodStart1  = new Date();
        previousPeriodStart1.setTime(previousPeriodStart.getTime() - diff)
        //console.log("boundsCustom || start >>>>>",previousPeriodStart1, "finish >>>>>", previousPeriodStart, " offset >>>",offset)

        return { start: previousPeriodStart1, finish: previousPeriodStart };
    }
}

function customDate(startDate, finishDate){
   // console.log("customDate || startDate >>>>>",startDate, "finishDate >>>>>", finishDate)

    var previousPeriodStart = startDate
    var previousPeriodFinish = finishDate
    var diff = previousPeriodFinish.getTime() - previousPeriodStart.getTime()

    var previousPeriodStart1  = new Date();
    previousPeriodStart1.setTime(previousPeriodStart.getTime())
    previousPeriodStart1.setTime(previousPeriodStart.getTime() - diff)
    var previousPeriod = FrameCore.DateTime.date('M d, Y', previousPeriodStart1) + " - " +
            FrameCore.DateTime.date('M d, Y', previousPeriodStart);

    var previousYear = FrameCore.DateTime.date('M d, Y', getLastYearOfDate(previousPeriodStart, TYPE_CUSTOM)) + " - " +
            FrameCore.DateTime.date('M d, Y', getLastYearOfDate(previousPeriodFinish, TYPE_CUSTOM));

    var compareTo = []
    compareTo.push(previousPeriod)
    compareTo.push(previousYear)
    return compareTo
}

