
QML_PATH = $$replace(PWD, $$PROJECT_HOME, "")

RESOURCE_FILES += \
    $$PWD/main.qml \
    $$PWD/PfmPage.qml

RESOURCE_FILES += \
    $$PWD/Login.qml \
    $$PWD/Home.qml

RESOURCE_FILES += \
    $$PWD/LandPage.qml

RESOURCE_FILES += \
    $$PWD/NavigationBar.qml

RESOURCE_FILES += \
    $$PWD/PfmLoginTextField.qml

RESOURCE_FILES += \
    $$PWD/PfmText.qml

RESOURCE_FILES += \
    $$PWD/PfmQueryBar.qml \
    $$PWD/PfmBlock.qml \
    $$PWD/PfmBlockDynamic.qml

RESOURCE_FILES += \
    $$PWD/Chart.qml

RESOURCE_FILES += \
    $$PWD/PfmBusyIndicator.qml

RESOURCE_FILES += \
    $$PWD/PfmController.js \
    $$PWD/PfmQueryablePage.qml

RESOURCE_FILES += \
    $$PWD/PfmApiCallablePage.qml \
    $$PWD/PfmAuthPage.qml

RESOURCE_FILES += \
    $$PWD/PfmDataManagerAPI.js \
    $$PWD/PfmDataManagerMemory.js \
    $$PWD/PfmDataManagerSQL.js

RESOURCE_FILES += \
    $$PWD/PfmDataManager.js \
    $$PWD/PfmChartBlock.qml

RESOURCE_FILES += \
    $$PWD/PfmTheme.qml

RESOURCE_FILES += \
    $$PWD/TouchInteractionButton.qml

RESOURCE_FILES += \
    $$PWD/SyncLoader.qml

RESOURCE_FILES += \
    $$PWD/MarkedCounter.qml \
    $$PWD/AvgText.qml \
    $$PWD/AvgTextBigStyle.qml

RESOURCE_FILES += \
    $$PWD/Range.qml \
    $$PWD/PopupNavigationBar.qml

RESOURCE_FILES += \
    $$PWD/PfmRangeCompareBlock.qml

RESOURCE_FILES += \
    $$PWD/PfmFilterBlock.qml

RESOURCE_FILES += \
    $$PWD/DatePicker.qml

RESOURCE_FILES += \
    $$PWD/PfmRangeCheckRow.qml \
    $$PWD/PfmRangeDatePickRow.qml

RESOURCE_FILES += \
    $$PWD/PfmQueryBarPrivate.js




