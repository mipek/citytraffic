import QtQuick 2.5

Item {
    id: syncLoader

    function show() {
        visible = true;
    }

    function hide() {
        visible = false;
    }

    anchors.fill: parent
    visible: false
    z: 9999

    Rectangle {
        anchors.fill: parent
        color: '#fff'
        opacity: .9
    }

    Column {
        spacing: dp(7)
        width: dp(200)
        anchors.centerIn: parent

        PfmText {
            text: 'Syncing new data...'
            font.pixelSize: sp(24)
            anchors.horizontalCenter: parent.horizontalCenter
        }

        PfmBusyIndicator {
            running: syncLoader.visible
            width: dp(32)
            height: dp(32)
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
