import QtQuick 2.5
import "PfmController.js" as PfmController

Column {
    property real avg: 0

    id: root
    spacing: dp(3)

    Item {
        width: dp(28)
        height: dp(18)
        anchors.horizontalCenter: parent.horizontalCenter

        Image {
            source: imagePath('increase.png')
            width: parent.width
            height: parent.height
            visible: avg > 0
        }

        Image {
            source: imagePath('decrease.png')
            width: parent.width
            height: parent.height
            visible: avg <= 0
        }
    }

    PfmText {
        font.pixelSize: sp(16)
        font.weight: Font.Medium
        color: avg <= 0 ? '#bf0029' : '#18A314'
        text: PfmController.getFormattedPercentage(avg)
    }
}
