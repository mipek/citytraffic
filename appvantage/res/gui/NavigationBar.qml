import QtQuick 2.5
import Frame.Mobile 1.0

NavigationBarController {
    signal backButtonClicked()
    signal logoutButtonClicked()
    signal rangeButtonClicked()

    property bool mainPageOnline: true
    property bool chartPageOnline: false
    property string rangeButtonText

    color: '#009e3a'

    label.font.pixelSize: dp(17)
    label.font.family: 'Avenir'
    label.color: '#fff'

    leftLoader.sourceComponent: Item {
        property bool backMode: !mainPageOnline

        implicitWidth: dp(44)
        implicitHeight: dp(44)

        NavigationBarButton {
            id: backButton

            interactiveBackground.visible: false

            icon.source: imagePath('back-arrow.png')
            icon.width: dp(13)
            icon.height: dp(22)

            visible: backMode

            mouse.onClicked: backButtonClicked()
            mouse.onPressedChanged: mouse.pressed ? touchIneractionBack.press() : touchIneractionBack.release()
        }

        TouchInteractionButton {
            maxOpacity: 0.5
            circle.color: '#fff'
            id: touchIneractionBack

            anchors.centerIn: backButton
        }

        NavigationBarButton {
            id: logoutButton

            interactiveBackground.visible: false

            icon.source: imagePath('logout.png')
            icon.width: dp(24)
            icon.height: dp(20)

            visible: !backMode

            mouse.onClicked: logoutButtonClicked()
            mouse.onPressedChanged: mouse.pressed ? touchIneractionLogout.press() : touchIneractionLogout.release()
        }

        TouchInteractionButton {
            maxOpacity: 0.5
            circle.color: '#fff'
            id: touchIneractionLogout

            anchors.centerIn: logoutButton
        }
    }

    rightLoader.sourceComponent: Item {
        implicitWidth: dp(60)
        implicitHeight: dp(44)

        NavigationBarButton {
            id: rangeButton

            interactiveBackground.visible: false

            label.visible: true
            label.text: rangeButtonText
            label.color: '#fff'

            visible: chartPageOnline

            mouse.onClicked: rangeButtonClicked()
            mouse.onPressedChanged: mouse.pressed ? rangeButtonIneractionLogout.press() : rangeButtonIneractionLogout.release()
        }

        TouchInteractionButton {
            maxOpacity: 0.5
            circle.color: '#fff'
            id: rangeButtonIneractionLogout

            anchors.centerIn: rangeButton
        }
    }

    z: 99
    height: dp(44)
}
