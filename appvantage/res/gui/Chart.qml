import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0
import com.triodor.pfm 1.0
import "PfmController.js" as PfmController
import "PfmQueryBarPrivate.js" as PfmQueryBarPrivate

PfmQueryablePage {
    property string pageTAG: '[Chart]'
    property alias dd: d
    property alias queryBar: queryBar

    id: root

    onAfterOpen: d.handleAfterOpen()
    onApiError: d.handleApiError(identifier, statusCode, metaCode, metaMessage)
    onCalculationCompleted: d.handleCalculationComplete(dataType, locationId, forCurrent)
    onResponseWifiGranularityDataSuccess: d.handleResponseWifiGranularity(granularityType, locationId, data, forCurrent)

    onLockRequest: d.handleLockRequest(dataType)
    onUnlockRequest: d.handleUnlockRequest(dataType)
    onAfterClose: d.handleAfterClose()
    onBeforeOpen: flickableBody.contentY = 0

    onSyncStarted: d.handleSyncStarted()
    onSyncCompleted: d.handleSyncCompleted()
    onSyncError: d.handleSyncError(errorId, code, message)

    function closeRightMenus(){
        console.log("here closes right menus...")
    }

    function changeQueryType(queryType, startDateCustom, finishDateCustom, yearBack) {
        var startDateRef;
        var finishDateRef;

        switch (queryType) {
        case PfmQueryBarPrivate.TYPE_MONTHLY:
            scene.rangeButtonText = "Month"
            break;
        case PfmQueryBarPrivate.TYPE_QUARTERLY:
            scene.rangeButtonText = "Quarter"
            break;
        case PfmQueryBarPrivate.TYPE_CUSTOM:
            scene.rangeButtonText = "Custom"

            startDateRef = startDateCustom
            finishDateRef = finishDateCustom

            break;
        case PfmQueryBarPrivate.TYPE_WEEKLY:
        default:
            scene.rangeButtonText = "Week"
            break;
        }

        queryBar.setQuery(startDateRef, finishDateRef, queryType, yearBack, 0);
    }

    function setLocationId(locationId, preCalculated) {
        d.locationId = locationId;
        if (preCalculated) {
            d.sensorPreviousCalculated = true;
        } else {
            d.sensorCurrentCalculated = false;
        }

        d.wifiPreviousCalculated = false;
        d.rendered = false;
        d.locked = false;

        d.hideAllContainers();

        var locationInfo = PfmController.getLocationById(d.locationId);
        if (locationInfo !== null) {
            root.pageInfo.title = locationInfo.name;
        }
    }

    function setQuery(offset) {
        queryBar.offset = offset;
    }

    function setAfterOpenHandler(callback) {
        if (Lang.isFunction(callback))
            d.callbacks.push(callback);
    }

    PfmQueryBar {
        id: queryBar

        locked: d.locked
        serverDate: serverModel.serverDate
        onQueryChanged: d.update()

        onNextTriggered: globalAnalytics.sendEvent('ChartQuery', 'Next #' + d.locationId + ' - ' + PfmController.getLocationById(d.locationId).name);
        onPreviousTriggered: globalAnalytics.sendEvent('ChartQuery', 'Previous #' + d.locationId + ' - ' + PfmController.getLocationById(d.locationId).name);
    }

    QtObject {
        property alias totalVisit: totalVisitBlock
        property alias newVisit: newVisitBlock
        property alias dwellTime: dwellTimeBlock
        property alias uniqueVisit: uniqueVisitBlock
        property alias queryBar: queryBar
        // property alias visitFrequency: visitFrequencyBlock

        property bool rendered: false
        property int locationId: 0
        property bool locked: false

        property bool sensorPreviousCalculated: false
        property bool sensorCurrentCalculated: false

        property bool wifiPreviousCalculated: false
        property bool wifiCurrentCalculated: false

        property bool wifiGranularityPreviousCalculated: false
        property bool wifiGranularityCurrentCalculated: false

        property var wifiGranularityCurrentData: null
        property var wifiGranularityPreviousData: null

        property var inProgressChartView: null

        property int wifiGranularityType: -1

        property var callbacks: ([])

        id: d

        onSensorPreviousCalculatedChanged: d.renderSensorChartsAsync()
        onSensorCurrentCalculatedChanged: d.renderSensorChartsAsync()
        onWifiPreviousCalculatedChanged: d.renderWifiChartsAsync()
        onWifiCurrentCalculatedChanged: d.renderWifiChartsAsync()
        onWifiGranularityPreviousCalculatedChanged: d.setWifiChartHeader()
        onWifiGranularityCurrentCalculatedChanged: d.setWifiChartHeader()

        function handleSyncStarted() {
            scene.syncLoader.show();
            Util.setTimeout(function() {
                d.update();
            }, 100);
        }

        function handleSyncCompleted() {
            scene.syncLoader.hide();
        }

        function handleSyncError(errorId, code, message) {
            scene.syncLoader.hide();
        }

        function handleAfterClose() {
            /*if (root.opener && root.opener.hasOwnProperty('setQuery'))
                root.opener.setQuery(queryBar.offset);*/
        }

        function handleAfterOpen() {
            var i = 0;
            var len = callbacks.length;
            for (; i < len; i++) {
                callbacks[i]();
            }

            callbacks = [];

            d.render();
        }

        function render() {
            globalAnalytics.sendEvent("Chart", '#' + d.locationId + ' - ' + PfmController.getLocationById(d.locationId).name);

            if (rendered)
                return;

            update();
            rendered = true;
        }

        function update() {
            if (d.locked)
                return;

            if (!PfmController.hasLocationData())
                return;

            sensorPreviousCalculated = false;
            sensorCurrentCalculated = false;
            wifiPreviousCalculated = false;
            wifiCurrentCalculated = false;
            wifiGranularityPreviousCalculated = false;
            wifiGranularityCurrentCalculated = false;

            var locationInfo = PfmController.getLocationById(d.locationId);
            if (locationInfo === null)
                return;

            if (locationInfo.options & PfmController.OPTION_HAS_SENSOR) {
                if (!d.rendered)
                    d.renderSensorCharts(true);

                d.setSensorChartsInProgress();
                Util.setTimeout(function() {
                    console.log(pageTAG, 'Sensor data updating with query:', JSON.stringify(queryBar.query()));
                    PfmController.updateVisitCounts(PfmController.DATA_TYPE_SENSOR, PfmController.GRANULARITY_NONE, root, queryBar.query(), false, d.locationId);
                }, 300);
            } else {
                console.log(pageTAG, 'No sensor data for this location.');
            }

            if (locationInfo.options & PfmController.OPTION_HAS_WIFI) {
                if (!d.rendered)
                    d.renderWifiCharts(true);

                d.setWifiChartsInProgress();

                Util.setTimeout(function() {
                    var granularityType = PfmQueryBarPrivate.getGranularityType(queryBar.dd.startDateRef, queryBar.dd.finishDateRef, null, queryBar.type);

                    console.log(pageTAG, 'Wifi data updating with query:', JSON.stringify(queryBar.query()), 'with granularity type:', granularityType);
                    PfmController.updateVisitCounts(PfmController.DATA_TYPE_WIFI, granularityType, root, queryBar.query(), false, d.locationId);
                }, 300);
            }  else {
                console.log(pageTAG, 'No Wifi data for this location.');
            }
        }

        function handleResponseWifiGranularity(granularityType, locationId, data, forCurrent) {
            if (parseInt(locationId, 10) !== d.locationId)
                return;

            if (forCurrent) {
                wifiGranularityCurrentData = data;
                wifiGranularityCurrentCalculated = true;
            } else {
                wifiGranularityPreviousData = data;
                wifiGranularityPreviousCalculated = true;
            }

            wifiGranularityType = granularityType;
        }

        function setWifiChartHeader() {
            if (!wifiGranularityPreviousCalculated || !wifiGranularityCurrentCalculated)
                return;

            var current = wifiGranularityCurrentData;
            var previous = wifiGranularityPreviousData;

            if (inProgressChartView === null || inProgressChartView === newVisitBlock) {
                var newCurrent = { formatted: current.new_percent + '%', value: current.new_percent };
                var newPrevious = { formatted: (previous ? previous.new_percent : 0) + '%' , value: (previous ? previous.new_percent : 0) };

                setChartHeader(newVisitBlock, newCurrent, newPrevious, function() {
                    return Number(current.new_percent - (previous ? previous.new_percent : 0));
                });
            }

            if (inProgressChartView === null || inProgressChartView === dwellTimeBlock) {
                var currentDwellTime = { formatted: DateTime.timeFormatter('?d ?h ?m !s', current.dwell_time), value: current.dwell_time };
                var previousDwellTime = { formatted: previous ? DateTime.timeFormatter('?d ?h ?m !s', previous.dwell_time) : '', value: previous ? previous.dwell_time : 0 };

                setChartHeader(dwellTimeBlock, currentDwellTime, previousDwellTime, function() {
                    return PfmController.getPercentageOfOverview(currentDwellTime.value, previousDwellTime.value);
                });
            }

            if (inProgressChartView === null || inProgressChartView === uniqueVisitBlock) {
                var uniqueCurrent = { formatted: current.unique_percent + '%', value: current.unique_percent };
                var uniquePrevious = { formatted: (previous ? previous.unique_percent : 0) + '%' , value: (previous ? previous.unique_percent : 0) };

                setChartHeader(uniqueVisitBlock, uniqueCurrent, uniquePrevious, function() {
                    return Number(current.unique_percent - (previous ? previous.unique_percent : 0));
                });
            }

            inProgressChartView = null;
        }

        function handleCalculationComplete(dataType, locationId, forCurrent) {
            if (locationId !== d.locationId)
                return;

            if (dataType === PfmController.DATA_TYPE_SENSOR) {
                if (forCurrent)
                    d.sensorCurrentCalculated = true;
                else
                    d.sensorPreviousCalculated = true;
            } else {
                if (forCurrent)
                    d.wifiCurrentCalculated = true;
                else
                    d.wifiPreviousCalculated = true;
            }
        }

        function hideAllContainers() {
            var charts = [totalVisitBlock, newVisitBlock, dwellTimeBlock, uniqueVisitBlock /*, visitFrequencyBlock*/];
            for (var i = 0; i < charts.length; i++) {
                charts[i].visible = false;
                setChartHeader(charts[i]);
            }
        }

        function renderSensorChartsAsync() {
            setSensorChartsInProgress();
            Util.setTimeout(function() {
                d.renderSensorCharts();
            }, 500);
        }

        function renderWifiChartsAsync() {
            setWifiChartsInProgress();
            Util.setTimeout(function() {
                d.renderWifiCharts();
            }, 500);
        }

        function renderSensorCharts(forceEmptyChart) {
            if (!forceEmptyChart && (!sensorPreviousCalculated || !sensorCurrentCalculated))
                return;

            buildChart(totalVisitBlock, PfmController.DATA_TYPE_SENSOR, 'total_count', 'Total Visits', forceEmptyChart, false, true);
        }

        function setSensorChartsInProgress() {
            totalVisitBlock.inProgress = true;
        }

        function renderWifiCharts(forceEmptyChart) {
            if (!forceEmptyChart && (!wifiPreviousCalculated || !wifiCurrentCalculated))
                return;

            buildChart(newVisitBlock, PfmController.DATA_TYPE_WIFI, 'new_percent', 'New Visits', forceEmptyChart, false, false);
            buildChart(dwellTimeBlock, PfmController.DATA_TYPE_WIFI, 'dwell_time', 'Dwell Time', forceEmptyChart, true, false);
            buildChart(uniqueVisitBlock, PfmController.DATA_TYPE_WIFI, 'unique_percent', 'Unique Visits', forceEmptyChart, false, false);
            // buildChart(visitFrequencyBlock, PfmController.DATA_TYPE_WIFI, 'frequency', 'Visit Frequency', forceEmptyChart);
        }

        function setWifiChartsInProgress() {
            newVisitBlock.inProgress = true;
            dwellTimeBlock.inProgress = true;
            uniqueVisitBlock.inProgress = true;
            // visitFrequencyBlock.inProgress = true;
        }

        function setChartHeader(chartBlock, currentTotalData, previousTotalData, avgHandlerCallback) {
            var previousCount = '';
            var currentCount = ''
            var avg = '';

            currentTotalData = currentTotalData || 0;
            previousTotalData = previousTotalData || 0;

            if (Lang.isObjectLiteral(previousTotalData)) {
                previousCount = previousTotalData.formatted;
                currentCount = currentTotalData.formatted;

                previousTotalData = previousTotalData.value;
                currentTotalData = currentTotalData.value;
            } else {
                previousCount = Numeral.numeral(previousTotalData).format('0,0');
                currentCount = Numeral.numeral(currentTotalData).format('0,0');
            }

            avg = avgHandlerCallback
                    ? avgHandlerCallback(currentTotalData, previousTotalData)
                    : PfmController.getPercentageOfOverview(currentTotalData, previousTotalData);

            chartBlock.current = currentCount;
            chartBlock.previous = previousCount;
            chartBlock.avg = avg;
        }

        function setMinMax(minMax, xVal, yVal) {
            if (xVal !== null) {
                if (minMax.x.min === null) {
                    minMax.x.min = xVal;
                    minMax.x.max = xVal;
                } else {
                    minMax.x.min = Math.min(minMax.x.min, xVal);
                    minMax.x.max = Math.max(minMax.x.max, xVal);
                }
            }

            if (yVal !== null) {
                if (minMax.y.min === null) {
                    minMax.y.min = yVal;
                    minMax.y.max = yVal;
                } else {
                    minMax.y.min = Math.min(minMax.y.min, yVal);
                    minMax.y.max = Math.max(minMax.y.max, yVal);
                }
            }
        }

        /**
         * Chart shows comparing the previous date with current date
         * X-Axis represents the date. (if we put previous date's data with current date's data  their shape will be seen distinct.)
         * Current Date data's shape must be top of Previous data's shape
         * To do that,this function's purpose is adjusting the X-Axis values(previous date) by 1 unit (not the previous date's data! just Date for view)
         *
         * @Param type type of query ( montly , quarterly, custom , weekly) if it is weekly Date must be adjusted by 7 day ( 7 day is a unit)
         * return adjusted Date
        */
        function createAdjustedPreviousTime(type, previousTime)
        {
            //return previousTime;

            var adjustedDate;
            var DAY = 86400000;

            switch (type) {
                case PfmQueryBarPrivate.TYPE_MONTHLY:
                    adjustedDate = new Date(previousTime);
                    adjustedDate.setMonth(adjustedDate.getMonth() + 1);

                    return adjustedDate.getTime();

                case PfmQueryBarPrivate.TYPE_QUARTERLY:
                    adjustedDate = new Date(previousTime);
                    adjustedDate.setMonth(adjustedDate.getMonth() + 3);

                    return adjustedDate.getTime();
                case PfmQueryBarPrivate.TYPE_CUSTOM:

                    var query = queryBar.query();
                    var diff = query.previous.finish - query.previous.start;
                    adjustedDate = new Date(previousTime + diff);

                    return adjustedDate.getTime();
                case PfmQueryBarPrivate.TYPE_WEEKLY:
                default:

                    return new Date(previousTime +  7 * DAY);
            }
        }

        function buildChart(chartBlock, dataType, field, title, forceEmptyChart, timeFormat, calculateHeader) {
            chartBlock.visible = true;
            chartBlock.title = title;

            var previous = [];
            var current = [];
            var minMax = {
                x: { min: null, max: null },
                y: { min: null, max: null }
            };

            var granularityType = PfmQueryBarPrivate.getGranularityType(queryBar.dd.startDateRef, queryBar.dd.finishDateRef, null, queryBar.type);
            var availableAxisType = PfmQueryBarPrivate.getAvailableAxisList(queryBar.dd.startDateRef, queryBar.dd.finishDateRef, queryBar.type);

            if (!forceEmptyChart) {
                var query = queryBar.query();
                var previousData = query.previous === null ? {} : PfmController.getData(dataType, d.locationId, query.previous.start, query.previous.finish, true, query.type);
                var currentData = PfmController.getData(dataType, d.locationId, query.current.start, query.current.finish, true, query.type);

                var i;
                var timestamp;
                var val;
                var parts;
                var day, month, year;
                var endDate;

                for (i in currentData) {
                    if (currentData.hasOwnProperty(i)) {
                        parts = i.split('-');
                        year = parseInt(parts[0], 10);
                        month = parseInt(parts[1][0] === '0' ? parts[1].substr(1) : parts[1], 10);
                        day = parseInt(parts[2], 10);

                        val = currentData[i][field] || 0;

                        timestamp = (new Date(Date.UTC(year, month - 1, day))).getTime();
                        current.push(Qt.point(timestamp, val));

                        setMinMax(minMax, timestamp, val);
                    }
                }

                endDate = PfmQueryBarPrivate.getEndDateOf(query.current.start, query.type, queryBar.dd.startDateRef, queryBar.dd.finishDateRef);

                i = 0;
                PfmController.fromStartToFinish(query.current.start, endDate, function(currentDate, currentDay) {
                    val = currentDate.getTime();
                    if (!currentData.hasOwnProperty(DateTime.date('Y-m-d', val))) {
                        current.push(Qt.point(val, 0));

                        setMinMax(minMax, val, 0);
                        i++;
                    }

                }, query.type);

                var k = 0;
                for (i in previousData) {
                    if (previousData.hasOwnProperty(i)) {
                        if (!current[k]) {
                            // normally this should not be happening although it is blocker
                            console.log(pageTAG, 'Date not found for syncing chart current/previous data:', i);
                            continue;
                        }

                        parts = i.split('-');
                        year = parseInt(parts[0], 10);
                        month = parseInt(parts[1][0] === '0' ? parts[1].substr(1) : parts[1], 10);
                        day = parseInt(parts[2], 10);

                        val = previousData[i][field] || 0;

                        timestamp = (new Date(Date.UTC(year, month - 1, day))).getTime();
                        //adjust previous time to cover current time//check the func comments
                        //timestamp = createAdjustedPreviousTime(query.type, timestamp);

                        //previous.push(Qt.point(queryBar.queryToYearBackForPrevious ? current[k].x : timestamp, val));
                        previous.push(Qt.point(current[k].x , val));

                        //setMinMax(minMax, queryBar.queryToYearBackForPrevious ? current[k].x : timestamp, val);
                        setMinMax(minMax, current[k].x , val);
                        k++;
                    }
                }

                if (query.previous !== null) {
                    endDate = PfmQueryBarPrivate.getEndDateOf(query.previous.start, query.type, queryBar.dd.startDateRef, queryBar.dd.finishDateRef);

                    i = 0;
                    PfmController.fromStartToFinish(query.previous.start, endDate, function(currentDate, currentDay) {
                        if (previous[i] === undefined) {
                            if (!current[i]) {
                                // normally this should not be happening although it is blocker
                                console.log(pageTAG, 'Date not found for syncing chart current/previous data:', currentDate);
                                return;
                            }

                            //var adjustedTime = createAdjustedPreviousTime(query.type, currentDate.getTime());
                            //timestamp = queryBar.queryToYearBackForPrevious ? current[i].x : currentDate.getTime();
                            timestamp = current[i].x;

                            previous.push(Qt.point(timestamp, 0));
                            setMinMax(minMax, timestamp, 0);
                        }

                        i++;
                    }, queryBar.type);
                }

                if (calculateHeader) {
                    var previousTotalData = PfmController.getTotalCount(dataType, previousData, field);
                    var currentTotalData = PfmController.getTotalCount(dataType, currentData, field);
                    setChartHeader(chartBlock, currentTotalData, previousTotalData);
                }
            }

            chartBlock.drawChart(previous, current, timeFormat ? true : false, minMax.x, minMax.y, granularityType, availableAxisType[0]);
            chartBlock.field = field;
            if (!forceEmptyChart) {
                Util.setTimeout(function() {
                    chartBlock.inProgress = false;
                }, 300);
            }
        }

        function handleLockRequest(dataType) {
            locked = true;
            console.log(pageTAG, 'Lock Request:', dataType);
        }

        function handleUnlockRequest(dataType) {
            locked = false;
            console.log(pageTAG, 'Unlock Request:', dataType);
        }

        function handleApiError(identifier, statusCode, metaCode, metaMessage) {
            d.locked = false;
            if (failedDialog.supports())
                failedDialog.simpleAlert();
        }

        function handleRecalculateGranularity(granularityType, chartView) {
            wifiGranularityCurrentCalculated = false;
            wifiGranularityPreviousCalculated = false;

            inProgressChartView = chartView;

            var query = queryBar.query();
            PfmController.executeGranularity(root, granularityType, PfmController.DATA_TYPE_WIFI, d.locationId, query.current.start, query.current.finish, true);
            PfmController.executeGranularity(root, granularityType, PfmController.DATA_TYPE_WIFI, d.locationId, query.previous.start, query.previous.finish, false);
        }
    }

    Flickable {
        id: flickableBody

        anchors.fill: parent
        anchors.topMargin: queryBar.height
        clip: true
        maximumFlickVelocity: 10000
        flickDeceleration: 5000

        contentWidth: parent.width
        contentHeight: Math.max(height, graphBox.height)

        Column {
            id: graphBox

            width: parent.width
            spacing: dp(0)

            PfmChartBlock {
                id: totalVisitBlock
                active: root.active
                locationId: d.locationId
                queryBar: root.queryBar
            }

            PfmChartBlock {
                id: newVisitBlock
                active: root.active
                locationId: d.locationId
                queryBar: root.queryBar

                onRecalculateGranularity: d.handleRecalculateGranularity(granularityType, this)
            }

            PfmChartBlock {
                id: dwellTimeBlock
                active: root.active
                locationId: d.locationId
                queryBar: root.queryBar

                onRecalculateGranularity: d.handleRecalculateGranularity(granularityType, this)
            }

            PfmChartBlock {
                id: uniqueVisitBlock
                active: root.active
                locationId: d.locationId
                queryBar: root.queryBar

                onRecalculateGranularity: d.handleRecalculateGranularity(granularityType, this)
            }

            /*PfmChartBlock {
                id: visitFrequencyBlock
                active: root.active
                locationId: d.locationId
                queryBar: root.queryBar
            }*/
        }
    }

    NativeDialog {
        id: failedDialog
        title: 'Visit Count Retrieval Failed'
        message: 'There is an error occured while we try to retrieve locations.'
    }




}
