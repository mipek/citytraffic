import QtQuick 2.5
import "PfmController.js" as PfmController

Row {
    property real avg: 0

    id: root
    spacing: dp(3)

    Item {
        width: dp(14)
        height: dp(9)
        anchors.verticalCenter: parent.verticalCenter

        Image {
            source: imagePath('increase.png')
            width: parent.width
            height: parent.height
            visible: avg > 0
        }

        Image {
            source: imagePath('decrease.png')
            width: parent.width
            height: parent.height
            visible: avg <= 0
        }
    }

    PfmText {
        font.pixelSize: sp(16)
        font.weight: Font.Medium
        color: avg <= 0 ? '#bf0029' : '#18A314'
        text: PfmController.getFormattedPercentage(avg)
    }
}
