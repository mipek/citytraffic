import QtQuick 2.5
import QtQuick.Extras 1.4
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0

Item {
    signal beforeShow()
    signal afterShow()

    signal beforeHide()
    signal afterHide()

    anchors.fill: parent
    visible: false

    Component.onCompleted: d.init()

    function show(date, resultCallback, cancelCallback) {
        if (d.inProgress)
            return;

        if (date !== null)
            d.setFromDate(date);

        if (Lang.isFunction(resultCallback))
            d.resultCallback = resultCallback;

        if (Lang.isFunction(cancelCallback))
            d.cancelCallback = cancelCallback;

        if (!visible) {
            beforeShow();

            d.inProgress = true; // we don't wait for polishing
            visible = true;
            overlay.show();
            container.y = height - container.height;
        }
    }

    function hide() {
        if (d.inProgress || !visible)
            return;

        beforeHide();

        d.inProgress = true;  // we don't wait for polishing
        overlay.hide();
        container.y = height;
    }

    QtObject {
        id: d

        property bool inited: false
        property var resultCallback: null
        property var cancelCallback: null
        property bool cancelled: false

        property bool inProgress: false

        property var result: null

        property ListModel dayModel: ListModel {}
        property ListModel monthModel: ListModel {}
        property ListModel yearModel: ListModel {}

        function init() {
            if (inited)
                return;

            var i = 0;
            var len = 31;
            for (; i < len; i++)
                dayModel.append({ value: i + 1 });

            i = 0;
            len = 12;
            for (; i < len; i++)
                monthModel.append({ value: DateTime.m_shortMonths[i] });

            i = 1970;
            len = parseInt(DateTime.date('Y'), 10);
            for (; i <= len; i++)
                yearModel.append({ value: i });

            inited = true;
        }

        function initPicker() {
            var i = 0;
            var len = tumbler.children.length;
            var row = null;
            for (; i < len; i++) {
                if (tumbler.children[i].toString().indexOf('QQuickRow') === 0) {
                    row = tumbler.children[i];
                    break;
                }
            }

            var found = false;
            if (row !== null) {
                i = 0;
                len = row.children.length;

                var item;
                var tumblerItem;

                for (; i < len; i++) {
                    item = row.children[i];
                    if (item.toString().indexOf('QQuickItem_') === 0) {
                        tumblerItem = item.children[0];
                        if (tumblerItem.toString().indexOf('QQuickPathView') === 0) {
                            tumblerItem.maximumFlickVelocity = 10000;
                            tumblerItem.flickDeceleration = 5000;
                            found = true;
                        }
                    }
                }

            }

            if (!found) {
                console.log('[DatePicker]', 'Row could not found for adjusting flickable velocity.');
            }
        }

        function setFromDate(date) {
            setDay(date.getDate());
            setMonth(date.getMonth());
            setYear(date.getFullYear());
        }

        function setDay(day) {
            tumbler.setCurrentIndexAt(0, day - 1);
        }

        function setMonth(month) {
            tumbler.setCurrentIndexAt(1, month);
        }

        function setYear(year) {
            tumbler.setCurrentIndexAt(2, year - 1970);
        }

        function handleAnimationStateChanged(running) {
            if (!running) {
                if (container.y === height)
                    completeClosing();
                else
                    completeOpening();
            }
        }

        function completeOpening() {
            inProgress = false;
            afterShow();
        }

        function completeClosing() {
            if (cancelled) {
                if (cancelCallback !== null) {
                    try {
                        cancelCallback();
                    } catch (e) {
                        console.log('[DatePicker]', 'Javascript Error Occured for Cancel Callback:', e);
                    }
                }
            } else {
                if (resultCallback !== null) {
                    try {
                        resultCallback(result);
                    } catch (e) {
                        console.log('[DatePicker]', 'Javascript Error Occured for Result Callback:', e);
                    }
                }
            }

            result = null;
            inProgress = false;
            visible = false;

            afterHide();
        }

        function handleDone() {
            var day = dayModel.get(dayColumn.currentIndex).value;
            var month = DateTime.m_shortMonths.indexOf(d.monthModel.get(monthColumn.currentIndex).value);
            var year = yearModel.get(yearColumn.currentIndex).value;

            result = new Date();
            result.setDate(day);
            result.setMonth(month);
            result.setFullYear(year);

            hide();
        }

        function handleCancel() {
            result = null;
            hide();
        }
    }

    Rectangle {
        id: overlay

        function show() {
            opacity = 0.7;
        }

        function hide() {
            opacity = 0;
        }

        anchors.fill: parent
        opacity: 0
        Behavior on opacity { NumberAnimation { duration: 300 } }
        color: '#fff'

        MouseArea {
            anchors.fill: parent
            preventStealing: true
        }
    }

    Item {
        id: container

        width: parent.width
        height: dp(237)
        y: parent.height

        Behavior on y {
            NumberAnimation {
                duration: 300
                easing.type: Easing.InOutQuad
                onRunningChanged: d.handleAnimationStateChanged(running)
            }
        }

        Rectangle {
            id: toolbar

            color: '#009e3a'
            width: parent.width
            height: dp(37)

            Rectangle {
                width: parent.width
                height: dp(1)
                color: '#009e3a'
                anchors.top: parent.top
            }

            NavigationBarButton {
                id: cancelButton

                interactiveBackground.visible: false

                label.visible: true
                label.text: 'Cancel'
                label.color: 'white'
                label.font.pixelSize: sp(15)

                mouse.onClicked: d.handleCancel()
                mouse.onPressedChanged: mouse.pressed ? cancelButtonIneractionLogout.press() : cancelButtonIneractionLogout.release()

                anchors.left: parent.left
                anchors.leftMargin: dp(15)

                anchors.verticalCenter: parent.verticalCenter
            }

            TouchInteractionButton {
                maxOpacity: 0.5
                circle.color: 'white'
                id: cancelButtonIneractionLogout

                anchors.centerIn: cancelButton
            }

            NavigationBarButton {
                id: doneButton

                interactiveBackground.visible: false

                label.visible: true
                label.text: 'Done'
                label.color: 'white'
                label.font.pixelSize: sp(15)

                mouse.onClicked: d.handleDone()
                mouse.onPressedChanged: mouse.pressed ? doneButtonIneractionLogout.press() : doneButtonIneractionLogout.release()

                anchors.right: parent.right
                anchors.rightMargin: dp(15)

                anchors.verticalCenter: parent.verticalCenter
            }

            TouchInteractionButton {
                maxOpacity: 0.5
                circle.color: 'white'
                id: doneButtonIneractionLogout

                anchors.centerIn: doneButton
            }
        }

        Tumbler {
            property int columnWidth: parseInt(container.width / 3, 10) - dp(14)

            id: tumbler

            width: parent.width
            height: dp(200)
            y: toolbar.height

            style: TumblerStyle {
                id: tumblerStyle

                padding.left: 0
                padding.right: 0
                padding.top: 0
                padding.bottom: 0

                delegate: Item {
                    implicitHeight: tumbler.height / tumblerStyle.visibleItemCount

                    Text {
                        text: styleData.value
                        color: styleData.current ? "#000" : "#808285"
                        font.weight: Font.Medium
                        font.pixelSize: sp(17)
                        font.family: 'Avenir'

                        opacity: 0.4 + Math.max(0, 1 - Math.abs(styleData.displacement)) * 0.6
                        anchors.centerIn: parent
                    }
                }

                property Component frame: Item {}
                property Component separator: Item {}
                property Component background: Rectangle {}
                property Component foreground: Item {}
            }

            TumblerColumn {
                id: dayColumn
                model: d.dayModel
                columnForeground: tumblerForground

                width: tumbler.columnWidth
            }

            TumblerColumn {
                id: monthColumn
                model: d.monthModel
                columnForeground: tumblerForground

                width: tumbler.columnWidth
            }

            TumblerColumn {
                id: yearColumn
                model: d.yearModel
                columnForeground: tumblerForground

                width: tumbler.width - tumbler.columnWidth * 2
            }

            Component.onCompleted: d.initPicker()
        }

        Component {
            id: tumblerForground

            Item {
                Rectangle {
                    width: parent.width
                    height: dp(1)
                    color: "#009e3a" //"#808285"
                }

                Rectangle {
                    width: parent.width
                    height: dp(1)
                    color: "#808285"
                    anchors.bottom: parent.bottom
                }
            }
        }
    }
}
