import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import Frame.Mobile 1.0
import Frame.Mobile.Core 1.0
import "PfmController.js" as PfmController

PfmQueryablePage {
    property string pageTAG: '[Home]'

    id: root

    onApiError: d.handleApiError(identifier, statusCode, metaCode, metaMessage)
    onLockRequest: d.handleLockRequest(dataType)
    onUnlockRequest: d.handleUnlockRequest(dataType)
    onAfterOpen: d.render()

    onResponseLocationDataSuccess: d.update()
    onResponseLocationDataFailed: d.handleDataFailed(errorId, code, message)

    onSyncStarted: d.handleSyncStarted()
    onSyncCompleted: d.handleSyncCompleted()
    onSyncError: d.handleSyncError(errorId, code, message)

    function setQuery(offset) {
        queryBar.offset = offset;
    }

    QtObject {
        id: d

        property bool rendered: false
        property bool locked: false
        property ListModel model: PfmController.getModel()
        property bool busy: false

        function handleSyncStarted() {
            scene.syncLoader.show();
            Util.setTimeout(function() {
                d.update();
            }, 100);
        }

        function handleSyncCompleted() {
            scene.syncLoader.hide();
        }

        function handleSyncError(errorId, code, message) {
            scene.syncLoader.hide();
        }

        function render() {
            globalAnalytics.sendScreenView("Dashboard");
            if (PfmController.getSharedData('rebuildModel')) {
                rendered = false;
                PfmController.resetModel();
                PfmController.setSharedData('rebuildModel', false);
            }

            if (rendered)
                return;

            d.busy = true;
            PfmController.init(root, queryBar.query());
            rendered = true;
        }

        function update() {
            if (!PfmController.hasLocationData())
                return;

            d.busy = false;
            if (d.locked)
                return;

            PfmController.updateVisitCounts(PfmController.DATA_TYPE_SENSOR, PfmController.GRANULARITY_NONE, root, queryBar.query(), true);
        }

        function handleLockRequest(dataType) {
            if (dataType === PfmController.DATA_TYPE_SENSOR) {
                d.locked = true;
                console.log(pageTAG, 'Lock Request:', dataType);
            }
        }

        function handleUnlockRequest(dataType) {
            if (dataType === PfmController.DATA_TYPE_SENSOR) {
                d.locked = false;
                console.log(pageTAG, 'Unlock Request:', dataType);
            }
        }

        function handleDataFailed(errorId, code, message) {
            d.locked = false;
            d.busy = false;

            if (failedDialog.supports())
                failedDialog.simpleAlert();
        }

        function handleLocationTap(locationId) {
            if (d.locked)
                return;

            var chartPage = navigation.open('chart');
            if (chartPage) {
                chartPage.setLocationId(locationId, true);
                chartPage.setAfterOpenHandler(function() {
                    chartPage.setQuery(queryBar.offset);
                });
            }
        }
    }

    PfmQueryBar {
        id: queryBar

        locked: d.locked || d.busy
        serverDate: serverModel.serverDate
        onQueryChanged: d.update()

        onNextTriggered: globalAnalytics.sendEvent('DashboardQuery', 'Next');
        onPreviousTriggered: globalAnalytics.sendEvent('DashboardQuery', 'Previous');
    }

    PfmBlock {
        anchors.fill: parent
        anchors.topMargin: queryBar.height

        body: Item {
            PfmBusyIndicator {
                anchors.centerIn: parent
                width: dp(48)
                height: dp(48)
                visible: d.busy
            }

            ListView {
                id: locationListView

                anchors.fill: parent
                clip: true

                model: d.model
                cacheBuffer: 9999

                maximumFlickVelocity: 10000
                flickDeceleration: 5000

                delegate: Rectangle {
                    id: delegateRoot

                    property real avg: PfmController.getPercentageOfOverview(currentVisitCount, previousVisitCount)

                    width: parent.width
                    height: dp(70)
                    color: index % 2 ? '#fff' : '#f9f9f9'

                    RowLayout {
                        anchors.fill: parent
                        Item { implicitWidth: dp(15) }
                        Item {
                            height: parent.height
                            Layout.fillWidth: true

                            PfmText {
                                text: name
                                font.pixelSize: theme.overviewLocationFontSize
                                font.weight: Font.Medium
                                color: '#1c2b30'

                                width: parent.width
                                elide: Text.ElideRight

                                anchors.verticalCenter: parent.verticalCenter
                            }
                        }

                        Item {
                            implicitWidth: dp(16 + 30)
                            implicitHeight: parent.height
                            visible: sensorState > PfmController.STATE_ERROR && sensorState < PfmController.STATE_LOADED_ALL

                            PfmBusyIndicator {
                                width: dp(16)
                                height: dp(16)
                                anchors.centerIn: parent
                            }
                        }

                        Item {
                            implicitWidth: dp(16)
                            implicitHeight: parent.height
                            visible: sensorState === PfmController.STATE_ERROR

                            PfmText {
                                text: '!'
                                font.bold: true
                                font.pixelSize: theme.overviewNumberFontSize

                                anchors.centerIn: parent
                            }
                        }

                        Item {
                            height: parent.height
                            implicitWidth: dp(85)
                            visible: sensorState == PfmController.STATE_LOADED_ALL

                            MarkedCounter {
                                text: Numeral.numeral(currentVisitCount).format('0,0')
                                textView.font.pixelSize: sp(16)
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: parent.right
                            }
                        }

                        Item {
                            height: parent.height
                            implicitWidth: dp(75)
                            visible: sensorState == PfmController.STATE_LOADED_ALL

                            AvgText {
                                id: avgText

                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: parent.right
                                avg: delegateRoot.avg
                            }
                        }

                        Item {
                            height: parent.height
                            implicitWidth: dp(30)
                            visible: sensorState == PfmController.STATE_LOADED_ALL

                            Image {
                                source: imagePath('list-right-arrow.png')
                                width: dp(13)
                                height: dp(21)

                                anchors.verticalCenter: parent.verticalCenter
                                anchors.left: parent.left
                                anchors.leftMargin: dp(5)
                            }
                        }
                    }

                    TouchInteractionButton {
                        id: listInteraction
                        circlar: false
                        width: parent.width
                        height: parent.height
                    }

                    MouseArea {
                        anchors.fill: parent
                        enabled: !d.locked
                        onClicked: d.handleLocationTap(locationId)
                        onPressedChanged: pressed ? listInteraction.press() : listInteraction.release()
                    }
                }
            }
        }
    }

    NativeDialog {
        id: failedDialog
        title: qsTr('Location Retrieval Failed')
        message: qsTr('There is an error occured while we try to retrieve locations.')
    }
}

