.import Frame.Mobile.Core 1.0 as FrameCore

function Adapter(pfmController, manager) {
    this.m_pfmController = pfmController;
    this.m_manager = manager;
    this.m_db = FrameCore.Db;
}

Adapter.prototype = {
    getSavedUser: function() {
        return this.m_db.fetchRow("SELECT * FROM pfm_user");
    },
    deleteUser: function() {
        this.m_db.query("DELETE FROM pfm_user");
    },
    saveUser: function(username, password) {
        this.deleteUser();
        return this.m_db.query("INSERT INTO pfm_user (username, password) VALUES (?, ?)", [username, password]);
    },
    clearData: function() {
        var closure = this;
        FrameCore.Util.setTimeout(function() {
            closure.m_db.query("DELETE FROM pfm_location");
            closure.m_db.query("DELETE FROM pfm_sensor_visit");
            closure.m_db.query("DELETE FROM pfm_wifi_visit");

            closure.revokeWifiGranularity();
            closure.revokeSensorGranularity();
        }, 100);
    },
    requestLocationData: function(wholeData) {
        var data = {};
        var rows = this.m_db.fetchAll("SELECT * FROM pfm_location");
        var i = 0;
        var len = rows.length;
        var row;
        var locationId;
        for (; i < len; i++) {
            row = rows[i];
            locationId = row.location_id;
            data[locationId] = {
                location_id:  row.location_id,
                name:         row.name,
                opening_time: row.opening_time,
                closing_time: row.closing_time,
                options:      row.options
            };

            if (wholeData) {
                data[locationId].created_at = row.created_at;
                data[locationId].updated_at = row.updated_at;
            }
        }

        return len > 0 ? data : null;
    },
    saveLocationData: function(data) {
        var existing = this.requestLocationData(true);
        if (existing === null)
            existing = {};

        this.m_db.query("DELETE FROM pfm_location");

        var bind;
        var currentDateTime = (new Date()).getTime();
        var current;
        for (var i in data) {
            if (data.hasOwnProperty(i)) {
                current = data[i];
                bind = [
                    current.location_id,
                    current.name,
                    current.opening_time,
                    current.closing_time,
                    current.options,
                    existing.hasOwnProperty(current.location_id) ? existing[current.location_id].created_at : currentDateTime,
                    currentDateTime,
                ];

                this.m_db.query("INSERT INTO pfm_location (location_id, name, opening_time, closing_time, options, created_at, updated_at)"
                            + " VALUES (?, ?, ?, ?, ?, ?, ?)", bind);
            }
        }
    },
    requestSensorVisitData: function(locationId, data, dateList) {
        if (data === null || data === undefined)
            data = {};

        var result = { status: this.m_pfmController.NOCHANGE, data: data };
        var len = dateList.length;
        if (len === 0)
            return result;

        var query = JSON.parse(JSON.stringify(dateList));
        query.push(locationId);

        var rows = this.m_db.fetchAll("SELECT * FROM pfm_sensor_visit WHERE date IN (" + FrameCore.Util.arrayFill(len, '?') + ") AND location_id = ? ", query);
        var i = 0;
        var current;
        len = rows.length;
        for (; i < len; i++) {
            current = rows[i];
            data[current.date] = {
                total_count: current.total_count
            };
        }

        var foundOne = len > 0;
        var lostOne = dateList.length !== len;

        if (foundOne)
            result.status = lostOne ? this.m_pfmController.PARTIAL : this.m_pfmController.COMPLETED;

        return result;
    },
    saveSensorVisitData: function(locationId, data) {
        var dateList = this.m_pfmController.getDateListFromObject(data);
        var existing = this.requestSensorVisitData(null, locationId, dateList);

        var deleteQuery = JSON.parse(JSON.stringify(dateList));
        deleteQuery.push(locationId);

        this.m_db.query("DELETE FROM pfm_sensor_visit WHERE date IN (" + FrameCore.Util.arrayFill(deleteQuery.length - 1, '?') + ") AND location_id = ?", deleteQuery);

        var bind;
        var currentDateTime = (new Date()).getTime();
        var current;
        for (var date in data) {
            if (!data.hasOwnProperty(date))
                continue;

            current = data[date];
            bind = [
                locationId,
                date,
                current.total_count,
                existing.hasOwnProperty(date) ? existing[date].created_at : currentDateTime,
                currentDateTime,
            ];

            this.m_db.query("INSERT INTO pfm_sensor_visit (location_id, date, total_count, created_at, updated_at)"
                        + " VALUES (?, ?, ?, ?, ?)", bind);
        }
    },
    requestWifiVisitData: function(locationId, data, dateList) {
        if (data === null || data === undefined)
            data = {};

        var result = { status: this.m_pfmController.NOCHANGE, data: data };
        var len = dateList.length;
        if (len === 0)
            return result;

        var query = JSON.parse(JSON.stringify(dateList));
        query.push(locationId);

        var rows = this.m_db.fetchAll("SELECT * FROM pfm_wifi_visit WHERE date IN (" + FrameCore.Util.arrayFill(len, '?') + ") AND location_id = ?", query);
        var i = 0;
        var current;
        len = rows.length;
        for (; i < len; i++) {
            current = rows[i];
            data[current.date] = {
                capture_rate:    current.capture_rate,
                unique_percent:  current.unique_percent,
                new_percent:     current.new_percent,
                dwell_time:      current.dwell_time,
                frequency:       current.frequency
            };
        }

        var foundOne = len > 0;
        var lostOne = dateList.length !== len;

        if (foundOne)
            result.status = lostOne ? this.m_pfmController.PARTIAL : this.m_pfmController.COMPLETED;

        return result;
    },

    saveWifiVisitData: function(locationId, data) {
        var dateList = this.m_pfmController.getDateListFromObject(data);
        var existing = this.requestWifiVisitData(null, locationId, dateList);

        var deleteQuery = JSON.parse(JSON.stringify(dateList));
        deleteQuery.push(locationId);

        this.m_db.query("DELETE FROM pfm_wifi_visit WHERE date IN (" + FrameCore.Util.arrayFill(deleteQuery.length - 1, '?') + ") AND location_id = ?", deleteQuery);

        var bind;
        var currentDateTime = (new Date()).getTime();
        var current;
        for (var date in data) {
            if (!data.hasOwnProperty(date))
                continue;

            current = data[date];
            bind = [
                locationId,
                date,
                current.capture_rate,
                current.unique_percent,
                current.new_percent,
                current.dwell_time,
                current.frequency,
                existing.hasOwnProperty(date) ? existing[date].created_at : currentDateTime,
                currentDateTime,
            ];

            this.m_db.query("INSERT INTO pfm_wifi_visit (location_id, date, capture_rate, unique_percent, new_percent, dwell_time, frequency, created_at, updated_at)"
                        + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", bind);
        }
    },
    requestSensorGranularityData: function(granularityType, locationId, start, finish, cacheTimeout) {
        var result = { status: this.m_pfmController.NOCHANGE, data: null };
        var bind = [granularityType, locationId, start, finish];
        var row = this.m_db.fetchRow("SELECT * FROM pfm_sensor_granularity WHERE granularity_type = ? AND location_id = ? AND start_date = ? AND finish_date = ?", bind);

        var time = (new Date()).getTime();
        if (cacheTimeout !== undefined && cacheTimeout !== null && (time - row.created_at) > cacheTimeout) {
            this.m_db.query("DELETE FROM pfm_sensor_granularity WHERE granularity_type = ? AND location_id = ? AND start_date = ? AND finish_date = ?", bind);
        } else if (row) {
            result.data = {
                granularity_type: granularityType,
                location_id: row.location_id,
                total_count: row.total_count,
                start_date: row.start_date,
                finish_date: row.finish_date
            };

            result.status = this.m_pfmController.COMPLETED;
        }

        return result;
    },
    saveSensorGranularityData: function(granularityType, locationId, data) {

        var bind = [granularityType, locationId, data.start_date, data.finish_date];
        this.m_db.query("DELETE FROM pfm_sensor_granularity WHERE granularity_type = ? AND location_id = ? AND start_date = ? AND finish_date = ?", bind);

        bind.push(data.total_count);
        bind.push((new Date()).getTime());
        this.m_db.query("INSERT INTO pfm_sensor_granularity (granularity_type, location_id, start_date, finish_date, total_count, created_at)"
                    + " VALUES (?, ?, ?, ?, ?, ?)", bind);
    },
    requestWifiGranularityData: function(granularityType, locationId, start, finish, cacheTimeout) {
        var result = { status: this.m_pfmController.NOCHANGE, data: null };
        var bind = [granularityType, locationId, start, finish];
        var row = this.m_db.fetchRow("SELECT * FROM pfm_wifi_granularity WHERE granularity_type = ? AND location_id = ? AND start_date = ? AND finish_date = ?", bind);

        var time = (new Date()).getTime();
        if (cacheTimeout !== undefined && cacheTimeout !== null && (time - row.created_at) > cacheTimeout) {
            this.m_db.query("DELETE FROM pfm_wifi_granularity WHERE granularity_type = ? AND location_id = ? AND start_date = ? AND finish_date = ?", bind);
        } else if (row) {
            result.data = {
                granularity_type: granularityType,
                location_id:    row.location_id,
                capture_rate:   row.capture_rate,
                unique_percent: row.unique_percent,
                new_percent:    row.new_percent,
                dwell_time:     row.dwell_time,
                frequency:      row.frequency,
                start_date:     row.start_date,
                finish_date:    row.finish_date
            };

            result.status = this.m_pfmController.COMPLETED;
        }

        return result;
    },
    saveWifiGranularityData: function(granularityType, locationId, data) {

        var bind = [granularityType, locationId, data.start_date, data.finish_date];
        this.m_db.query("DELETE FROM pfm_wifi_granularity WHERE granularity_type = ? AND location_id = ? AND start_date = ? AND finish_date = ?", bind);

        bind.push(data.capture_rate);
        bind.push(data.unique_percent);
        bind.push(data.new_percent);
        bind.push(data.dwell_time);
        bind.push(data.frequency);
        bind.push((new Date()).getTime());
        this.m_db.query("INSERT INTO pfm_wifi_granularity (granularity_type, location_id, start_date, finish_date, capture_rate, unique_percent, new_percent, dwell_time, frequency, created_at)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", bind);
    },

    revokeWifiGranularity: function() {
        this.m_db.query("DELETE FROM pfm_wifi_granularity");
    },

    revokeSensorGranularity: function() {
        this.m_db.query("DELETE FROM pfm_sensor_granularity");
    }
};




