import QtQuick 2.5
import Frame.Mobile 1.0

NavigationBarController {
    signal okButtonClicked()
    signal cancelButtonClicked()

    color: '#009e3a'

    label.font.pixelSize: dp(17)
    label.font.family: 'Avenir'
    label.color: '#fff'

    leftLoader.sourceComponent: Item {
        implicitWidth: dp(60)
        implicitHeight: dp(44)

        NavigationBarButton {
            id: cancelButton

            interactiveBackground.visible: false

            label.visible: true
            label.text: 'Cancel'
            label.color: '#fff'
            label.font.pixelSize: sp(17)

            mouse.onClicked: cancelButtonClicked()
            mouse.onPressedChanged: mouse.pressed ? cancelButtonIneractionLogout.press() : cancelButtonIneractionLogout.release()

            anchors.left: parent.left
            anchors.leftMargin: dp(15)
        }

        TouchInteractionButton {
            maxOpacity: 0.5
            circle.color: '#fff'
            id: cancelButtonIneractionLogout

            anchors.centerIn: cancelButton
        }
    }

    rightLoader.sourceComponent: Item {
        implicitWidth: dp(60)
        implicitHeight: dp(44)

        NavigationBarButton {
            id: okButton

            interactiveBackground.visible: false

            label.visible: true
            label.text: 'OK'
            label.color: '#fff'
            label.font.pixelSize: sp(17)

            mouse.onClicked: okButtonClicked()
            mouse.onPressedChanged: mouse.pressed ? okButtonIneractionLogout.press() : okButtonIneractionLogout.release()

            anchors.right: parent.right
            anchors.rightMargin: dp(7)
        }

        TouchInteractionButton {
            maxOpacity: 0.5
            circle.color: '#fff'
            id: okButtonIneractionLogout

            anchors.centerIn: okButton
        }
    }

    z: 99
    height: dp(44)
}
