
SCALABLE_IMAGE_REBUILD = 0
# LOCAL_DEV = 0
SCALABLE_IMAGE_PATH = $$PWD
SCALABLE_IMAGES = \
    back-arrow.png \
    decrease.png \
    increase.png \
    left-arrow.png \
    left-arrow-disabled.png \
    login-logo.png \
    logout.png \
    right-arrow.png \
    right-arrow-disabled.png \
    splash-logo.png \
    splash.png \
    tables.png \
    clear.png \
    drop-shadow-left.png \
    drop-shadow-top.png \
    drop-shadow-right.png \
    drop-shadow-bottom.png \
    list-right-arrow.png \
    spinner.png \
    back-arrow-glow.png \
    big-increase.png \
    big-decrease.png \
    check.png \
    drag_icon.png
