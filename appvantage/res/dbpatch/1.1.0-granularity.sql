CREATE TABLE pfm_sensor_granularity (
    granularity_type INTEGER DEFAULT 0,
    location_id INTEGER DEFAULT 0,
    start_date TEXT NOT NULL DEFAULT '',
    finish_date TEXT NOT NULL DEFAULT '',
    total_count INTEGER NOT NULL DEFAULT 0,
    created_at INTEGER NOT NULL DEFAULT 0
);

CREATE INDEX pfm_sensor_granularity_index ON pfm_sensor_granularity (granularity_type, location_id, start_date, finish_date);

CREATE TABLE pfm_wifi_granularity (
    granularity_type INTEGER DEFAULT 0,
    location_id INTEGER DEFAULT 0,
    start_date TEXT NOT NULL DEFAULT '',
    finish_date TEXT NOT NULL DEFAULT '',
    total_count INTEGER NOT NULL DEFAULT 0,
    new_count INTEGER NOT NULL DEFAULT 0,
    unique_count INTEGER NOT NULL DEFAULT 0,
    dwell_time INTEGER NOT NULL DEFAULT 0,
    frequency REAL NOT NULL DEFAULT 0,
    created_at INTEGER NOT NULL DEFAULT 0
);

CREATE INDEX pfm_wifi_granularity_index ON pfm_wifi_granularity (granularity_type, location_id, start_date, finish_date);
