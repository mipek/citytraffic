CREATE TABLE pfm_user (
    username TEXT NOT NULL DEFAULT '',
    password TEXT NOT NULL DEFAULT ''
);

CREATE TABLE pfm_location (
    location_id INTEGER PRIMARY KEY,
    name TEXT NOT NULL DEFAULT '',
    opening_time INTEGER NOT NULL DEFAULT 0,
    closing_time INTEGER NOT NULL DEFAULT 0,
    options INTEGER NOT NULL DEFAULT 0,
    created_at INTEGER NOT NULL DEFAULT 0,
    updated_at INTEGER NOT NULL DEFAULT 0
);

CREATE INDEX pfm_location_location_id_index ON pfm_location (location_id);

CREATE TABLE pfm_sensor_visit (
    location_id INTEGER DEFAULT 0,
    date TEXT NOT NULL DEFAULT '',
    total_count INTEGER NOT NULL DEFAULT 0,
    created_at INTEGER NOT NULL DEFAULT 0,
    updated_at INTEGER NOT NULL DEFAULT 0
);

CREATE INDEX pfm_sensor_visit_location_id_index ON pfm_sensor_visit (location_id);
CREATE INDEX pfm_sensor_visit_date_index ON pfm_sensor_visit (date);

CREATE TABLE pfm_wifi_visit (
    location_id INTEGER DEFAULT 0,
    date TEXT NOT NULL DEFAULT '',
    total_count INTEGER NOT NULL DEFAULT 0,
    new_count INTEGER NOT NULL DEFAULT 0,
    unique_count INTEGER NOT NULL DEFAULT 0,
    dwell_time INTEGER NOT NULL DEFAULT 0,
    created_at INTEGER NOT NULL DEFAULT 0,
    updated_at INTEGER NOT NULL DEFAULT 0
);

CREATE INDEX pfm_wifi_visit_location_id_index ON pfm_wifi_visit (location_id);
CREATE INDEX pfm_wifi_visit_date_index ON pfm_wifi_visit (date);
