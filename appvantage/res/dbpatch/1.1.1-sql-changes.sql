
DROP TABLE pfm_wifi_visit;
DROP TABLE pfm_wifi_granularity;

CREATE TABLE pfm_wifi_visit (
    location_id INTEGER DEFAULT 0,
    date TEXT NOT NULL DEFAULT '',
    capture_rate REAL NOT NULL DEFAULT 0,
    new_percent REAL NOT NULL DEFAULT 0,
    unique_percent REAL NOT NULL DEFAULT 0,
    dwell_time INTEGER NOT NULL DEFAULT 0,
    frequency REAL NOT NULL DEFAULT 0,
    created_at INTEGER NOT NULL DEFAULT 0,
    updated_at INTEGER NOT NULL DEFAULT 0
);

CREATE INDEX pfm_wifi_visit_location_id_index ON pfm_wifi_visit (location_id);
CREATE INDEX pfm_wifi_visit_date_index ON pfm_wifi_visit (date);

CREATE TABLE pfm_wifi_granularity (
    granularity_type INTEGER DEFAULT 0,
    location_id INTEGER DEFAULT 0,
    start_date TEXT NOT NULL DEFAULT '',
    finish_date TEXT NOT NULL DEFAULT '',
    capture_rate REAL NOT NULL DEFAULT 0,
    new_percent REAL NOT NULL DEFAULT 0,
    unique_percent REAL NOT NULL DEFAULT 0,
    dwell_time INTEGER NOT NULL DEFAULT 0,
    frequency REAL NOT NULL DEFAULT 0,
    created_at INTEGER NOT NULL DEFAULT 0
);

CREATE INDEX pfm_wifi_granularity_index ON pfm_wifi_granularity (granularity_type, location_id, start_date, finish_date);
