
FONT_PATH = $$replace(PWD, $$PROJECT_HOME, "")

# Define fonts
RESOURCE_FILES += \
    $$PWD/avenir-lt-std/AvenirLTStd-Black.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-BlackOblique.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-Book.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-BookOblique.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-Heavy.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-HeavyOblique.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-Light.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-LightOblique.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-Medium.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-MediumOblique.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-Oblique.otf \
    $$PWD/avenir-lt-std/AvenirLTStd-Roman.otf

RESOURCE_FILES += \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-Bold.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-BoldCn.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-BoldCnIt.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-Cn.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-CnIt.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-Demi.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-DemiCn.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-DemiCnIt.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-DemiIt.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-HeavyCn.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-HeavyCnIt.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-It.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-MediumCn.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-MediumCnIt.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-Regular.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-UltLtCn.otf \
    $$PWD/avenir-next-lt-pro/AvenirNextLTPro-UltLtCnIt.otf

