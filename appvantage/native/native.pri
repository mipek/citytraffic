
android {
    # Android native project
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
    OTHER_FILES += $$PWD/android/AndroidManifest.xml
}

ios {
    QMAKE_INFO_PLIST = $$PWD/ios/Info.plist
    LIBS += -framework CoreData -L$$PWD/../3rdparty/google-analytics-ios -lGoogleAnalyticsServices

    APP_ICON_FILES.files = $$files($$PWD/ios/pfm/icons/*.png)
    APP_ICON_FILES.path =
    QMAKE_BUNDLE_DATA += APP_ICON_FILES

    DEFAULT_LAUNCH.files = $$files($$PWD/ios/pfm/splash/LaunchImage*.png)
    DEFAULT_LAUNCH.path =
    QMAKE_BUNDLE_DATA += DEFAULT_LAUNCH

    LAUNCH_SCREEN.files = $$PWD/ios/Launch.xib $$PWD/ios/launch-splash.png
    QMAKE_BUNDLE_DATA += LAUNCH_SCREEN
}
