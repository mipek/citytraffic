#include "applicationplugin.h"
#include <qqml.h>
#include <QDir>
#include <QFontDatabase>
#include "cryptoutil.h"
#include "qtchartdatabridge.h"

Q_LOGGING_CATEGORY(AppPlugin, "com.triodor.pfm.plugin")

void ApplicationPlugin::registerTypes()
{
    const char *uri = "com.triodor.pfm";

    qmlRegisterType<CryptoUtil>(uri, 1, 0, "CryptoUtil");
    qmlRegisterType<QtChartDataBridge>(uri, 1, 0, "QtChartDataBridge");
}

void ApplicationPlugin::loadFonts(const QString &directoryPath)
{
    const QStringList fontList = QDir(directoryPath).entryList();
    foreach (const QString &fontName, fontList) {
        if (QFontDatabase::addApplicationFont(QString("%1/%2").arg(directoryPath).arg(fontName)) < 0) {
            qCWarning(AppPlugin) << "Font cannot loaded for OS: " << fontName;
        }
    }
}
