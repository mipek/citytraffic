#include "cryptoutil.h"
#include <QFile>

CryptoUtil::CryptoUtil(QObject *parent)
    : QObject(parent)
    , m_rsa(new CryptographyRsa())
    , m_inited(false)
{
}

CryptoUtil::~CryptoUtil()
{
    delete m_rsa;
    m_rsa = 0;
}

void CryptoUtil::init()
{
    if (m_inited)
        return;

    QFile file(":/res/key/public.key");
    file.open(QFile::ReadOnly);
    m_rsa->setPublicKey(QString::fromLatin1(file.readAll()));
    file.close();

    m_inited = true;
}

QString CryptoUtil::encrypt(const QString &data)
{
    init();
    return m_rsa->encryptWithPublicKey(data);
}
