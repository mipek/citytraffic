#ifndef CRYPTOUTIL_H
#define CRYPTOUTIL_H

#include <QObject>
#include <FrameSecurity/cryptographyrsa.h>
#include <qqml.h>

class CryptoUtil : public QObject
{
    Q_OBJECT
public:
    explicit CryptoUtil(QObject *parent = 0);
    ~CryptoUtil();

    Q_INVOKABLE QString encrypt(const QString &data);

private:
    void init();

private:
    CryptographyRsa *m_rsa;
    bool m_inited;
};

QML_DECLARE_TYPE(CryptoUtil)

#endif // CRYPTOUTIL_H
