#include "qtchartdatabridge.h"
#include <QtCharts/QXYSeries>
#include <Frame/utility.h>

Q_LOGGING_CATEGORY(ChartData, "ChartData")

QtChartDataBridge::QtChartDataBridge(QObject *parent)
    : QObject(parent)
{
}

void QtChartDataBridge::setPoints(QtCharts::QAbstractSeries *series, const QVariant &points)
{
    QVariant pointVar = Utility::normalizeJSValue(points);
    if (pointVar.type() != QVariant::List) {
        qCWarning(ChartData) << "Given data should be Array.";
        return;
    }

    QtCharts::QXYSeries *xySeries = qobject_cast<QtCharts::QXYSeries *>(series);
    if (!xySeries) {
        qCWarning(ChartData) << "Series is not a QXYSeries object.";
        return;
    }

    QVector<QPointF> replacePoints;

    QVariantList *pointList = static_cast<QVariantList *>(pointVar.data());
    const QVariantMap *map = 0;

    int i = 0;
    int len = pointList->length();

    // reserve length
    if (len > 0) {
        replacePoints.reserve(len);

        for (; i < len; i++) {
            const QVariant point = pointList->at(i);
            if (point.type() == QVariant::Map) {
                map = static_cast<const QVariantMap *>(point.data());
                if (!map->contains(QLatin1String("x")) || !map->contains(QLatin1String("y"))) {
                    qCWarning(ChartData) << "Point value object does not contain x or y";
                    continue;
                }

                replacePoints.append(QPointF(map->value(QLatin1String("x")).toDouble(), map->value(QLatin1String("y")).toDouble()));
            } else if (point.type() == QVariant::PointF) {
                replacePoints.append(point.toPointF());
            } else {
                qCWarning(ChartData) << "Point value should be Object or PointF.";
            }
        }
    }

    xySeries->replace(replacePoints);
}

