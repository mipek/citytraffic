#ifndef QTCHARTDATABRIDGE_H
#define QTCHARTDATABRIDGE_H

#include <qqml.h>
#include <QtCharts/QAreaSeries>
#include <QLoggingCategory>

#include <QDate>

Q_DECLARE_LOGGING_CATEGORY(ChartData)


class QtChartDataBridge : public QObject
{
    Q_OBJECT
public:
    explicit QtChartDataBridge(QObject *parent = 0);
    Q_INVOKABLE int getDayNum(QString date)
    {
        QDate tempdate = QDate::fromString(date,"yyyy-MM-dd");
        return tempdate.day();;
    }
    Q_INVOKABLE int getWeekNum(QString date)
    {
        QDate tempdate = QDate::fromString(date,"yyyy-MM-dd");
        return tempdate.weekNumber();
    }
    Q_INVOKABLE int getMonthNum(QString date)
    {
        QDate tempdate = QDate::fromString(date,"yyyy-MM-dd");
        return tempdate.month();
    }

    Q_INVOKABLE int diffDatesInDays(QString date1,QString date2)
    {
        QDate tempdate1 = QDate::fromString(date1,"yyyy-MM-dd");
        QDate tempdate2 = QDate::fromString(date2,"yyyy-MM-dd");
        return tempdate1.daysTo(tempdate2);
    }

public slots:
    void setPoints(QtCharts::QAbstractSeries *series, const QVariant &pointList);
};

QML_DECLARE_TYPE(QtChartDataBridge)

#endif // QTCHARTDATABRIDGE_H
