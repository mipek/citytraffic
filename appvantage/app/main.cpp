#include <QApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QDir>
#include <QLoggingCategory>
#include <QFontDatabase>
#include <QStandardPaths>
#include <QQmlContext>
#include <QFile>
#include <QSettings>
#include <Frame/applicationinfo.h>
#include <Frame/frame.h>
#include <QColor>
#include "../src/applicationplugin.h"


Q_LOGGING_CATEGORY(FRAME_TAG, "com.triodor.main")

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationVersion(QLatin1String(APP_VERSION));

#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
    // we are handling high dpi in frame for now
    app.setAttribute(Qt::AA_DisableHighDpiScaling);
#endif

    ApplicationPlugin::registerTypes();
    ApplicationPlugin::loadFonts(QString(":%1/avenir-lt-std").arg(FRAME_FONT_PATH));
    ApplicationPlugin::loadFonts(QString(":%1/avenir-next-lt-pro").arg(FRAME_FONT_PATH));

    // init frame
    Frame *frame = Frame::instance();
    frame->initialize();
    frame->setStatusBarColor(QColor(0, 158, 58));

    // set application info
    ApplicationInfo *applicationInfo = ApplicationInfo::instance();
    applicationInfo->setVersion(app.applicationVersion());
#if defined(CONSOLE_DEV) && CONSOLE_DEV == 1
    applicationInfo->setTestMode(true);
#else
    applicationInfo->setTestMode(false);
#endif

#if defined(DEV_MODE) && DEV_MODE == 1
    applicationInfo->setDevMode(true);
#else
    applicationInfo->setDevMode(false);
#endif

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:" FRAME_QML_PATH "/main.qml")));

    qCDebug(FRAME_TAG) << engine.offlineStoragePath();

    return app.exec();
}
